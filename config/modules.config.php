<?php
return [
    'Laminas\ZendFrameworkBridge',
    'Laminas\Router',
    'Laminas\Validator',
    'Laminas\Form',
    'Laminas\Mvc\Plugin\FlashMessenger',
    'Application',
];
