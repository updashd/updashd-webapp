<?php

return [
    'caches' => [
        'Cache\Transient' => [
            'adapter' => 'memcache',
            'ttl'     => 60,
            'options' => [
                'writable' => getenv('UPDASHD_CACHE') == 'no' ? false : true,
            ],
            'plugins' => [
                'exception_handler' => [
                    'throw_exceptions' => false,
                ],
            ],
        ],
        'Cache\Persistence' => [
            'adapter' => 'filesystem',
            'ttl'     => 86400,
            'options' => [
                'writable' => getenv('UPDASHD_CACHE') == 'no' ? false : true,
                'cache_dir' => __DIR__ . '/../../data/cache/',
            ],
            'plugins' => [
                'serializer'
            ],
        ],
    ],
    'service_manager' => [
        'abstract_factories' => [
            'Laminas\Cache\Service\StorageCacheAbstractServiceFactory'
        ],
    ],
];