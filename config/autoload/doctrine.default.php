<?php
return [
    'doctrine' => [
        'log_queries' => (getenv('UPDASHD_LOG_QUERIES') == 'yes'),
        'options' => [
            'dev_mode' => getenv('UPDASHD_DEBUG') ?: false,
            'simple_annotation' => false,
            'entity_paths' => [
                __DIR__ . '/../../vendor/updashd/updashd-model/src'
            ],
            'proxy_dir' => __DIR__ . '/../../data/doctrine_proxy'
        ],
        'connection' => [
            'driver' => 'pdo_mysql',
            'host' => getenv('MYSQL_HOST') ?: 'localhost',
            'dbname' => getenv('MYSQL_DB') ?: 'updashd',
            'user' => getenv('MYSQL_USER') ?: 'updashd',
            'password' => getenv('MYSQL_PASS') ?: null,
        ],
        'caching' => [
            'enable' => getenv('UPDASHD_CACHE') == 'no' ? false : true,
            'result_cache' => getenv('UPDASHD_CACHE') == 'no' ? false : true,
            'hydration_cache' => getenv('UPDASHD_CACHE') == 'no' ? false : true,
            'metadata_cache' => getenv('UPDASHD_CACHE') == 'no' ? false : true,
            'query_cache' => getenv('UPDASHD_CACHE') == 'no' ? false : true,
            'engine' => 'filesystem',
            'engine_configuration' => [
                'array' => null,
                'apc' => null,
                'apcu' => null,
                'memcache' => [
                    'host' => 'localhost',
                    'port' => 11211,
                    'timeout' => null
                ],
                'memcached' => [
                    'servers' => [
                        [
                            'host' => 'localhost',
                            'port' => 11211,
                            'weight' => 0
                        ]
                    ]
                ],
                'xcache' => null,
                'redis' => [
                    'host' => 'localhost',
                    'port' => 6379,
                    'timeout' => 2,
                    'password' => null,
                    'database' => null,
                ],
                'predis' => 'tcp://127.0.0.1:6379',
                'filesystem' => [
                    'directory' => __DIR__ . '/../../data/doctrine_cache',
                    'extension' => '.dat',
                    'umask' => 0002
                ]
            ]
        ]
    ]
];