<?php
$redisAddr = getenv('REDIS_ADDR') ?: '127.0.0.1';
$redisPort = getenv('REDIS_PORT') ?: 6379;
$redisPassword = getenv('REDIS_PASS') ?: null;

$predisParams = [];

if ($redisPassword) {
    $predisParams['password'] = $redisPassword;
}

$predisUri = 'tcp://' . $redisAddr . ':' . $redisPort . ($predisParams ? '?' . http_build_query($predisParams) : '');

return [
    'session_custom' => [
        'type' => 'existing_predis', // can also be file
        'config' => $predisUri,
        'ttl' => 3600 //in seconds
    ],
    'session' => [
        'remember_me_seconds' => 180,
        'use_cookies' => true,
        'cookie_httponly' => true
    ]
];