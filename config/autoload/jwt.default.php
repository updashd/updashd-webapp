<?php
return [
    'jwt' => [
        'algorithm' => 'HS256', // HMAC + SHA256
        'email_key' => getenv('EMAIL_JWT_KEY') ?: 'abc123', // This is the key for messages. Use any string, preferably random.
        'pusher_key' => getenv('PUSHER_JWT_KEY') ?: 'abc123', // This is the key for WebSocket pushes. Use any string, preferably random.
    ]
];