<?php

$transport = (getenv('SMTP_ENABLED')) ? 'smtp' : 'memory';
$transportOptions = [];
$logLevel = getenv('UPDASHD_EMAIL_LOG') ?: 1;

if (getenv('SMTP_HOST')) $transportOptions['host'] = getenv('SMTP_HOST');
if (getenv('SMTP_CLASS')) $transportOptions['connection_class'] = getenv('SMTP_CLASS');
if (getenv('SMTP_PORT')) $transportOptions['connection_config']['port'] = getenv('SMTP_PORT');
if (getenv('SMTP_USER')) $transportOptions['connection_config']['username'] = getenv('SMTP_USER');
if (getenv('SMTP_PASS')) $transportOptions['connection_config']['password'] = getenv('SMTP_PASS');
if (getenv('SMTP_SSL')) $transportOptions['connection_config']['ssl'] = getenv('SMTP_SSL');

return [
    'mail' => [
        'transport' => 'memory', // Either sendmail, smtp, or memory

        //////////////////////
        //// SMTP Options ////
        //////////////////////
        // 'name' => 'localhost.localdomain',
        // 'host' => '127.0.0.1',
        // 'connection_class' => 'crammd5',
        // 'connection_config' => [
        //     'username' => 'user',
        //     'password' => 'pass',
        //     'ssl' => 'tls',
        // ],
        'transport_options' => $transportOptions,

        'logging' => [
            'level' => $logLevel, // 0 for none, 1 for general only, 2 for complete message
            'general_log' => __DIR__ . '/../../logs/mail/all.txt',
            'path' => __DIR__ . '/../../logs/mail/',
        ]
    ]
];