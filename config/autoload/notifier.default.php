<?php
return [
    'notifier_config' => [
        'notifiers' => [
            \Updashd\Notifier\SlackNotifier::class,
        ],
        'public_key_file' => getenv('NOTIFIER_PUB_KEY_FILE') ?: __DIR__ . '/../keys/notifier_pub.pem'
    ]
];