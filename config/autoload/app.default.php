<?php
return [
    'application' => [
        'url' => getenv('UPDASHD_URL') ?: 'http://localhost/',
        'pusher' => getenv('UPDASHD_PUSHER_URL') ?: 'ws://localhost:8080/'
    ]
];