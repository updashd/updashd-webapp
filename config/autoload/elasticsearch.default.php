<?php
return [
    'elasticsearch' => [
        'Hosts' => [
            getenv('ELASTICSEARCH_HOST') ?: 'localhost'
        ]
    ]
];