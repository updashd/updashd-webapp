<?php
return [
    'worker_config' => [
        'workers' => [
            \Updashd\Worker\CurlHttp::class,
            \Updashd\Worker\CurlHttps::class,
            \Updashd\Worker\IcmpPing::class,
            \Updashd\Worker\Dns::class,
            \Updashd\Worker\MySQLReplication::class,
            \Updashd\Worker\MySQLPing::class,
            \Updashd\Worker\MySQLQueryResult::class,
        ],
        'public_key_file' => getenv('WORKER_PUB_KEY_FILE') ?: __DIR__ . '/../keys/worker_pub.pem'
    ]
];