Updashd
=======
Extensible Server Monitoring

# Installation

1. Copy `config/doctrine.local.php.dist` to `config/doctrine.local.php`
2. Configure Database connection in `config/doctrine.local.php`
3. Copy `config/redis.local.php.dist` to `config/redis.local.php`
4. Configure Redis connection in `config/redis.local.php`
5. `composer install`
6. Change to public and `bower install`

# Generating Doctrine Entities (For Contributors)

    ./vendor/bin/doctrine orm:convert-mapping -f --from-database --extend "Application\\Model\\Entity\\AbstractAuditedEntity" --namespace "Application\\Model\\Entity\\" -- annotation module/Application/src/
    ./vendor/bin/doctrine orm:generate-entities --generate-annotations=true module/Application/src
    
    