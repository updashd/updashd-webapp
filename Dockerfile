FROM php:7.2.13-apache-stretch

RUN mkdir -p /opt/updashd-webapp

WORKDIR /opt/updashd-webapp

COPY public ./public

RUN apt-get update && apt-get install -y \
    zip \
    unzip \
    git \
    libzip-dev \
    zlib1g-dev \
    gnupg2 \
    software-properties-common \
    && curl -sL https://deb.nodesource.com/setup_8.x | bash - \
    && apt-get install -y nodejs \
    && npm install -g bower \
    && CURDIR=$PWD \
    && cd public \
    && bower install --allow-root \
    && cd $CURDIR \
    && npm remove -g bower \
    && apt-get remove -y software-properties-common gnupg2 nodejs \
    && apt-get autoremove -y \
    && rm -rf /var/lib/apt/lists/*

RUN docker-php-ext-configure zip --with-libzip \
    && docker-php-ext-install zip \
    && docker-php-ext-configure pdo_mysql \
    && docker-php-ext-install pdo_mysql

COPY composer.json ./
COPY composer.lock ./

RUN curl https://raw.githubusercontent.com/composer/getcomposer.org/master/web/installer -q | php -- --quiet --install-dir="/usr/local/bin" --filename="composer" \
    && composer install


COPY data ./data
COPY logs ./logs

RUN a2enmod ssl rewrite \
    && rm -f /etc/apache2/sites-enabled/000-default.conf \
    && chown -R www-data:www-data data \
    && chown -R www-data:www-data logs

COPY config ./config
COPY module ./module

COPY updashd.conf /etc/apache2/sites-enabled/updashd.conf

EXPOSE 80 443

