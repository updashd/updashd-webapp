<?php
namespace Application;

use Application\Controller\AbstractControllerFactory;
use Application\View\Helper\AbstractFactory;
use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;

return [
    'router' => [
        'routes' => [
            'home' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/',
                    'defaults' => [
                        'route' => 'index',
                        'controller' => 'index',
                        'action' => 'index',
                    ],
                ],
            ],
            'admin' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/admin[/:controller[/:action]]',
                    'constraints' => [
                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ],
                    'defaults' => [
                        'controller' => 'index',
                        'action' => 'index',
                    ],
                ],
            ],
            'auth' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/auth[/:controller[/:action]]',
                    'constraints' => [
                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ],
                    'defaults' => [
                        'controller' => 'index',
                        'action' => 'index',
                    ],
                ],
            ],
            'ajax' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/ajax[/:controller[/:action]]',
                    'constraints' => [
                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ],
                    'defaults' => [
                        'controller' => 'index',
                        'action' => 'index',
                    ],
                ],
            ],
            'user' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/user[/:action]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*'
                    ],
                    'defaults' => [
                        'controller' => 'index',
                        'action' => 'index',
                    ],
                ],
            ],
            'portal' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/portal[/:action]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*'
                    ],
                    'defaults' => [
                        'controller' => 'index',
                        'action' => 'index',
                    ],
                ],
            ],
            'account' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/account[/:account[/:controller[/:action]]]',
                    'constraints' => [
                        'account' => '[a-zA-Z][a-zA-Z0-9_\-]+',
                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ],
                    'defaults' => [
                        'controller' => 'index',
                        'action' => 'index',
                    ],
                ],
            ],
            'open' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/open[/:account[/:controller[/:action]]]',
                    'constraints' => [
                        'account' => '[a-zA-Z][a-zA-Z0-9_\-]+',
                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ],
                    'defaults' => [
                        'controller' => 'index',
                        'action' => 'index',
                    ],
                ],
            ],
        ],
    ],
    'controllers' => [
        'abstract_factories' => [
            AbstractControllerFactory::class
        ],
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions' => true,
        'doctype' => 'HTML5',
        'not_found_template' => 'error/404',
        'exception_template' => 'error/index',
        'template_map' => [
            'layout/layout' => __DIR__ . '/../view/layout/layout.phtml',
            'layout/auth' => __DIR__ . '/../view/layout/auth.phtml',
            'layout/public' => __DIR__ . '/../view/layout/public.phtml',
            'error/404' => __DIR__ . '/../view/error/404.phtml',
            'error/index' => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
        'strategies' => [
            'ViewJsonStrategy'
        ],
    ],
    'service_manager' =>  include __DIR__ . '/servicemanager.config.php',
    'acl' => include __DIR__ . '/acl.config.php',
    'menu' => include __DIR__ . '/menu.config.php',
    'view_helpers' => [
        'abstract_factories' => [
            AbstractFactory::class
        ]
    ],
];
