<?php

use Application\Service;

return [
    'factories' => [
        \Laminas\Session\Config\ConfigInterface::class => \Laminas\Session\Service\SessionConfigFactory::class,
        Service\DoctrineManager::class => Service\Factory\DoctrineManagerFactory::class,
        Service\PredisService::class => Service\Factory\PredisServiceFactory::class,
        Service\AclService::class => Service\Factory\AclServiceFactory::class,
        Service\AuthService::class => Service\Factory\AuthServiceFactory::class,
        Service\SlugHelperService::class => Service\Factory\SlugHelperServiceFactory::class,
        Service\Menu::class => Service\Factory\MenuFactory::class,
        Service\LayoutService::class => Service\Factory\LayoutServiceFactory::class,
        Service\WorkerService::class => Service\Factory\WorkerServiceFactory::class,
        Service\NotifierService::class => Service\Factory\NotifierServiceFactory::class,
        Service\CacheManagerService::class => Service\Factory\CacheManagerFactory::class,
        Service\PersonSettingService::class => Service\Factory\PersonSettingServiceFactory::class,
        Service\ElasticSearchService::class => Service\Factory\ElasticSearchServiceFactory::class,
        Service\MailService::class => Service\Factory\MailServiceFactory::class,
        Service\AccountService::class => Service\Factory\AccountServiceFactory::class,
        Service\JWTService::class => Service\Factory\JWTServiceFactory::class,
    ]
];