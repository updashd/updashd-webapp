<?php

namespace Application\Session\SaveHandler;

use Laminas\Session\SaveHandler\SaveHandlerInterface;

class Predis implements SaveHandlerInterface {
    /** @var array */
    protected $config;

    /** @var \Predis\Client */
    protected $client;

    const PREFIX = 'SESSION_';

    /**
     * Predis constructor.
     *
     * @param array $config
     * @param \Predis\Client $client
     */
    public function __construct ($config, $client = null) {
        $this->config = $config;
        $this->client = $client ?: new \Predis\Client($config['config']);
    }

    /**
     * Close the session
     *
     * @link https://php.net/manual/en/sessionhandlerinterface.close.php
     * @return bool <p>
     * The return value (usually TRUE on success, FALSE on failure).
     * Note this value is returned internally to PHP for processing.
     * </p>
     * @since 5.4.0
     */
    public function close () {
        return true;
    }

    /**
     * Destroy a session
     *
     * @link https://php.net/manual/en/sessionhandlerinterface.destroy.php
     * @param string $session_id The session ID being destroyed.
     * @return bool <p>
     * The return value (usually TRUE on success, FALSE on failure).
     * Note this value is returned internally to PHP for processing.
     * </p>
     * @since 5.4.0
     */
    public function destroy ($session_id) {
        $this->client->del([$this->getKeyPath($session_id)]);

        return true;
    }

    /**
     * Cleanup old sessions
     *
     * @link https://php.net/manual/en/sessionhandlerinterface.gc.php
     * @param int $maxlifetime <p>
     * Sessions that have not updated for
     * the last maxlifetime seconds will be removed.
     * </p>
     * @return bool <p>
     * The return value (usually TRUE on success, FALSE on failure).
     * Note this value is returned internally to PHP for processing.
     * </p>
     * @since 5.4.0
     */
    public function gc ($maxlifetime) {
        return true;
    }

    /**
     * Initialize session
     *
     * @link https://php.net/manual/en/sessionhandlerinterface.open.php
     * @param string $save_path The path where to store/retrieve the session.
     * @param string $name The session name.
     * @return bool <p>
     * The return value (usually TRUE on success, FALSE on failure).
     * Note this value is returned internally to PHP for processing.
     * </p>
     * @since 5.4.0
     */
    public function open ($save_path, $name) {
        return true;
    }

    /**
     * Read session data
     *
     * @link https://php.net/manual/en/sessionhandlerinterface.read.php
     * @param string $session_id The session id to read data for.
     * @return string <p>
     * Returns an encoded string of the read data.
     * If nothing was read, it must return an empty string.
     * Note this value is returned internally to PHP for processing.
     * </p>
     * @since 5.4.0
     */
    public function read ($session_id) {
        return $this->client->get($this->getKeyPath($session_id)) ?: '';
    }

    /**
     * Write session data
     *
     * @link https://php.net/manual/en/sessionhandlerinterface.write.php
     * @param string $session_id The session id.
     * @param string $session_data <p>
     * The encoded session data. This data is the
     * result of the PHP internally encoding
     * the $_SESSION superglobal to a serialized
     * string and passing it as this parameter.
     * Please note sessions use an alternative serialization method.
     * </p>
     * @return bool <p>
     * The return value (usually TRUE on success, FALSE on failure).
     * Note this value is returned internally to PHP for processing.
     * </p>
     * @since 5.4.0
     */
    public function write ($session_id, $session_data) {
        $this->client->set($this->getKeyPath($session_id), $session_data, 'EX', $this->getTtl());

        return true;
    }

    /**
     * @param $session_id
     * @return string
     */
    public function getKeyPath ($session_id) : string {
        return self::PREFIX . $session_id;
    }

    public function getTtl () {
        return $this->config['ttl'];
    }
}