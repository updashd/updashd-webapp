<?php
namespace Application\Doctrine;

use Doctrine\DBAL\Logging\SQLLogger;
use Doctrine\DBAL\Platforms\AbstractPlatform;

class Logger implements SQLLogger {
    
    const QUERY_TYPE_SELECT = "SELECT";
    const QUERY_TYPE_UPDATE = "UPDATE";
    const QUERY_TYPE_INSERT = "INSERT";
    const QUERY_TYPE_DELETE = "DELETE";
    const QUERY_TYPE_CREATE = "CREATE";
    const QUERY_TYPE_ALTER = "ALTER";
    
    private $dbPlatform;
    
    private static $queries = [];
    
    private $loggedQueryTypes;

    private $startTime;
    private $endTime;

    public function __construct (AbstractPlatform $dbPlatform, array $loggedQueryTypes = array()) {
        $this->dbPlatform = $dbPlatform;
        $this->loggedQueryTypes = $loggedQueryTypes;
    }
    
    /**
     * {@inheritdoc}
     */
    public function startQuery ($sql, array $params = null, array $types = null) {
        if ($this->isLoggable($sql)) {
//            if (! empty($params)) {
//                foreach ($params as $key => $param) {
//                    $type = Type::getType($types[$key]);
//                    $value = $type->convertToDatabaseValue($param, $this->dbPlatform);
//                    $sql = join(var_export($value, true), explode('?', $sql, 2));
//                }
//            }
            
            self::$queries[] = [
                'stack' => implode("\n", array_map(function ($v) {
                    $output = '';
                    $output .= isset($v['class']) ? $v['class'] : '';
                    $output .= isset($v['type']) ? $v['type'] : '';
                    $output .= isset($v['function']) ? $v['function'] : '';
                    $output .= ': ';
                    $output .= isset($v['file']) ? str_replace(ROOT_DIR, 'ROOT_DIR', $v['file']) : '';
                    $output .= '(' . (isset($v['line'])  ? $v['line'] : '') . ')';
                    return $output;
                }, debug_backtrace())),
                'sql' => $sql,
                'params' => $params
            ];
        }

        $this->startTimer();
    }
    
    /**
     * {@inheritdoc}
     */
    public function stopQuery () {
        $this->stopTimer();

        self::$queries[count(self::$queries) - 1]['duration'] = $this->getTimerElapsed();
    }
    
    private function isLoggable ($sql) {
        if (empty($this->loggedQueryTypes)) {
            return true;
        }
        foreach ($this->loggedQueryTypes as $validType) {
            if (strpos($sql, $validType) === 0) {
                return true;
            }
        }
        return false;
    }

    public static function hasQueries () {
        return count(self::$queries);
    }
    
    /**
     * @return string[]
     */
    public static function getQueries () {
        return self::$queries;
    }

    public function startTimer () {
        $this->startTime = microtime(true);
    }

    public function stopTimer () {
        $this->endTime = microtime(true);
    }

    public function getTimerElapsed () {
        return $this->endTime - $this->startTime;
    }
}