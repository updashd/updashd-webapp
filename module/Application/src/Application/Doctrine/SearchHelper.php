<?php
namespace Application\Doctrine;

use Doctrine\ORM\QueryBuilder;

class SearchHelper {
    const COMBINE_AND = 'and';
    const COMBINE_OR = 'OR';

    protected $queryBuilder;
    protected $whereExpr;
    protected $havingExpr;
    protected $params = [];

    public function __construct (QueryBuilder $queryBuilder) {
        $this->setQueryBuilder($queryBuilder);
    }

    public function addWhereConditionBetween ($field, $firstValue, $secondValue, $combiner = self::COMBINE_AND) {
        $qb = $this->getQueryBuilder();

        $expr = null;

        $expr = $qb->expr()->between($field, $this->addParam($firstValue), $this->addParam($secondValue));

        if ($expr) {
            $this->combineExpression($expr, $combiner);
            $this->setWhereExpr($expr);
        }
    }

    public function addWhereCondition ($field, $value, $useLike = false, $combiner = self::COMBINE_AND) {
        $qb = $this->getQueryBuilder();

        $expr = null;

        if (is_array($value)) {
            $expr = $qb->expr()->in($field, $this->addParam($value));
        }
        elseif ($useLike && is_string($value) && strlen($value) > 0) {
            $expr = $qb->expr()->like($field, $this->addParam($value));
        }
        elseif ($value || is_bool($value) || is_numeric($value)) {
            $expr = $qb->expr()->eq($field, $this->addParam($value));
        }

        if ($expr) {
            $this->combineExpression($expr, $combiner);
            $this->setWhereExpr($expr);
        }
    }

    public function addHavingCondition ($field, $value, $useLike = false, $combiner = self::COMBINE_AND) {
        $qb = $this->getQueryBuilder();

        $expr = null;

        if (is_array($value)) {
            $expr = $qb->expr()->in($field, $this->addParam($value));
        }
        elseif ($useLike && is_string($value) && strlen($value) > 0) {
            $expr = $qb->expr()->like($field, $this->addParam($value));
        }
        elseif ($value) {
            $expr = $qb->expr()->eq($field, $this->addParam($value));
        }

        if ($expr) {
            $this->combineExpression($expr, $combiner);
            $this->setHavingExpr($expr);
        }
    }

    public function combineExpression (&$expr, $combiner = self::COMBINE_AND) {
        $qb = $this->getQueryBuilder();

        $currentExpr = $this->getWhereExpr();
        if ($expr && $currentExpr) {
            switch ($combiner) {
                case self::COMBINE_OR: $expr = $qb->expr()->orX($currentExpr, $expr); break;
                case self::COMBINE_AND: // Default is and
                default: $expr = $qb->expr()->andX($currentExpr, $expr); break;
            }
        }
    }

    protected function addParam($value) {
        $key = 'shp' . count($this->params);

        $this->params[$key] = $value;

        return ':' . $key;
    }

    /**
     * @return \Doctrine\ORM\Query
     */
    public function getQuery () {
        $qb = $this->getQueryBuilder();

        if ($this->getWhereExpr()) {
            $qb->where($this->getWhereExpr());
        }

        if ($this->getHavingExpr()) {
            $qb->having($this->getHavingExpr());
        }

        $query = $qb->getQuery();

        foreach ($this->params as $paramKey => $paramValue) {
            $query->setParameter($paramKey, $paramValue);
        }

        return $query;
    }

    /**
     * @return array
     */
    public function getResult () {
        return $this->getQuery()->getResult();
    }

    /**
     * @return object
     */
    protected function getWhereExpr () {
        return $this->whereExpr;
    }

    /**
     * @param object $whereExpr
     */
    protected function setWhereExpr ($whereExpr) {
        $this->whereExpr = $whereExpr;
    }

    /**
     * @return object
     */
    protected function getHavingExpr () {
        return $this->havingExpr;
    }

    /**
     * @param object $havingExpr
     */
    protected function setHavingExpr ($havingExpr) {
        $this->havingExpr = $havingExpr;
    }

    /**
     * @return QueryBuilder
     */
    protected function getQueryBuilder () {
        return $this->queryBuilder;
    }

    /**
     * @param QueryBuilder $queryBuilder
     */
    protected function setQueryBuilder ($queryBuilder) {
        $this->queryBuilder = $queryBuilder;
    }
}