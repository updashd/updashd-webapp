<?php
namespace Application\Doctrine;

use Doctrine\ORM\Tools\Pagination\Paginator;
use Laminas\Paginator\Adapter\AdapterInterface;

class DoctrinePaginatorAdapter implements AdapterInterface {
    protected $paginator = null;
    protected $count = null;

    public function __construct (Paginator $paginator) {
        $this->paginator = $paginator;
        $this->count = count($paginator);
    }

    public function getItems ($offset, $itemCountPerPage) {
        return $this->paginator->getIterator();
    }

    public function count () {
        return $this->count;
    }
}