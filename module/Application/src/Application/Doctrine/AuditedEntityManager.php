<?php
namespace Application\Doctrine;

use Application\Service\AuthService;
use Doctrine\ORM\Decorator\EntityManagerDecorator;
use Doctrine\ORM\EntityManagerInterface;
use Updashd\Model\AbstractAuditedEntity;
use Updashd\Model\Person;

class AuditedEntityManager extends EntityManagerDecorator {
    private $authService;

    public function __construct(EntityManagerInterface $entityManager, $authService) {
        parent::__construct($entityManager);

        $this->setAuthService($authService);
    }
    
    public function persist ($entity) {
        if ($entity instanceof AbstractAuditedEntity && $this->getAuthService()) {
            $personId = $this->getAuthService()->getPersonId();
            
            if ($personId) {
                $personReference = $this->wrapped->getReference(Person::class, $personId);
                
                $entity->setUpdater($personReference);
    
                if (! $entity->getCreator()) {
                    $entity->setCreator($personReference);
                }
            }
        }
        
        $this->wrapped->persist($entity);
    }

    /**
     * @return AuthService
     */
    public function getAuthService () {
        return $this->authService;
    }

    /**
     * @param AuthService $authService
     */
    public function setAuthService ($authService) {
        $this->authService = $authService;
    }
}