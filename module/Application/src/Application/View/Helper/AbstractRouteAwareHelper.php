<?php
namespace Application\View\Helper;

use Laminas\Router\RouteMatch;

abstract class AbstractRouteAwareHelper extends AbstractHelper {
    private $routeMatch;
    
    protected function prepareRouteMatch () {
        $this->setRouteMatch($this->getRouter()->match($this->getRequest()));
    }
    
    /**
     * @return RouteMatch
     */
    protected function getRouteMatch () {
        return $this->routeMatch;
    }
    
    /**
     * @param RouteMatch|null $routeMatch
     */
    protected function setRouteMatch ($routeMatch) {
        $this->routeMatch = $routeMatch;
    }
}