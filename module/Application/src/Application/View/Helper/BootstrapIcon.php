<?php
namespace Application\View\Helper;

class BootstrapIcon extends AbstractHelper {
    public function __invoke ($type = 'circle-o', $vendor = null, $extraClass = null, $tagName = null) {
        $tagName = $tagName ? : 'span';
        $vendor = $vendor ? : 'fa';
        return '<' . $tagName . ' class="' . $vendor . ' ' . $vendor . '-' . $type . ($extraClass ? ' ' . $extraClass : '') . '"></' . $tagName . '>';
    }
}