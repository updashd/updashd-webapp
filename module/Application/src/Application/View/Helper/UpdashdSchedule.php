<?php
namespace Application\View\Helper;

use Updashd\Scheduler\ScheduleType;

class UpdashdSchedule extends AbstractHelper {
    public function __invoke ($type, $value) {
        $view = $this->getView();

        if ($type == ScheduleType::TYPE_SECOND_INTERVAL) {
            return $view->UpdashdDateDiff()->formatSecondInterval($value);
//            return $value . ' Second' . ($value != 1 ? 's' : '');
        }
        elseif ($type == ScheduleType::TYPE_CRON) {
            return 'Cron: ' . $value;
        }
        elseif ($type == ScheduleType::TYPE_ONCE) {
            return 'Once';
        }
        else {
            return 'Unknown Schedule Type';
        }
    }
}