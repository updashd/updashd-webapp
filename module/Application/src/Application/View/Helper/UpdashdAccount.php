<?php
namespace Application\View\Helper;

use Application\Service\ServiceInterface\AuthServiceInterface;
use Application\Service\ServiceInterface\DoctrineManagerInterface;
use Application\Service\ServiceTrait\AuthServiceTrait;
use Application\Service\ServiceTrait\DoctrineManagerTrait;
use Updashd\Model\Account;

class UpdashdAccount extends AbstractRouteAwareHelper  implements AuthServiceInterface, DoctrineManagerInterface  {
    use AuthServiceTrait;
    use DoctrineManagerTrait;

    private $account = null;

    public function __invoke ($account = null) {
        $this->prepareRouteMatch();
        
        if (! $account) {
            $account = $this->getActiveAccount();
        }

        $this->setAccount($account);
        
        return $this;
    }

    public function getAccountBySlug ($accountSlug) {
        return $this->getAuthService()->getAccount($accountSlug, $this->getDoctrineManager());
    }
    
    /**
     * @return null|object|Account
     */
    public function getActiveAccount () {
        $routeMatch = $this->getRouteMatch();
        
        // There was no match
        if (! $routeMatch) {
            return null;
        }
        
        $account = $routeMatch->getParam('account');
        
        if ($account) {
            return $this->getAccountBySlug($account);
        }
        
        return null;
    }

    public function getAccountLogoUrl () {
        if ($this->getAccount() && $this->getAccount()->getLogoUrl()) {
            return $this->getAccount()->getLogoUrl();
        }
        else {
            return '/img/noaccountlogo.jpg';
        }
    }
    
    /**
     * @return Account[]
     */
    public function getAccounts () {
        return $this->getAuthService()->getAccounts($this->getDoctrineManager());
    }

    /**
     * @return Account
     */
    public function getAccount () {
        return $this->account;
    }

    /**
     * @param Account $account
     */
    public function setAccount ($account) {
        $this->account = $account;
    }
}