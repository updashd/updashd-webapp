<?php
namespace Application\View\Helper;

use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Interop\Container\Exception\NotFoundException;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\AbstractFactoryInterface;

class AbstractFactory implements AbstractFactoryInterface {
    /**
     * Can the factory create an instance for the service?
     *
     * @param  ContainerInterface $container
     * @param  string $requestedName
     * @return bool
     */
    public function canCreate (ContainerInterface $container, $requestedName) {
        $className = ucfirst($requestedName);
        $fullyQualified = 'Application\\View\\Helper\\' . $className;

        if (class_exists($fullyQualified)) {
            return true;
        }

        return false;
    }

    /**
     * Create an object
     *
     * @param  ContainerInterface $container
     * @param  string $requestedName
     * @param  null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service.
     * @throws ServiceNotCreatedException if an exception is raised when
     *     creating a service.
     * @throws ContainerException if any other error occurs
     */
    public function __invoke (ContainerInterface $container, $requestedName, array $options = null) {
        $className = ucfirst($requestedName);
        $fullyQualified = 'Application\\View\\Helper\\' . $className;

        $class = new \ReflectionClass($fullyQualified);

        $interfaces = $class->getInterfaces();

        $instance = new $fullyQualified();

        foreach ($interfaces as $interface) {
            $namespaceName = $interface->getNamespaceName();
            $interfaceName = $interface->getName();


            // Only care about matching traits
            if (preg_match('/ServiceInterface$/', $namespaceName) && preg_match('/Interface/', $interfaceName)) {
                $hasMatch = preg_match('/([A-Za-z]*)Interface$/', $interfaceName, $matches);

                if (! $hasMatch) {
                    throw new ServiceNotCreatedException('Cannot determine service name from interface. ' . $interfaceName);
                }

                $serviceName = $matches[1];

                try {
                    $setMethod = $class->getMethod('set' . $serviceName);
                }
                catch (\ReflectionException $e) {
                    throw new ServiceNotCreatedException('Interface for service found, but no setter method exists');
                }

                $parameters = $setMethod->getParameters();

                $requestedService = null;

                foreach ($parameters as $parameter) {
                    $requestedService = $parameter->getClass()->getName();
                }

                if (! $requestedService) {
                    throw new ServiceNotCreatedException('Setter for service exists, but setter method does not accept service class.');
                }

                try {
                    $setMethod->invoke($instance, $container->get($requestedService));
                }
                catch (NotFoundException $e) {
                    throw new ServiceNotCreatedException('Could not get a service for ' . $requestedService);
                }
            }
        }

        // Inject stuff for the abstract helper
        if ($instance instanceof AbstractHelper) {
            $instance->setRequest($container->get('request'));
            $instance->setRouter($container->get('router'));
        }

        return $instance;
    }
}