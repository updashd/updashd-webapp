<?php
namespace Application\View\Helper;

use Laminas\Http\Request;
use Laminas\Mvc\Console\View\Renderer;
use Laminas\Router\RouteStackInterface;

abstract class AbstractHelper extends \Laminas\View\Helper\AbstractHelper {
    /**
     * RouteStackInterface instance.
     *
     * @var RouteStackInterface
     */
    protected $router;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @return RouteStackInterface
     */
    public function getRouter () {
        return $this->router;
    }

    /**
     * @param RouteStackInterface $router
     */
    public function setRouter ($router) {
        $this->router = $router;
    }

    /**
     * @return Request
     */
    public function getRequest () {
        return $this->request;
    }

    /**
     * @param Request $request
     */
    public function setRequest ($request) {
        $this->request = $request;
    }
    
    /**
     * Get the view object
     *
     * @return null|Renderer|object
     */
    public function getView () {
        return $this->view;
    }
}