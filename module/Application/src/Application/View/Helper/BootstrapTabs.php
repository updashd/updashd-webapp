<?php
namespace Application\View\Helper;

class BootstrapTabs extends AbstractHelper {
    public function __invoke ($panes = null, $activeId = null, $useFade = true, $extraClass = null) {
        if ($panes) {
            return $this->add($panes, $activeId, $useFade, $extraClass);
        }
        else {
            return $this;
        }
    }
    
    /**
     * [
     *     ['id' => 'pane1', 'title' => 'Pane 1 Title', 'content' => 'This is pane 1', 'fade' => true, 'icon' => '', 'label' => '', 'label_type' => ''],
     *     ...
     * ]
     *
     * @param array $panes the array structure for content
     * @param mixed $activeId the active tab id
     * @param bool $useFade whether to use fade or not
     * @param string $extraClass
     * @return string
     */
    public function add ($panes = null, $activeId = null, $useFade = true, $extraClass = null) {
        $html = $this->start($extraClass);
    
        $html .= $this->addTabs($panes, $activeId);
        
        $html .= $this->startContent($extraClass);
        
        $html .= $this->addTabPanes($panes, $activeId, $useFade);
        
        $html .= $this->endContent();
    
        $html .= $this->end();

        return  $html;
    }
    
    public function start ($extraClass = null) {
        return '<div class="nav-tabs-custom' . ($extraClass ? ' ' . $extraClass : '') . '">';
    }
    
    public function end () {
        return '</div>';
    }
    
    /**
     * Add the content for the tab area.
     * [
     *     ['id' => 'pane1', 'content' => 'This is pane 1', 'fade' => true],
     *     ...
     * ]
     * @param array $panes the array structure for content
     * @param mixed $activeId the active tab id
     * @param bool $useFade whether to use fade or not
     * @param string $extraClass extra class to add onto the content container div
     * @return string
     */
    public function addContent ($panes, $activeId = null, $useFade = true, $extraClass = null) {
        return $this->startContent($extraClass) . $this->addTabPanes($panes, $activeId, $useFade) . $this->endContent();
    }
    
    public function startContent ($extraClass = null) {
        return '<div class="tab-content' . ($extraClass ? ' ' . $extraClass : '') . '">';
    }
    
    public function endContent () {
        return '</div>';
    }
    
    /**
     * Add panes specified by an array.
     * [
     *     ['id' => 'pane1', 'content' => 'This is pane 1', 'fade' => true],
     *     ...
     * ]
     * @param array $panes the panes to display
     * @param mixed $activeId the active tab id
     * @param bool $useFade whether to use fade or not
     * @return string html for tab panes
     */
    public function addTabPanes ($panes, $activeId = null, $useFade = true) {
        $html = '';
    
        if (! $activeId) {
            $activeId = $panes[0]['id'];
        }
        
        foreach ($panes as $pane) {
            $id = isset($pane['id']) ? $pane['id'] : null;
            $content = isset($pane['content']) ? $pane['content'] : null;
            $useFade = isset($pane['fade']) ? $pane['fade'] : $useFade;
    
            $html .= $this->addTabPane($id, $content, ($id && $activeId == $id), $useFade);
        }
        
        return $html;
    }
    
    public function addTabPane ($id, $content, $isActive = false, $useFade = true) {
        return $this->startTabPane($id, $isActive, $useFade) . $content . $this->endTabPane();
    }
    
    public function startTabPane ($id, $isActive = false, $useFade = true) {
        return '<div class="tab-pane'
            . ($isActive ? ' active' : '')
            . ($isActive && $useFade ? ' in' : '')
            . ($useFade ? ' fade' : '')
            . '" id="' . $id . '">';
    }
    
    public function endTabPane () {
        return '</div>';
    }
    
    public function addTabs ($panes, $activeId = null) {
        $html = $this->startHeader();
        
        if (! $activeId) {
            $activeId = $panes[0]['id'];
        }

        foreach ($panes as $pane) {
            if (is_array($pane)) {
                $id = isset($pane['id']) ? $pane['id'] : null;
                $url = isset($pane['url']) ? $pane['url'] : null;
                $title = isset($pane['title']) ? $pane['title'] : null;
                $icon = isset($pane['icon']) ? $pane['icon'] : null;
                $label = isset($pane['label']) ? $pane['label'] : null;
                $labelType = isset($pane['label_type']) ? $pane['label_type'] : null;
                $isDisabled = isset($pane['disabled']) ? $pane['disabled'] : false;

                $html .= $this->addTab($id, $title, $url, ($id && $activeId == $id), $isDisabled, $label, $icon, $labelType);
            }
            else {
                // Swallow invalid stuff
            }
        }
    
        $html .= $this->endHeader();
        
        return $html;
    }

    public function startHeader ($extraClass = null) {
        return '<ul class="nav nav-tabs' . ($extraClass ? ' ' . $extraClass : '') . '">';
    }

    public function endHeader () {
        return '</ul>';
    }

    public function addTab ($id, $title, $url = null, $isActive = null, $isDisabled = false, $label = null, $icon = null, $labelType = null) {
        $url = $isDisabled ? '#' : $url;
        return $this->startTab($isActive, $isDisabled) . $this->getTabHtml($id, $url, $title, $label, $icon, $labelType) . $this->endTab();
    }

    public function startTab ($isActive = false, $isDisabled = false) {
        $classes = [];

        if ($isActive) {
            $classes[] = 'active';
        }

        if ($isDisabled) {
            $classes[] = 'disabled';
        }

        return '<li role="presentation"' . (count($classes) ? ' class="' . implode(' ', $classes). '"' : '') . '>';
    }

    public function endTab () {
        return '</li>';
    }

    public function getTabHtml($id, $url = null, $title, $label = null, $icon = null, $labelType = null) {
        if ($icon) {
            $title = '<i class="' . $icon . '"></i>' . $title;
        }

        if ($label) {
            $title = $title . '<span class="label label-' . ($labelType ? $labelType : 'primary'). ' pull-right">' . $label . '</span>';
        }

        $href = $url ? $url : '#' . $id;

        return '<a href="' . $href . '" ' . (! $url ? ' data-toggle="tab"' : '') . '>' . $title . '</a>';
    }
}