<?php
namespace Application\View\Helper;

use Application\Service\ServiceInterface\AuthServiceInterface;
use Application\Service\ServiceInterface\PersonSettingServiceInterface;
use Application\Service\ServiceTrait\AuthServiceTrait;
use Application\Service\ServiceTrait\PersonSettingServiceTrait;
use Updashd\Model\Person;

class UpdashdPerson extends AbstractHelper implements AuthServiceInterface, PersonSettingServiceInterface {
    use AuthServiceTrait;
    use PersonSettingServiceTrait;

    private $person = null;
    
    public function __invoke ($person = null) {
        if (! $person) {
            $person = $this->getAuthService()->getPerson();
        }
        
        $this->setPerson($person);

        return $this;
    }
    
    public function profilePicture () {
        // Default if there isn't a person or if there is no profile picture url
        if (! $this->getPerson() || ! $this->getPerson()->getProfilePictureUrl()) {
            return '/img/noprofilepicture.jpg';
        }
        
        return $this->getPerson()->getProfilePictureUrl();
    }

    public function username () {
        // Default if there isn't a person or if there is no profile picture url
        if (! $this->getPerson()) {
            return 'Unknown';
        }

        return $this->getPerson()->getUsername();
    }

    public function name () {
        // Default if there isn't a person or if there is no profile picture url
        if (! $this->getPerson()) {
            return 'Unknown';
        }

        return $this->getPerson()->getName();
    }

    public function createdDate ($format = 'M. Y') {
        // Default if there isn't a person or if there is no profile picture url
        if (! $this->getPerson()) {
            return 'Unknown';
        }

        return $this->getPerson()->getCreatedDate()->format($format);
    }

    public function timezone () {
        // Default if there isn't a person or if there is no profile picture url
        if (! $this->getPerson()) {
            return 'UTC';
        }

        return $this->setting('timezone');
    }

    public function setting($key) {
        $pss = $this->getPersonSettingService();

        return $pss->getPersonSetting($this->getPerson(), $key);
    }

    public function isAuthenticated () {
        return $this->person ? true : false;
    }

    /**
     * @return Person|null
     */
    public function getPerson () {
        return $this->person;
    }
    
    /**
     * @param Person|null $person
     */
    public function setPerson ($person) {
        $this->person = $person;
    }
}