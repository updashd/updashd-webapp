<?php
namespace Application\View\Helper;

use Application\Pagination\Paginator;

class BootstrapPaginator extends AbstractHelper {
    const OPTION_USE_PREVIOUS_BUTTON = 'use_previous_button';
    const OPTION_USE_NEXT_BUTTON = 'use_next_button';
    const OPTION_PREVIOUS_BUTTON_TEXT = 'previous_button_text';
    const OPTION_NEXT_BUTTON_TEXT = 'next_button_text';
    const OPTION_SIZE = 'size';
    
    const SIZE_NORMAL = '';
    const SIZE_LARGE = 'lg';
    const SIZE_SMALL = 'sm';
    
    private $options = [];
    
    public function __construct () {
        $this->setOptions([
            self::OPTION_USE_PREVIOUS_BUTTON => true,
            self::OPTION_USE_NEXT_BUTTON => true,
            self::OPTION_PREVIOUS_BUTTON_TEXT => '«',
            self::OPTION_NEXT_BUTTON_TEXT => '»',
            self::OPTION_SIZE => self::SIZE_NORMAL,
        ]);
    }

    /**
     * @param Paginator $paginator
     * @param int $showPages
     * @return $this|string
     */
    public function __invoke (Paginator $paginator = null, $showPages = 10) {
        if ($paginator) {
            return $this->fromPaginator($paginator, $showPages);
        }
        else {
            return $this;
        }
    }
    
    public function fromPaginator (Paginator $paginator = null, $showPages = 10) {
        return $this->fromPageNumbers($paginator->getPage(), $paginator->calcPageCount(), $showPages, $paginator->getParams());
    }
    
    public function fromPageNumbers ($currentPage, $totalPages, $showPages = 10, $queryParams = null) {
        $output = $this->addNumberedButton($currentPage, true, $queryParams);
    
        $showPages = $showPages >= 2 ? $showPages : 2;
    
        $leftBound = max(1, $currentPage - $showPages / 2);
        $rightBound = min($totalPages, $currentPage + $showPages / 2);
        
        // Buttons Less than Current Page (prepend)
        for ($i = $currentPage - 1; $i >= $leftBound; $i--) {
            $output = $this->addNumberedButton($i, false, $queryParams) . $output;
        }

        // Buttons Greater than current page (append)
        for ($i = $currentPage + 1; $i <= $rightBound; $i++) {
            $output = $output . $this->addNumberedButton($i, false, $queryParams) ;
        }

        // Prepend if available
        if ($currentPage > 1) {
            $output = $this->addPreviousButton($currentPage - 1, $queryParams) . $output;
        }

        if ($currentPage < $totalPages) {
            $output =  $output . $this->addNextButton($currentPage + 1, $queryParams);
        }
    
        $output = $this->start() . $output;
        $output = $output . $this->end();
    
        return $output;
    }
    
    public function start ($extraClasses = null) {
        return '<ul class="pagination ' .
            ($this->getOption('size') ? ' pagination-' . $this->getOption('size') : '') .
            ($extraClasses ? ' ' . $extraClasses : '') .
            '">';
    }
    
    public function end () {
        return '</ul>';
    }
    
    public function addPreviousButton ($pageNumber, $queryParams = null) {
        return $this->addButton($this->getUrl($pageNumber, $queryParams), $this->getOption(self::OPTION_PREVIOUS_BUTTON_TEXT));
    }
    
    public function addNextButton ($pageNumber, $queryParams = null) {
        return $this->addButton($this->getUrl($pageNumber, $queryParams), $this->getOption(self::OPTION_NEXT_BUTTON_TEXT));
    }
    
    public function addNumberedButton ($pageNumber, $isCurrent = false, $queryParams = null) {
        return $this->addButton($this->getUrl($pageNumber, $queryParams), $pageNumber, $isCurrent);
    }
    
    public function addButton ($url, $text, $isCurrent = false) {
        return '<li' . ($isCurrent ? ' class="active"' : '') . '><a href="' . $url . '">' . $text . '</a></li>';
    }
    
    public function getUrl ($pageNumber, $queryParams = null) {
        /** @var RouteUrl $routeUrlHelper */
        $routeUrlHelper = $this->getView()->routeUrl();

        $paginatorParams = ['page' => $pageNumber];

        if (is_array($queryParams)) {
            $paginatorParams = array_replace($queryParams, $paginatorParams);
        }

        return $routeUrlHelper->getUrl(null, $paginatorParams);
    }
    
    /**
     * @return array
     */
    public function getOptions () {
        return $this->options;
    }
    
    /**
     * @param array $options
     * @param bool $overRide True to replace the entire old settings, false to only replace certain indexes.
     */
    public function setOptions ($options, $overRide = false) {
        if (! $overRide) {
            $oldOptions = $this->options;
            $this->options = array_replace($oldOptions, $options);
        }
        else {
            $this->options = $options;
        }
    }
    
    /**
     * @param $key string
     * @param $value string
     */
    public function setOption ($key, $value) {
        $this->options[$key] = $value;
    }
    
    /**
     * @param $key
     * @param null $default
     * @return mixed
     */
    public function getOption ($key, $default = null) {
        if (isset($this->options[$key])) {
            return $this->options[$key];
        }
        else {
            return $default;
        }
    }
}