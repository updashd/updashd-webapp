<?php
namespace Application\View\Helper;

use Application\Service\ServiceInterface\LayoutServiceInterface;
use Application\Service\ServiceTrait\LayoutServiceTrait;

class UpdashdLayout extends AbstractHelper implements LayoutServiceInterface  {
    use LayoutServiceTrait;
    
    /**
     * @return \Application\Service\LayoutService
     */
    public function __invoke () {
        return $this->getLayoutService();
    }
}