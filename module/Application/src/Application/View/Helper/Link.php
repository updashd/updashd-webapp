<?php
namespace Application\View\Helper;

class Link extends AbstractHelper {
    public function __invoke ($text, $url, $class = null) {
        return '<a href="' . $url . '"' . ($class ? ' class="' . $class . '"' : '') . '>' . $text . '</a>';
    }
}