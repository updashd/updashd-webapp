<?php
namespace Application\View\Helper;

class LayoutInfoBox extends AbstractHelper {

    /**
     * @param string $text
     * @param mixed $number
     * @param string $color
     * @param string $largeIcon
     * @param string $largeIconVendor
     * @param int $progressPercent
     * @param string $progressText
     * @return $this|string
     */
    public function __invoke ($text = null, $number = null, $color = 'aqua',
                              $largeIcon = 'information-circled', $largeIconVendor = 'ion',
                              $progressPercent = null, $progressText = null) {
        if ($text) {
            return $this->add ($text, $number, $color, $largeIcon, $largeIconVendor,
                $progressPercent, $progressText);
        }
        else {
            return $this;
        }
    }

    /**
     * @param string $text
     * @param string $number
     * @param string $color
     * @param string $largeIcon
     * @param string $largeIconVendor
     * @param int $progressPercent
     * @param string $progressText
     * @return string
     */
    public function add ($text, $number = null, $color = 'aqua',
                         $largeIcon = 'information-circled', $largeIconVendor = 'ion',
                         $progressPercent = null, $progressText = null) {
        $html = '';

        $html .= $this->start($color);
        $html .= $this->getIconHtml($largeIcon, $largeIconVendor);
        $html .= $this->addContent($text, $number, $progressPercent, $progressText);
        $html .= $this->end();

        return $html;
    }

    public function start ($color = 'aqua') {
        return '<div class="info-box bg-' . $color . '">';
    }

    public function end () {
        return '</div>';
    }
    public function addContent($text, $number, $progressPercent = null, $progressText = null) {
        return
            $this->startContent()
            . $this->getTextHtml($text)
            . $this->getNumberHtml($number)
            . $this->getProgressHtml($progressPercent, $progressText)
            . $this->endContent();
    }

    public function startContent () {
        return '<div class="info-box-content">';
    }

    public function endContent () {
        return '</div>';
    }

    public function getTextHtml ($text) {
        return '<span class="info-box-text">' . $text . '</span>';
    }

    public function getNumberHtml ($number) {
        return '<span class="info-box-number">' . $number . '</span>';
    }

    public function getIconHtml ($largeIcon = 'information-circled', $largeIconVendor = 'ion') {
        $view = $this->getView();

        return '<span class="info-box-icon">' . $view->bootstrapIcon($largeIcon, $largeIconVendor) . '</span>';
    }


    public function getProgressHtml ($progressPercent = null, $progressText = null) {
        $view = $this->getView();

        $html = '';

        if ($progressPercent !== null) {
            $html .= $view->bootstrapProgress($progressPercent);
        }

        if ($progressText !== null) {
            $html .= $this->getProgressDescriptionHtml($progressText);
        }

        return $html;
    }

    public function getProgressDescriptionHtml ($progressText = null) {
        if ($progressText) {
            return '<span class="progress-description">' . $progressText . '</span>';
        }
        else {
            return '';
        }
    }
}