<?php
namespace Application\View\Helper;

class BootstrapDescription extends AbstractHelper {
    public function __invoke ($items, $isHorizontal = false) {
        return $this->addList($items, $isHorizontal);
    }
    
    public function addItem ($term, $definition) {
        return $this->addTerm($term) . $this->addDefinition($definition);
    }
    
    public function addList ($items, $isHorizontal = false) {
        $output = $this->startList($isHorizontal);
        
        $items = is_array($items) ? $items : [$items];
        
        foreach ($items as $term => $definition) {
            $output .= $this->addItem($term, $definition);
        }
        
        $output .= $this->endList();
        
        return $output;
    }
    
    public function startList ($isHorizontal = false) {
        return '<dl' . ($isHorizontal ? ' class="dl-horizontal"' : '') . '>';
    }
    
    public function endList () {
        return '</dl>';
    }
    
    public function addTerm ($term) {
        return $this->startTerm() . $term . $this->endTerm();
    }
    
    public function startTerm () {
        return '<dt>';
    }
    
    public function endTerm () {
        return '</dt>';
    }
    
    public function addDefinition ($definition) {
        return $this->startDefinition() . $definition . $this->endDefinition();
    }
    
    public function startDefinition () {
        return '<dd>';
    }
    
    public function endDefinition () {
        return '</dd>';
    }
}