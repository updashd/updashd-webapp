<?php
namespace Application\View\Helper;

class PrintArrayValue extends AbstractHelper {
    public function __invoke ($array, $key, $default = '') {
        return array_key_exists($key, $array) ? $array[$key] : $default;
    }
}