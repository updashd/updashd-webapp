<?php
namespace Application\View\Helper;

use Application\Service\ServiceInterface\CacheManagerInterface;
use Application\Service\ServiceInterface\DoctrineManagerInterface;
use Application\Service\ServiceTrait\CacheManagerTrait;
use Application\Service\ServiceTrait\DoctrineManagerTrait;
use Updashd\Model\Severity;

class UpdashdSeverity extends AbstractHelper implements DoctrineManagerInterface, CacheManagerInterface {
    use DoctrineManagerTrait;
    use CacheManagerTrait;

    const CACHE_KEY = 'severity-names';

    protected $severityArray;
    protected $severity;

    public function __invoke (Severity $severity) {
        if ($severity) {
            return $this->getLabel($severity);
        }
        else {
            return $this;
        }
    }

    public function getLabel (Severity $severity) {
        $view = $this->getView();

        return $view->bootstrapLabel($this->getSeverityName($severity), $this->getSeverityColor($severity));
    }

    protected function getSeverityField (Severity $severity, $key) {
        $severityArray = $this->getSeverityArray();

        $severityId = $severity->getSeverityId();

        return $severityArray[$severityId][$key];
    }

    protected function getSeverityName (Severity $severity) {
        return $this->getSeverityField($severity, 'name');
    }

    protected function getSeverityColor (Severity $severity) {
        return $this->getSeverityField($severity, 'color');
    }

    protected function retrieveSeverityArray () {
        $em = $this->getEntityManager();

        $severityRepo = $em->getRepository(Severity::class);

        /** @var Severity[] $severities */
        $severities = $severityRepo->findAll();

        $output = [];

        foreach ($severities as $severity) {
            $output[$severity->getSeverityId()] = [
                'name' => $severity->getSeverityName(),
                'color' => $severity->getBootstrapColor(),
            ];
        }

        return $output;
    }

    protected function getSeverityArray () {
        $cache = $this->getCacheManager()->getCache('Cache\Persistence');

        $item = $cache->getItem(self::CACHE_KEY);

        if (! $item) {
            $item = $this->retrieveSeverityArray();
            $cache->setItem(self::CACHE_KEY, $item);
        }

        return $item;
    }
}