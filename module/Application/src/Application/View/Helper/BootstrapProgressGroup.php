<?php
namespace Application\View\Helper;

class BootstrapProgressGroup extends AbstractHelper {

    public function __invoke ($text = null, $number = null, $percent = null, $color = 'primary', $striped = false, $animated = false, $extraClass = null) {
        if ($text !== null && $number !== null && $percent !== null) {
            return $this->add($text, $number, $percent, $color, $striped, $animated, $extraClass);
        }
        else {
            return $this;
        }
    }

    public function add ($text, $number, $percent, $color = 'primary', $striped = false, $animated = false, $extraClass = null) {
        return $this->start($extraClass)
            . $this->getTextHtml($text)
            . $this->getNumberHtml($number)
            . $this->getProgressBarHtml($percent, $color, $striped, $animated)
            . $this->end();
    }

    public function start ($extraClass = null) {
        return '<div class="progress-group' . ($extraClass ? ' ' . $extraClass : '') . '">';
    }

    public function end () {
        return '</div>';
    }

    public function getTextHtml ($text) {
        return '<span class="progress-text">' . $text . '</span>';
    }

    public function getNumberHtml ($number) {
        return '<span class="progress-number">' . $number . '</span>';
    }

    public function getProgressBarHtml ($percent, $color = 'primary', $striped = false, $animated = false) {
        $view = $this->getView();

        return $view->bootstrapProgress($percent, null, $color, $striped, $animated, 'sm');
    }
}
