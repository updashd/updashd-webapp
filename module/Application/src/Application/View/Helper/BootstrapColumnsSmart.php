<?php
namespace Application\View\Helper;

class BootstrapColumnsSmart extends AbstractHelper {
    public function __invoke ($items = null, $maxItemsPerRow = 3) {
        if ($items) {
            return $this->render($items, $maxItemsPerRow);
        }
        else {
            return $this;
        }
    }

    public function render ($items = null, $maxItemsPerRow = 3) {
        $html = '';

        $view = $this->getView();

        /** @var BootstrapColumns $ch */
        $ch = $view->bootstrapColumns();

        $itemCnt = count($items);

        $sqrt = sqrt($itemCnt);

        $itemsPerRow = ceil($sqrt);

        $itemsPerRow = $itemsPerRow < $maxItemsPerRow ? $itemsPerRow : $maxItemsPerRow;

        for ($i = 0; $i < $itemCnt;) {
            $itemsLeft = $itemCnt - $i;
            $itemsThisRow = $itemsLeft < $itemsPerRow ? $itemsLeft : $itemsPerRow;

            $html .= $ch->addRow(array_slice($items, $i, $itemsThisRow));

            $i += $itemsThisRow;
        }

        return $html;
    }
}