<?php
namespace Application\View\Helper;

class BootstrapColumns extends AbstractHelper {
    public function __invoke () {
        return $this;
    }

    public function addRow ($columns, $sizes = null) {
        $html = $this->startRow();

        $defaultWidth = 6;
        if (! $sizes || ! is_array($sizes) && count($columns)) {
            $defaultWidth = floor(12 / count($columns));

            if ($defaultWidth < 1) {
                $defaultWidth = 1;
            }
        }

        foreach ($columns as $id => $column) {
            $width = $defaultWidth;
            $responsiveSizes = null;

            if (isset($sizes[$id])) {
                if (is_array($sizes[$id])) {
                    $responsiveSizes = $sizes[$id];
                }
                else {
                    $width = $sizes[$id];
                }
            }

            $html .= $this->addColumn($width, $column, $responsiveSizes);
        }

        $html .= $this->endRow();

        return $html;
    }

    public function startRow ($extraClass = null) {
        return '<div class="row' . ($extraClass ? ' ' . $extraClass : '') . '">';
    }

    public function addColumn ($width, $content, $sizes = null) {
        return $this->startColumn($width, $sizes) . $content . $this->endColumn();
    }

    public function startColumn ($width = 6, $sizes = null, $extraClass = null) {
        if (! $sizes) {
            $sizes = [
                'xs' => 12,
                'sm' => $width
            ];
        }

        $html = '<div class="';

        $classes = [];
        foreach ($sizes as $sizeId => $sizeWidth) {
            $classes[] = 'col-' . $sizeId . '-' . $sizeWidth;
        }

        $html .= implode(' ', $classes);
        $html .= $extraClass ? ' ' . $extraClass : '';
        $html .= '">';

        return $html;
    }

    public function endColumn () {
        return '</div>';
    }

    public function endRow () {
        return '</div>';
    }
}