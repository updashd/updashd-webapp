<?php
namespace Application\View\Helper;

class BootstrapLabel extends AbstractHelper {
    public function __invoke ($text, $type = 'default', $extraClass = null) {
        return '<span class="label label-' . $type . ($extraClass ? ' ' . $extraClass : '') . '">' . $text . '</span>';
    }
}