<?php
namespace Application\View\Helper;

use Updashd\Model\Result as ResultEntity;
use Updashd\Model\ResultMetric;
use Updashd\Worker\Result as WorkerResult;

class UpdashdResultMetric extends AbstractHelper {
    /**
     * @param ResultMetric $resultMetric
     * @return UpdashdDate|string|null
     */
    public function __invoke ($resultMetric = null) {
        if (! $resultMetric) {
            return $this;
        }
        else {
            return $this->getResponse($resultMetric);
        }
    }

    /**
     * @param ResultMetric $resultMetric
     * @param int $precision
     * @return string
     */
    public function getResponse ($resultMetric, $precision = 3) {
        $metricType = $resultMetric->getMetricType()->getMetricTypeId();

        switch ($metricType) {
            case WorkerResult\Metric::TYPE_INT:
                return $resultMetric->getValueI();
            case WorkerResult\Metric::TYPE_FLOAT:
                return $this->round($resultMetric->getValueF(), $precision);
            case WorkerResult\Metric::TYPE_STRING:
                return $resultMetric->getValueS();
            case WorkerResult\Metric::TYPE_TEXT:
                return $resultMetric->getValueT();
            default: return 'Unknown Result Type';
        }
    }

    /**
     * @param ResultEntity $result
     * @param int $precision
     * @return string
     */
    public function getElapsed($result, $precision = 3) {
        return $this->round($result->getElapsedTime(), $precision);
    }

    protected function round($num, $precision = 3){
        return round($num, $precision);
    }
}