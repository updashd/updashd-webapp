<?php
namespace Application\View\Helper;

class UpdashdDateDiff extends AbstractHelper {

    /**
     * @param \DateTime $dateTimeFirst
     * @param \DateTime $dateTimeSecond
     * @param bool $firstOnly
     * @param bool $ago
     * @return string|$this
     */
    public function __invoke ($dateTimeFirst = null, $dateTimeSecond = null, $firstOnly = false, $ago = false) {
        if (! $dateTimeFirst instanceof \DateTime || ! $dateTimeSecond instanceof \DateTime) {
            return $this;
        }
        else {
            // Swap if out of order
            if ($dateTimeSecond < $dateTimeFirst) {
                $dt = $dateTimeFirst;
                $dateTimeFirst = $dateTimeSecond;
                $dateTimeSecond = $dt;
            }

            $dateTimeDiff = $dateTimeSecond->diff($dateTimeFirst);

            return $this->formatInterval($dateTimeDiff, $firstOnly, $ago);
        }
    }

    /**
     * @param \DateTime $dateTime
     * @param bool $firstOnly
     * @return string
     */
    public function formatAgo ($dateTime, $firstOnly = true) {
        $now = new \DateTime();

        $interval = $now->diff($dateTime);

        return $this->formatInterval($interval, $firstOnly, true);
    }

    /**
     * @param \DateTime $dateTime
     * @param bool $firstOnly
     * @return string
     */
    public function formatAgoDynamic ($dateTime, $firstOnly = true) {
        $view = $this->getView();
        $view
            ->inlineScript()
            ->prependFile($view->basePath('js/account/dynamicdateago.js'));

        $now = new \DateTime();

        $interval = $now->diff($dateTime);

        return '<span class="dynamic-date-ago" data-timestamp="' . $dateTime->getTimestamp() . '">' . $this->formatInterval($interval, $firstOnly, true) . '</span>';
    }

    /**
     * @param $seconds
     * @param bool $firstOnly
     * @param bool $ago
     * @return string
     */
    public function formatSecondInterval ($seconds, $firstOnly = false, $ago = false) {
        $now = new \DateTime();
        $now->setTimestamp($now->getTimestamp()); // For the time to be second aligned (fixes bug starting with 7.2.12)

        $later = new \DateTime();
        $later->setTimestamp($now->getTimestamp() + $seconds);

        $interval = $later->diff($now);

        return $this->formatInterval($interval, $firstOnly, $ago);
    }

    public function formatInterval (\DateInterval $dateInterval, $firstOnly = false, $ago = false) {
        $parts = [
            'y' => 'year',
            'm' => 'month',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second'
        ];

        $formatParts = [];

        foreach ($parts as $key => $name) {
            $amt = $dateInterval->$key;

            if ($amt) {
                $formatParts[] = '%' . $key . ' ' . $name . (abs($amt) != 1 ? 's' : '');
            }
        }

        if ($firstOnly && count($formatParts)) {
            $formatParts = [$formatParts[0]];
        }

        if (count($formatParts)) {
            return $dateInterval->format(implode(' ', $formatParts)) . ($ago ? ' ago' : '');
        }
        else {
            return 'N/A';
        }
    }


}