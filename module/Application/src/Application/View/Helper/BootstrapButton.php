<?php
namespace Application\View\Helper;

class BootstrapButton extends AbstractHelper {
    /**
     * @param null|string $text
     * @param null|string $url
     * @param string $type
     * @param null|string $extraClass
     * @return $this|string
     */
    public function __invoke ($text = null, $url = null, $type = 'default', $extraClass = null) {
        if ($text) {
            return $this->link($text, $url, $type, $extraClass);
        }
        else {
            return $this;
        }
    }

    public function link($text, $url, $type = 'default', $extraClass = null) {
        return '<a href="' . $url . '" class="btn btn-' . $type . ($extraClass ? ' ' . $extraClass : '') . '">' . $text . '</a>';
    }
}