<?php
namespace Application\View\Helper;

use Application\Form\AbstractForm;
use Laminas\Form\Element;
use Laminas\Form\Form;
use Laminas\Form\View\Helper\FormLabel;

class BootstrapForm extends AbstractHelper {
    const OPTION_LAYOUT = 'layout';
    const OPTION_PLACEHOLDER_AS_LABEL = 'placeholder_as_label';
    const OPTION_FORM_CLASS = 'form_class';
    const OPTION_LABEL_COLUMN_SIZE = 'label_column_size';
    const OPTION_ELEMENT_COLUMN_SIZE = 'element_column_size';
    const OPTION_GROUP_CLASS = 'group_class';
    const OPTION_CHECKBOX_LABEL_POS = 'checkbox_label_pos';

    const LAYOUT_HORIZONTAL = 'horizontal';
    const LAYOUT_VERTICAL = 'vertical';
    const LAYOUT_INLINE = 'inline';

    const CHECKOUT_LABEL_POS_LEFT = 'left';
    const CHECKOUT_LABEL_POS_RIGHT = 'right';

    private $form;
    private $options = [];

    public function __construct () {
        $this->setOptions([
            self::OPTION_LAYOUT => self::LAYOUT_HORIZONTAL,
            self::OPTION_PLACEHOLDER_AS_LABEL => false,
            self::OPTION_LABEL_COLUMN_SIZE => 4,
            self::OPTION_ELEMENT_COLUMN_SIZE => 8,
            self::OPTION_CHECKBOX_LABEL_POS => self::CHECKOUT_LABEL_POS_LEFT,
            self::OPTION_FORM_CLASS => '',
            self::OPTION_GROUP_CLASS => '',
        ]);
    }

    public function __invoke () {
        return $this;
    }

    public function start (AbstractForm $form) {
        $this->setForm($form);

        $form->prepare();

        $view = $this->getView();

        $layout = $this->getOption(self::OPTION_LAYOUT);

        if ($layout == self::LAYOUT_HORIZONTAL) {
            $this->addClassToElement($form, 'form-horizontal');
        }
        elseif ($layout == self::LAYOUT_INLINE) {
            $this->addClassToElement($form, 'form-inline');
        }
        elseif ($layout == self::LAYOUT_VERTICAL) {
            // Nothing to add
        }

        $this->addClassToElement($form, $this->getOption(self::OPTION_FORM_CLASS));

        $html = $view->form()->openTag($form);

        // Show error messages, if any
        if ($form->hasValidated()) {
            $errorCount = count($form->getMessages());

            if ($errorCount) {
                $messageHtml = 'Form contains ' . $errorCount . ($errorCount != 1 ? ' fields with errors': ' field with an error') . '. Please correct '. ($errorCount != 1 ? 'them': 'it') . '.';

                $messageHtml .= '<ul>';
                foreach ($form->getMessages() as $fieldName => $fieldErrors) {
                    foreach ($fieldErrors as $errorId => $errorText) {
                        $readableName = $form->get($fieldName)->getLabel();
                        $messageHtml .= '<li><strong>' . $readableName . '</strong>: ' . $errorText . '</li>';
                    }
                }
                $messageHtml .= '</ul>';

                $html .= $view->bootstrapAlert('Invalid', $messageHtml, 'warning');
            }
        }

        if ($form instanceof \Application\Form\Form) {
            $notices = $form->getNotices();

            foreach ($notices as $notice) {
                $html .= $view->bootstrapAlert($notice['title'], $notice['message'], $notice['type']);
            }
        }

        return $html;
    }

    public function end () {
        $view = $this->getView();

        return $view->form()->closeTag();
    }
    
    public function addGroupsAuto ($groupClass = '', $elementClass = '', $feedbackClass = '') {
        $form = $this->getForm();
        
        $html = '';
        
        foreach ($form->getElements() as $element) {
            $html .= $this->addGroup($element, $groupClass, $elementClass, $feedbackClass);
        }
        
        return $html;
    }

    public function addGroup ($elementNameOrElem, $groupClass = '', $elementClass = '', $feedbackClass = '') {
        if ($feedbackClass) {
            $groupClass = $this->combineClasses($groupClass, 'has-feedback');
        }

        $form = $this->getForm();

        /** @var Element $element */
        $element = null;
        $elementName = '';
        
        if ($elementNameOrElem instanceof Element) {
            $element = $elementNameOrElem;
            $elementName = $elementNameOrElem->getName();
        }
        else {
            $element = $form->get($elementNameOrElem);
            $elementName = $elementNameOrElem;
        }

        if (count($element->getMessages())) {
            $groupClass = $this->combineClasses($groupClass, 'has-error');
        }

        $html = $this->startGroup($groupClass);

        if ($this->getOption(self::OPTION_LAYOUT) != self::LAYOUT_HORIZONTAL)  {
            $html .= $this->getLabelHtml($elementName);
            $html .= $this->getElementHtml($elementName, $elementClass);
        }
        else {
            $html .= $this->addLabelColumn($elementName);
            $html .= $this->addElementColumn($elementName, $this->getOption(self::OPTION_ELEMENT_COLUMN_SIZE), $elementClass);
        }

        if ($feedbackClass) {
            $html .= '<span class="' . $feedbackClass . ' form-control-feedback"></span>';
        }

        $html .= $this->endGroup();

        return $html;
    }

    public function startGroup ($class = '') {
        return '<div class="form-group ' . $class . '">';
    }

    public function endGroup () {
        return '</div>';
    }

    public function addLabelColumn ($element, $colWidth = 4) {
        return $this->startLabelColumn($colWidth) . $this->getLabelHtml($element) . $this->endLabelColumn();
    }

    public function addLabelColumnHtml ($html, $colWidth = 4) {
        return $this->startLabelColumn($colWidth) . $html . $this->endLabelColumn();
    }

    public function startLabelColumn ($colWidth = 4) {
        return '<div class="col-xs-12 col-sm-' . $colWidth . '">';
    }

    public function endLabelColumn () {
        return '</div>';
    }

    public function getLabelHtml ($element) {
        $view = $this->getView();

        $html = '';

        $element = $this->getForm()->get($element);

        if (
            $element instanceof Element\Hidden /*||
            $element instanceof Element\Checkbox ||
            $element instanceof Element\Radio*/
        ) {
            // Don't add the class
        }
        else {
            $this->addClassToElementLabel($element, 'control-label');
        }

        if ($element->getLabel() && ! $this->getOption(self::OPTION_PLACEHOLDER_AS_LABEL)) {
            // Special treatment of checkbox labels
            if (
                $element instanceof Element\Checkbox &&
                ! $element instanceof Element\MultiCheckbox &&
                $this->getOption(self::OPTION_CHECKBOX_LABEL_POS) == self::CHECKOUT_LABEL_POS_RIGHT
            ) {
                // Don't add anything
            }
            else {
                $html .= $view->formLabel($element);
            }
        }

        return $html;
    }

    public function addElementColumnHtml ($html, $colWidth = 8) {
        return $this->startElementColumn($colWidth) . $html . $this->endElementColumn();
    }

    public function addElementColumn ($element, $colWidth = 8, $class = '') {
        return $this->startElementColumn($colWidth) . $this->getElementHtml($element, $class) . $this->endElementColumn();
    }

    public function startElementColumn ($colWidth = 8) {
        return '<div class="col-xs-12 col-sm-' . $colWidth . '">';
    }

    public function endElementColumn () {
        return '</div>';
    }

    public function getElementHtml ($element, $class = '') {
        $view = $this->getView();

        $html = '';

        $element = $this->getForm()->get($element);

        if ($class) {
            $this->addClassToElement($element, $class);
        }

        if ($this->getOption(self::OPTION_PLACEHOLDER_AS_LABEL)) {
            $element->setAttribute('placeholder', $element->getLabel());
        }

        if (
            $element instanceof Element\Hidden
        ) {
            $html .= $view->formElement($element);
        }
        elseif ($element instanceof Element\MultiCheckbox) {
            /** @var \Laminas\Form\View\Helper\FormMultiCheckbox $helper */
            $helper = $view->formMultiCheckbox();
            $helper->setSeparator('<br />');
            $html .= $helper->render($element);
        }
        elseif ($element instanceof Element\Checkbox) {

            if ($this->getOption(self::OPTION_CHECKBOX_LABEL_POS) == self::CHECKOUT_LABEL_POS_RIGHT) {
                $element->setAttribute('id', $element->getName());
                $html .= $view->formLabel($element, $view->formElement($element) . ' ', FormLabel::APPEND);
            }
            else {
                $html .= $view->formElement($element);
            }
        }
        elseif ($element instanceof Element\Radio) {
            $html .= $view->formElement($element);
        }
        elseif ($element instanceof Element\Button || $element instanceof Element\Submit) {
            $this->addClassToElement($element, 'btn');
            $html .= $view->formElement($element);
        }
        else {
            $this->addClassToElement($element, 'form-control');
            $html .= $view->formElement($element);
        }

        $messages = $element->getMessages();

        if (count($messages)) {
            if ($this->getOption(self::OPTION_LAYOUT) == self::LAYOUT_HORIZONTAL) {
                $html .=  '<div class="col-xs-12">';
            }

            $html .= '<div class="help-block"><ul>';

            foreach ($messages as $message) {
                $html .= '<li>' . $message . '</li>';
            }

            $html .= '</div></ul>';

            if ($this->getOption(self::OPTION_LAYOUT) == self::LAYOUT_HORIZONTAL) {
                $html .=  '</div>';
            }
        }

        return $html;
    }

    /**
     * @param string|Element|Form $element
     * @param $classes
     */
    private function addClassToElement($element, $classes) {
        if (is_string($element)) {
            $element = $this->getForm()->get($element);
        }

        $existingClasses = $element->getAttribute('class');
        $combined = $this->combineClasses($existingClasses, $classes);
        $element->setAttribute('class', $combined);
    }

    /**
     * @param Element|Form $element
     * @param $classes
     */
    private function addClassToElementLabel($element, $classes) {
        $attributes = $element->getLabelAttributes();
        $existingClasses = isset($attributes['class']) ? $attributes['class'] : '';
        $combined = $this->combineClasses($existingClasses, $classes);
        $attributes['class'] = $combined;
        $element->setLabelAttributes($attributes);
    }

    public function combineClasses ($original, $additional) {
        $original = trim($original);
        $additional = trim($additional);

        $existingClasses = preg_split('/(\s+)/', $original);

        $newClasses = null;
        if (is_string($additional)) {
            $newClasses = preg_split('/(\s+)/', $additional);
        }
        else {
            $newClasses = $additional;
        }

        return implode(' ', array_merge($existingClasses, $newClasses));
    }

    /**
     * @return AbstractForm
     */
    private function getForm () {
        return $this->form;
    }

    /**
     * @param AbstractForm $form
     */
    private function setForm ($form) {
        $this->form = $form;
    }

    /**
     * @return array
     */
    public function getOptions () {
        return $this->options;
    }

    /**
     * @param array $options
     * @param bool $overRide True to replace the entire old settings, false to only replace certain indexes.
     */
    public function setOptions ($options, $overRide = false) {
        if (! $overRide) {
            $oldOptions = $this->options;
            $this->options = array_replace($oldOptions, $options);
        }
        else {
            $this->options = $options;
        }
    }

    /**
     * @param $key string
     * @param $value string
     */
    public function setOption ($key, $value) {
        $this->options[$key] = $value;
    }

    /**
     * @param $key
     * @param null $default
     * @return mixed
     */
    public function getOption ($key, $default = null) {
        if (isset($this->options[$key])) {
            return $this->options[$key];
        }
        else {
            return $default;
        }
    }
}