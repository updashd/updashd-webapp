<?php
namespace Application\View\Helper;

class BootstrapTable extends AbstractHelper {
    const OPTION_USE_RESPONSIVE_WRAPPER = 'use_responsive_wrapper';

    private $options = [];

    public function __construct () {
        $this->setOptions([
            self::OPTION_USE_RESPONSIVE_WRAPPER => true,
        ]);
    }

    public function __invoke () {
        return $this;
    }

    // <table class="table">
    public function start ($additionalClasses = '', $useResponsive = null) {
        if ($useResponsive !== null) {
            $this->setOption(self::OPTION_USE_RESPONSIVE_WRAPPER, $useResponsive);
        }

        $html = '<table class="table ' . $additionalClasses . '">';

        if ($this->getOption(self::OPTION_USE_RESPONSIVE_WRAPPER)) {
            $html = '<div class="table-responsive">' . $html;
        }

        return $html;
    }

    // </table>
    public function end () {
        $html = '</table>';

        if ($this->getOption(self::OPTION_USE_RESPONSIVE_WRAPPER)) {
            $html = $html . '</div>';
        }

        return $html;
    }

    // <tr><td></td><td></td></tr>
    public function addRow (array $columns) {
        $html = $this->startRow();

        foreach ($columns as $column) {
            $html .= $this->addCell($column);
        }

        $html .= $this->endRow();

        return $html;
    }

    // <tr>
    public function startRow () {
        return '<tr>';
    }

    // </tr>

    public function addCell ($contents, $additionalClasses = '') {
        return $this->startCell($additionalClasses) . $contents . $this->endCell();
    }

    // <td>contents</td>

    public function startCell ($additionalClasses = '') {
        return '<td' . ($additionalClasses ? ' class="' . $additionalClasses . '"' : '') . '>';
    }

    // <td>

    public function endCell () {
        return '</td>';
    }

    // </td>

    public function endRow () {
        return '</tr>';
    }

    // <tr><th></th><th></th></tr>

    public function addHeadings (array $headings, $additionalClasses = '') {
        $html = $this->startRow();

        foreach ($headings as $heading) {
            $html .= $this->addHeadingCell($heading, $additionalClasses);
        }

        $html .= $this->endRow();

        return $html;
    }

    // <th>content</td>
    public function addHeadingCell ($content, $additionalClasses = '') {
        return $this->startHeadingCell($additionalClasses) . $content . $this->endHeadingCell();
    }

    // <th>
    public function startHeadingCell ($additionalClasses = '') {
        return '<th' . ($additionalClasses ? ' class="' . $additionalClasses . '"' : '') . '>';
    }

    // </th>
    public function endHeadingCell () {
        return '</th>';
    }

    // <thead>
    public function startHead () {
        return '<thead>';
    }

    // </thead>
    public function endHead () {
        return '</thead>';
    }

    // <tbody>
    public function startBody () {
        return '<tbody>';
    }

    // </tbody>
    public function endBody () {
        return '</tbody>';
    }

    public function startFoot () {
        return '<tfoot>';
    }

    // </tfoot>
    public function endFoot () {
        return '</tfoot>';
    }

    /**
     * @return array
     */
    public function getOptions () {
        return $this->options;
    }

    /**
     * @param array $options
     */
    public function setOptions ($options) {
        $this->options = $options;
    }

    /**
     * @param $key string
     * @param $value string
     */
    public function setOption ($key, $value) {
        $this->options[$key] = $value;
    }

    /**
     * @param $key
     * @param null $default
     * @return mixed
     */
    public function getOption ($key, $default = null) {
        if (isset($this->options[$key])) {
            return $this->options[$key];
        }
        else {
            return $default;
        }
    }
}