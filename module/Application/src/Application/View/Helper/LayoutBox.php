<?php
namespace Application\View\Helper;

class LayoutBox extends AbstractHelper {
    
    /**
     * Add a box for the AdminLTE layout
     * @param string $content Body content (html)
     * @param string $type Type of box (primary, success, error, etc -- see Bootstrap colors)
     * @param string|null $bodyType Type of box (class name that is prefixed with box-)
     * @param string|null $bodyExtraClass A string of any other desired classes
     * @return string|LayoutBox
     */
    public function __invoke ($content = null, $type = 'primary', $bodyType = null, $bodyExtraClass = null) {
        if ($content) {
            return $this->add($content, $type, $bodyType, $bodyExtraClass);
        }
        else {
            return $this;
        }
    }
    
    /**
     * Add a box for AdminLTE
     * @param string $content content for the box (can be HTML)
     * @param string $type Type of box (primary, success, error, etc -- see Bootstrap colors)
     * @param string|null $bodyType Type of box (class name that is prefixed with box-)
     * @param string|null $bodyExtraClass A string of any other desired classes
     * @return string The HTML markup for the box
     */
    public function add ($content, $type = 'primary', $bodyType = null, $bodyExtraClass = null) {
        return $this->start($type) . $this->addBody($content, $bodyType, $bodyExtraClass) . $this->endBody();
    }
    
    /**
     * Begin a box for AdminLTE
     * @param string $type Type of box (primary, success, error, etc -- see Bootstrap colors)
     * @param null $otherClasses  string of any other desired classes
     * @return string
     */
    public function start ($type = 'primary', $otherClasses = null) {
        return '<div class="box box-' . $type . ($otherClasses ? ' ' . $otherClasses : '') . '">';
    }
    
    /**
     * End the box
     * @return string
     */
    public function end () {
        return '</div>';
    }
    
    /**
     * Add a header to the box
     * @param string $title The title to put in the header
     * @param string $class Classes to put on the header (with-border by default)
     * @param array $tools An array of tools. See docs for getHeaderToolsInnerHtml
     * @return string
     */
    public function addHeader ($title, $class = 'with-border', $tools = []) {
        return $this->startHeader($class) . $this->getHeaderHtml($title, $tools) . $this->endHeader();
    }
    
    /**
     * Start a header with the given class
     * @param string $class
     * @return string
     */
    public function startHeader ($class = 'with-border') {
        return '<div class="box-header ' . $class . '">';
    }
    
    /**
     * End the header
     * @return string
     */
    public function endHeader () {
        return '</div>';
    }
    
    /**
     * Get the HTML for a box header for AdminLTE
     * @param string $title The title text
     * @param array $tools The tools. See docs for getHeaderToolsInnerHtml
     * @return string
     */
    public function getHeaderHtml ($title, $tools = []) {
        return $this->getHeaderTitleHtml($title) . $this->getHeaderToolsHtml($tools);
    }
    
    /**
     * Get the actually title HTML for the header
     * @param string $title The title
     * @return string
     */
    public function getHeaderTitleHtml ($title) {
        return '<h3 class="box-title">' . $title . '</h3>';
    }
    
    /**
     * Get the HTML for the box tools
     * @param array $tools See docs for getHeaderToolsInnerHtml
     * @return string
     */
    public function getHeaderToolsHtml ($tools) {
        return '<div class="box-tools pull-right">' . $this->getHeaderToolsInnerHtml($tools) . '</div>';
    }
    
    /**
     * Get the HTML for the tool of a box header for AdminLTE
     * The tool array should look similar to one of these:
     *
     * Array (
     *      'type' => 'link',
     *      'url' => 'http://google.com/',
     *      'icon' => 'circle',
     *      'tooltip' => 'Go to Google'
     * )
     *
     * Array (
     *      'type' => 'button',
     *      'icon' => 'plus',
     *      'tooltip' => 'Toggle',
     *      'action' => 'collapse'
     * )
     *
     * Array (
     *      'type' => 'badge',
     *      'bg' => 'blue',
     *      'text' => '11 Unread'
     * )
     *
     * @param array $tools Array of Tool definitions (see main function description)
     * @return string
     */
    public function getHeaderToolsInnerHtml ($tools = []) {
        $html = '';

        foreach ($tools as $tool) {
            $type = isset($tool['type']) ? $tool['type'] : 'link';
            
            $url = isset($tool['url']) ? $tool['url'] : null;

            $icon = isset($tool['icon']) ? $tool['icon'] : 'circle';
            $iconHtml = '<i class="fa fa-' . $icon . '"></i>';

            $tooltip = isset($tool['tooltip']) ? $tool['tooltip'] : null;
            $additionalAttr = $tooltip ? ' data-toggle="tooltip" title="' . $tooltip . '"' : '';
            
            $action = isset($tool['action']) ? $tool['action'] : null;
            $additionalAttr .= $action ? ' data-widget="' . $action . '"' : '';
            
            if ($type == 'link') {
                $html .= '<a href="' . $url . '" class="btn btn-box-tool"' . $additionalAttr . '>' . $iconHtml. '</a>';
            }
            else if ($type == 'button') {
                $html .= '<button type="button" class="btn btn-box-tool"' . $additionalAttr . '>' . $iconHtml. '</button>';
            }
            else if ($type == 'badge') {
                $bg = isset($tool['bg']) ? $tool['bg'] : 'blue';
                $text = isset($tool['text']) ? $tool['text'] : $iconHtml;
                $html .= '<span class="badge bg-' . $bg . '" ' . $additionalAttr . '>' . $text. '</span>';
            }
        }

        return $html;
    }
    
    /**
     * Add the body of the box
     * @param string $content HTML content to display
     * @param string|null $type The type of body to use
     * @param string|null $extraClass Any extra classes that need to be added to the body
     * @return string
     */
    public function addBody ($content, $type = null, $extraClass = null) {
        return $this->startBody($type, $extraClass) . $content . $this->endBody();
    }
    
    /**
     * @param string|null $type The type of body to use
     * @param string|null $extraClass Any extra classes that need to be added to the body
     * @return string
     */
    public function startBody ($type = null, $extraClass = null) {
        return '<div class="box-body' . ($type ? ' box-' . $type : '') . ($extraClass ? ' ' . $extraClass : '') . '">';
    }
    
    /**
     * End the body
     * @return string
     */
    public function endBody () {
        return '</div>';
    }
    
    /**
     * Add a footer
     * @param string $content
     * @return string
     */
    public function addFooter ($content) {
        return $this->startFooter() . $content . $this->endFooter();
    }
    
    /**
     * Start footer
     * @return string
     */
    public function startFooter () {
        return '<div class="box-footer">';
    }
    
    /**
     * End Footer
     * @return string
     */
    public function endFooter () {
        return '</div>';
    }
}