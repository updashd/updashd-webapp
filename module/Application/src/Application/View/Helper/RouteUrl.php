<?php
namespace Application\View\Helper;

use Application\Menu\Item;
use Application\Service\ServiceInterface\MenuInterface;
use Application\Service\ServiceTrait\MenuTrait;

class RouteUrl extends AbstractRouteAwareHelper implements MenuInterface {
    use MenuTrait;

    public function __invoke ($action = null, $getParams = [], $controllerName = null, $routeName = null, $routeParams = []) {
        $this->prepareRouteMatch();

        if ($action) {
            return $this->getUrl($action, $getParams, $controllerName, $routeName, $routeParams);
        }
        else {
            return $this;
        }
    }

    public function getRouteParam ($key, $default = null) {
        $routeMatch = $this->getRouteMatch();

        return $routeMatch->getParam($key, $default);
    }

    public function getUrl ($action = null, $getParams = [], $controllerName = null, $routeName = null, $routeParams = [], $reuseParams = true) {
        $routeMatch = $this->getRouteMatch();

        $controllerName = $controllerName ?: ($routeMatch->getParam('_controller') ?: $routeMatch->getParam('controller'));
        $routeName = $routeName ?: $routeMatch->getMatchedRouteName();

        if ($reuseParams && $routeMatch) {
            $routeParams = array_replace_recursive($routeParams, $routeMatch->getParams());
        }

        // Replace any given parameters with the ones from the call
        
        // Controller
        if ($controllerName) {
            $routeParams = array_replace_recursive($routeParams, ['controller' => $controllerName]);
        }
        
        // Action
        if ($action) {
            $routeParams = array_replace_recursive($routeParams, ['action' => $action]);
        }
        
        $url = $this->getRouter()->assemble($routeParams, ['name' => $routeName]);

        if (count($getParams)) {
            $url .= '?' . http_build_query($getParams);
        }

        return $url;
    }

    public function getUrlFromMenuItem (Item $item, $getParams = [], $params = [], $reuseParams = true) {
        if ($item->getUrl()) {
            return $item->getUrl();
        }

        if (! $item->isItem()) {
            return '#notitem';
        }

        if (! $item->getAction()) {
            return '#noaction';
        }

        if (! $item->getController()) {
            return '#nocontroller';
        }

        if (! $item->getRoute()) {
            return '#noroute';
        }

        return $this->getUrl($item->getAction(), $getParams, $item->getController(), $item->getRoute(), $params, $reuseParams);
    }
    
    public function getUrlFromMenuItemById($id, $getParams = [], $params = [], $reuseParams = true) {
        $menu = $this->getMenu();
        
        $item = $menu->getItemById($id);
        
        if ($item) {
            return $this->getUrlFromMenuItem($item, $getParams, $params, $reuseParams);
        }
        else {
            return '#notfound';
        }
    }
}