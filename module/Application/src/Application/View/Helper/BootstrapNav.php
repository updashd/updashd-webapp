<?php
namespace Application\View\Helper;

class BootstrapNav extends AbstractHelper {
    public function __invoke ($items = null, $activeItem = null, $type = 'tabs', $extraClass = null) {
        if ($items) {
            return $this->add($items, $activeItem, $type, $extraClass);
        }
        else {
            return $this;
        }
    }

    public function add ($items = null, $activeItem = null, $type = 'tabs', $extraClass = null) {
        $html = $this->start($type, $extraClass);

        foreach ($items as $item) {
            if (is_string($item)) {
                $html .= $this->addItem($item);
            }
            elseif (is_array($item)) {
                $id = isset($item['id']) ? $item['id'] : null;
                $url = isset($item['url']) ? $item['url'] : null;
                $content = isset($item['content']) ? $item['content'] : null;
                $icon = isset($item['icon']) ? $item['icon'] : null;
                $label = isset($item['label']) ? $item['label'] : null;
                $labelType = isset($item['label_type']) ? $item['label_type'] : null;

                $html .= $this->addItem($content, $url, ($id  && $activeItem == $id), $label, $icon, $labelType);
            }
            else {
                // Swallow invalid stuff
            }
        }

        $html .= $this->end();
        return  $html;
    }

    public function start ($type = 'tabs', $extraClass = null) {
        return '<ul class="nav nav-' . $type . ($extraClass ? ' ' . $extraClass : '') . '">';
    }

    public function end () {
        return '</ul>';
    }

    public function addItem ($content, $url = null, $isActive = null, $label = null, $icon = null, $labelType = null) {
        return $this->startItem($isActive) . $this->getItemHtml($content, $url, $label, $icon, $labelType) . $this->endItem();
    }

    public function startItem ($isActive = false) {
        return '<li role="presentation"' . ($isActive ? ' class="active"' : '') . '>';
    }

    public function endItem () {
        return '</li>';
    }

    public function getItemHtml($content, $url = null, $label = null, $icon = null, $labelType = null) {
        if ($icon) {
            $content = '<i class="' . $icon . '"></i>' . $content;
        }

        if ($label) {
            $content = $content . '<span class="label label-' . ($labelType ? $labelType : 'primary'). ' pull-right">' . $label . '</span>';
        }

        if ($url) {
            return '<a href="' . $url . '">' . $content . '</a>';
        }
        else {
            return $content;
        }
    }
}