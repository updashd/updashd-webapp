<?php
namespace Application\View\Helper;

class BootstrapAlert extends AbstractHelper {
    public function __invoke ($title, $message, $type = 'danger', $icon = null, $dismissible = false) {
        $html = '';

        if ($dismissible) {
            $html .= '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
        }

        $html .= '<div class="alert alert-' . $type . ($dismissible ? ' alert-dismissible' : '') . '">';

        if (! $icon) {
            switch ($type) {
                case 'success': $icon = 'fa fa-check'; break;
                case 'info': $icon = 'fa fa-info'; break;
                case 'warning': $icon = 'fa fa-warning'; break;
                case 'danger': $icon = 'fa fa-ban'; break;
            }
        }

        $iconHtml = '<i class="icon ' . $icon . '"></i>';

        $html .= '<h4>' . $iconHtml . $title . '</h4>';

        $html .= $message;

        $html .= '</div>';

        return $html;
    }
}