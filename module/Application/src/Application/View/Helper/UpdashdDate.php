<?php
namespace Application\View\Helper;

class UpdashdDate extends AbstractHelper {

    /**
     * @param \DateTime $dateTime
     * @param string $format
     * @return string
     */
    public function __invoke ($dateTime = null, $format = null) {
        $view = $this->getView();

        if ($dateTime === null) {
            return 'N/A';
        }
        else {
            $localTime = clone $dateTime;

            $timezone = $view->updashdPerson()->setting('timezone');

            if ($format === null) {
                $format = $view->updashdPerson()->setting('datetime_format');
            }

            $localTime->setTimezone(new \DateTimeZone($timezone));

            return $localTime->format($format);
        }
    }
}