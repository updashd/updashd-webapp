<?php
namespace Application\View\Helper;

use Application\Service\ServiceInterface\MenuInterface;
use Application\Service\ServiceTrait\MenuTrait;

class UpdashdMenu extends AbstractHelper implements MenuInterface {
    use MenuTrait;
    
    /**
     * @return \Application\Service\Menu
     */
    public function __invoke () {
        return $this->getMenu();
    }
}