<?php
namespace Application\View\Helper;

class LayoutSmallBox extends AbstractHelper {

    public function __invoke ($title = null, $content = null, $color = 'aqua',
                              $actionIcon = 'information-circled', $actionIconVendor = 'ion',
                              $footerText = 'More Info', $footerUrl = '#',
                              $footerIcon = 'arrow-right', $footerIconVendor = 'fa') {
        if ($content) {
            return $this->add ($title, $content, $color, $actionIcon, $actionIconVendor,
                $footerText, $footerUrl, $footerIcon, $footerIconVendor);
        }
        else {
            return $this;
        }
    }

    public function add ($title, $content = null, $color = 'aqua',
                         $actionIcon = 'information-circled', $actionIconVendor = 'ion',
                         $footerText = 'More Info', $footerUrl = '#',
                         $footerIcon = 'arrow-right', $footerIconVendor = 'fa') {
        $html = '';

        $html .= $this->start($color);
        $html .= $this->addInner($title, $content);
        $html .= $this->addIconContainer($actionIcon, $actionIconVendor);

        if ($footerText) {
            $html .= $this->addFooter($footerText, $footerUrl, $footerIcon, $footerIconVendor);
        }

        $html .= $this->end();

        return $html;
    }

    public function start ($color = 'aqua') {
        return '<div class="small-box bg-' . $color . '">';
    }

    public function end () {
        return '</div>';
    }

    public function addInner ($title, $content) {
        return $this->startInner() . $this->getHeaderHtml($title) . $this->getContentHtml($content) . $this->endInner();
    }

    public function startInner () {
        return '<div class="inner">';
    }

    public function endInner () {
        return '</div>';
    }

    public function getHeaderHtml ($title) {
        return '<h3>' . $title . '</h3>';
    }

    public function getContentHtml ($content) {
        return '<p>' . $content . '</p>';
    }

    public function addIconContainer ($icon = 'information-circled', $iconVendor = 'ion') {
        return $this->startIconContainer() . $this->getIconHtml($icon, $iconVendor) . $this->endIconContainer();
    }

    public function startIconContainer () {
        return '<div class="icon">';
    }

    public function endIconContainer () {
        return '</div>';
    }

    public function getIconHtml ($icon = 'information-circled', $iconVendor = 'ion') {
        $view = $this->getView();

        return $view->bootstrapIcon($icon, $iconVendor);
    }

    public function addFooter ($text, $url = '#', $icon = 'arrow-right', $iconVendor = 'fa') {
        return $this->getFooterHtml($text, $url, $icon, $iconVendor);
    }

    public function getFooterHtml ($text, $url = '#', $icon = 'arrow-right', $iconVendor = 'fa') {
        $view = $this->getView();

        return '<a href="' . $url . '" class="small-box-footer">' . $text . ' ' . $view->bootstrapIcon($icon, $iconVendor) . '</a>';
    }
}