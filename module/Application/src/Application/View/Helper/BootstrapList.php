<?php
namespace Application\View\Helper;

class BootstrapList extends AbstractHelper {
    public function __invoke ($items = null, $options = [], $extraClass = null) {
        if ($items) {
            return $this->add($items, $options, $extraClass);
        }
        else {
            return $this;
        }
    }

    public function add ($items = null, $options = [], $extraClass = null) {
        $html = $this->start($options, $extraClass);

        foreach ($items as $item) {
            if (is_array($item)) {
                $id = isset($item['id']) ? $item['id'] : null;
                $heading = isset($item['heading']) ? $item['heading'] : null;
                $context = isset($item['context']) ? $item['context'] : null;
                $text = isset($item['text']) ? $item['text'] : null;
                $icon = isset($item['icon']) ? $item['icon'] : null;
                $label = isset($item['label']) ? $item['label'] : null;
                $labelType = isset($item['label_type']) ? $item['label_type'] : null;
                $extraItemClass = isset($item['class']) ? $item['class'] : null;

                $html .= $this->addItem($id, $text, $heading, $context, $extraItemClass, $icon, $label, $labelType);
            }
            else {
                // Swallow invalid stuff
            }
        }

        $html .= $this->end();
        return  $html;
    }

    public function start ($options = [], $extraClass = null) {
        $optionsStr = implode(' ', array_map(function($e){ return 'list-group-' . $e; }, $options));

        return '<ul class="list-group ' . $optionsStr . ($extraClass ? ' ' . $extraClass : '') . '">';
    }

    public function end () {
        return '</ul>';
    }

    public function addLinkItem ($id, $content, $link, $heading = null, $context = null, $extraClass = null, $icon = null, $label = null, $labelType = null) {
        return
            $this->startItem($id, $context, $extraClass, $link, 'a')
            . $this->getItemHtml($content, $heading, $icon, $label, $labelType)
            . $this->endItem('a');
    }

    public function addItem ($id, $content, $heading = null, $context = null, $extraClass = null, $icon = null, $label = null, $labelType = null) {
        return $this->startItem($id, $context, $extraClass) . $this->getItemHtml($content, $heading, $icon, $label, $labelType) . $this->endItem();
    }

    public function startItem ($id, $context = null, $extraClass = null, $link = null,  $type = 'li') {
        return
            '<' . $type
            . ($id ? ' id="' . $id . '"': '')
            . ' class="list-group-item '
            . ($context ? 'list-group-item-' . $context : '')
            . ($extraClass ?: '')
            . '"'
            . ($link ? 'href="' . $link . '"' : '')
            . '>';
    }

    public function endItem ($type = 'li') {
        return '</' . $type . '>';
    }

    public function getItemHtml($text, $heading = null, $icon = null, $label = null, $labelType = null) {
        if ($heading) {
            $content = $this->getItemHeaderHtml($heading, $icon);
            $content .= $this->getItemTextHtml($text);
        }
        else {
            $content = $this->getItemIcon($icon) . $text;
        }

        if ($label) {
            $content .= '<span class="label label-' . ($labelType ? $labelType : 'primary'). ' pull-right">' . $label . '</span>';
        }

        return $content;
    }

    public function getItemIcon ($icon) {
        $view = $this->getView();
        return $view->bootstrapIcon($icon, null, 'margin-r-5');
    }

    public function getItemHeaderHtml ($heading, $icon = null) {

        return '<h4 class="list-group-item-heading">' . ($icon ? $this->getItemIcon($icon) : '') . $heading . '</h4>';
    }

    public function getItemTextHtml ($text) {
        return '<p class="list-group-item-text">' . $text . '</p>';
    }
}