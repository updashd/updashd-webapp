<?php
namespace Application\View\Helper;

use Application\Service\ServiceInterface\JWTServiceInterface;
use Application\Service\ServiceInterface\LayoutServiceInterface;
use Application\Service\ServiceTrait\JWTServiceTrait;
use Application\Service\ServiceTrait\LayoutServiceTrait;
use Updashd\Model\NodeServiceZone;

class UpdashdNodeServiceZoneStatus extends AbstractHelper implements JWTServiceInterface, LayoutServiceInterface {
    use JWTServiceTrait;
    use LayoutServiceTrait;

    /**
     * @param int|NodeServiceZone $nodeServiceZoneOrId Either a NodeServiceZone or an id for one
     * @param null $extraClass
     * @param bool $tooltip
     * @return string
     */
    public function __invoke ($nodeServiceZoneOrId, $extraClass = null, $tooltip = false) {
        if ($nodeServiceZoneOrId instanceof NodeServiceZone) {
            return $this->byNodeServiceZone($nodeServiceZoneOrId, $extraClass, $tooltip);
        }
        else {
            return $this->byIds($nodeServiceZoneOrId, $extraClass, $tooltip);
        }
    }

//    /**
//     * @param NodeService $nodeService
//     * @param string $zoneId
//     * @param string$zoneName
//     * @param string $extraClass
//     * @param bool $tooltip
//     * @return string
//     */
//    public function byNodeService ($nodeService, $zoneId, $zoneName, $extraClass = null, $tooltip = false) {
//        return $this->byIds($nodeService->getNodeServiceId(), $zoneId, $zoneName, $extraClass, $tooltip);
//    }

    /**
     * @param NodeServiceZone $nodeServiceZone
     * @param string $extraClass
     * @param bool $tooltip
     * @return string
     */
    public function byNodeServiceZone ($nodeServiceZone, $extraClass = null, $tooltip = false) {
        return $this->byIds(
            $nodeServiceZone->getNodeServiceZoneId(),
            $extraClass,
            $tooltip
        );
    }

    /**
     * @param int $nodeServiceZoneId
     * @param string $extraClass
     * @param bool $tooltip
     * @return string
     */
    public function byIds($nodeServiceZoneId, $extraClass = null, $tooltip = false) {

        $jwtService = $this->getJwtService();
        $jwt = $jwtService->encode(['nszid' => $nodeServiceZoneId], $jwtService::PUSHER_KEY);

        $config = $this->getLayoutService()->getConfig();
        $server = $config['application']['pusher'];

        $view = $this->getView();

        $view->inlineScript()->appendFile($view->basePath('js/account/node_service_zone_status.js'));

        $html = '<span'
            . ' class="node-service-zone-status' . ($extraClass ? ' ' .$extraClass : '') . '"'
            . ' data-server="' . $server . '"'
            . ' data-node-service-zone-id="' . $nodeServiceZoneId . '"'
            . ' data-jwt="' . $jwt . '"'
            . '></span>';

        if ($tooltip) {
            $html = '<span data-toggle="tooltip" title="Tooltip">' . $html . '</span>';
        }

        return $html;
    }
}