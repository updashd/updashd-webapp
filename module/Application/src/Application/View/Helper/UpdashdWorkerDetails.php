<?php
namespace Application\View\Helper;

use Application\Service\ServiceInterface\WorkerServiceInterface;
use Application\Service\ServiceTrait\WorkerServiceTrait;
use Updashd\Model\ResultMetric;

class UpdashdWorkerDetails extends AbstractHelper implements WorkerServiceInterface {

    use WorkerServiceTrait;

    const FIELD_NAME = 'name';
    const FIELD_UNIT = 'unit';
    const FIELD_TYPE = 'type';

    /**
     * @param ResultMetric|null $resultMetric
     * @return $this|string
     */
    public function __invoke ($resultMetric = null) {
        if ($resultMetric) {
            return $this->getMetricName($resultMetric);
        }
        else {
            return $this;
        }
    }

    /**
     * @param ResultMetric $resultMetric
     * @param string $field One of the FIELD_* const
     * @return mixed|string
     */
    public function getMetricField ($resultMetric, $field) {
        $fieldName = $resultMetric->getFieldName();
        $moduleName = $resultMetric->getResult()->getNodeService()->getService()->getModuleName();

        return $this->getField($moduleName, $fieldName, $field);
    }

    /**
     * @param ResultMetric $resultMetric
     * @return mixed|string
     */
    public function getMetricName ($resultMetric) {
        return $this->getMetricField($resultMetric, self::FIELD_NAME);
    }

    /**
     * @param ResultMetric $resultMetric
     * @return mixed|string
     */
    public function getMetricUnit ($resultMetric) {
        return $this->getMetricField($resultMetric, self::FIELD_UNIT);
    }

    /**
     * @param ResultMetric $resultMetric
     * @return mixed|string
     */
    public function getMetricType ($resultMetric) {
        return $this->getMetricField($resultMetric, self::FIELD_TYPE);
    }

    /**
     * @param string $moduleName the worker module name
     * @param string $fieldKey the result metric field key
     * @param string $field One of the FIELD_* const
     * @return mixed|string
     */
    public function getField ($moduleName, $fieldKey, $field) {
        $workerService = $this->getWorkerService();

        $workerServiceResult = $workerService->getServiceWorkerResult($moduleName);

        $metric = $workerServiceResult->getMetric($fieldKey, false);

        if ($metric) {
            switch ($field) {
                case self::FIELD_NAME:
                    return $metric->getName();
                case self::FIELD_UNIT:
                    return $metric->getUnit();
                case self::FIELD_TYPE:
                    return $metric->getType();
            }
        }

        return 'Unknown';
    }

    public function getName ($moduleName, $fieldKey) {
        return $this->getField($moduleName, $fieldKey, self::FIELD_NAME);
    }

    public function getUnit ($moduleName, $fieldKey) {
        return $this->getField($moduleName, $fieldKey, self::FIELD_UNIT);
    }

    public function getType ($moduleName, $fieldKey) {
        return $this->getField($moduleName, $fieldKey, self::FIELD_TYPE);
    }
}