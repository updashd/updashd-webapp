<?php
namespace Application\View\Helper;

class BootstrapBadge extends AbstractHelper {
    public function __invoke ($text, $type = 'default', $extraClass = null) {
        return '<span class="badge bg-' . $type . ($extraClass ? ' ' . $extraClass : '') . '">' . $text . '</span>';
    }
}