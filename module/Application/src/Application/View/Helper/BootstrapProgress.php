<?php
namespace Application\View\Helper;

class BootstrapProgress extends AbstractHelper {

    /**
     * @param int $percent
     * @param string $text
     * @param string $type
     * @param bool $striped
     * @param bool $animated
     * @param string $extraClass
     * @return BootstrapProgress|string
     */
    public function __invoke ($percent = null, $text = null, $type = 'primary', $striped = false, $animated = false, $extraClass = null) {
        if ($percent !== null) {
            return $this->add($percent, $text, $type, $striped, $animated, $extraClass);
        }
        else {
            return $this;
        }
    }

    public function add ($percent = null, $text = null, $type = 'primary', $striped = false, $animated = false, $extraClass = null) {
        return $this->start($extraClass) . $this->addBar($percent, $text, $type, $striped, $animated) . $this->end();
    }

    public function start ($extraClass = null) {
        return '<div class="progress' . ($extraClass ? ' ' . $extraClass : '') . '">';
    }

    public function end () {
        return '</div>';
    }

    public function addBar ($percent, $text = null, $type = 'primary', $striped = false, $animated = false) {
        return $this->startBar($percent, $type, $striped, $animated) . $this->getBarText($text) . $this->endBar();
    }

    public function startBar ($percent, $type = 'primary', $striped = false, $animated = false) {
        return '<div class="progress-bar' . ($type ? ' progress-bar-' . $type : '') . ($striped ? ' progress-bar-striped' : '')  . ($animated ? ' active' : '') . '" style="width: ' . $percent . '%;">';
    }

    public function endBar () {
        return '</div>';
    }

    public function getBarText ($text = null) {
        return $text ? : '';
    }
}