<?php
namespace Application\Controller\Ajax;

use Application\Doctrine\SearchHelper;
use Application\Form\TrendFilter;
use Application\Service\ServiceInterface\DoctrineManagerInterface;
use Application\Service\ServiceInterface\WorkerServiceInterface;
use Application\Service\ServiceTrait\DoctrineManagerTrait;
use Application\Service\ServiceTrait\WorkerServiceTrait;
use Updashd\Model\NodeServiceZone;
use Laminas\View\Model\JsonModel;

class TrendController extends AbstractAjaxController implements
    DoctrineManagerInterface,
    WorkerServiceInterface {
    use DoctrineManagerTrait;
    use WorkerServiceTrait;

    protected $queryBuilder;
    protected $trendFilterForm;
    protected $searchHelper;

    public function init () {
        // Setup general query
        $em = $this->getEntityManager();

        $accountSlug = $this->getParam('account');

        if (! $accountSlug) {
            throw new \Exception('No account slug provided.');
        }

        $account = $this->getAuthService()->getAccount($accountSlug, $this->getDoctrineManager());

        if (! $account) {
            throw new \Exception('Account not found.');
        }

        $qb = $em->createQueryBuilder();

        $trendFilterForm = new TrendFilter($account->getSlug());

        $this->setTrendFilterForm($trendFilterForm);

        $qb->from(NodeServiceZone::class, 'nsz')
            ->innerJoin('nsz.nodeService', 'ns')
            ->innerJoin('ns.node', 'n')
            ->innerJoin('n.environment', 'e')
            ->innerJoin('n.account', 'a')
            ->innerJoin('ns.service', 's')
            ->innerJoin('s.serviceMetricFields', 'smf')
            ->innerJoin('nsz.zone', 'z');

        $sh = new SearchHelper($qb);

        if ($trendFilterForm->isDataValid($this->getQueryParam())) {
            $sh->addWhereCondition('e.environmentId', $trendFilterForm->getElementValue('environment'));
            $sh->addWhereCondition('z.zoneId', $trendFilterForm->getElementValue('zone'));
            $sh->addWhereCondition('n.nodeId', $trendFilterForm->getElementValue('node'));
            $sh->addWhereCondition('s.serviceId', $trendFilterForm->getElementValue('service'));
            $sh->addWhereCondition('ns.nodeServiceId', $trendFilterForm->getElementValue('node_service'));
            $sh->addWhereCondition('smf.fieldName', $trendFilterForm->getElementValue('field'));
        }

        $sh->addWhereCondition('a.accountId', $account->getAccountId());

        $this->setSearchHelper($sh);

        $this->setQueryBuilder($qb);
    }

    public function environmentsAction () {
        $qb = $this->getQueryBuilder();
        $qb
            ->addSelect('DISTINCT e.environmentId AS id')
            ->addSelect('e.environmentName AS text');

        return $this->getJsonResult();
    }

    public function zonesAction () {
        $qb = $this->getQueryBuilder();
        $qb
            ->addSelect('DISTINCT z.zoneId AS id')
            ->addSelect('z.zoneName AS text');

        return $this->getJsonResult();
    }

    public function nodesAction () {
        $qb = $this->getQueryBuilder();
        $qb
            ->addSelect('DISTINCT n.nodeId AS id')
            ->addSelect('CONCAT(n.nodeName, \' (\', e.environmentName, \')\') AS text');

        return $this->getJsonResult();
    }

    public function servicesAction () {
        $qb = $this->getQueryBuilder();
        $qb
            ->addSelect('DISTINCT s.serviceId AS id')
            ->addSelect('s.serviceName AS text');

        return $this->getJsonResult();
    }

    public function nodeServicesAction () {
        $qb = $this->getQueryBuilder();
        $qb
            ->addSelect('DISTINCT ns.nodeServiceId AS id')
            ->addSelect('CONCAT(ns.nodeServiceName, \' (\', s.serviceName, \')\') AS text');

        return $this->getJsonResult();
    }

    public function fieldsAction () {
        $qb = $this->getQueryBuilder();
        $qb
            ->addSelect('DISTINCT smf.fieldName AS id')
            ->addSelect('smf.readableName AS text');

        return $this->getJsonResult();
    }

    protected function getResult () {
        $sh = $this->getSearchHelper();

        $query = $sh->getQuery();

        return $query->getResult();
    }

    protected function getJsonResult () {
        return new JsonModel($this->getResult());
    }

    /**
     * @return \Doctrine\ORM\QueryBuilder
     */
    protected function getQueryBuilder () {
        return $this->queryBuilder;
    }

    /**
     * @param \Doctrine\ORM\QueryBuilder $queryBuilder
     */
    protected function setQueryBuilder ($queryBuilder) {
        $this->queryBuilder = $queryBuilder;
    }

    /**
     * @return TrendFilter
     */
    protected function getTrendFilterForm () {
        return $this->trendFilterForm;
    }

    /**
     * @param TrendFilter $trendFilterForm
     */
    protected function setTrendFilterForm (TrendFilter $trendFilterForm) {
        $this->trendFilterForm = $trendFilterForm;
    }

    /**
     * @return SearchHelper
     */
    public function getSearchHelper () {
        return $this->searchHelper;
    }

    /**
     * @param SearchHelper $searchHelper
     */
    public function setSearchHelper (SearchHelper $searchHelper) {
        $this->searchHelper = $searchHelper;
    }
}