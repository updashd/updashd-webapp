<?php
namespace Application\Controller\Ajax;

use Application\Controller\AbstractController;
use Application\Service\ServiceInterface\AuthServiceInterface;
use Application\Service\ServiceInterface\DoctrineManagerInterface;
use Application\Service\ServiceInterface\PersonSettingServiceInterface;
use Application\Service\ServiceInterface\PredisServiceInterface;
use Application\Service\ServiceTrait\AuthServiceTrait;
use Application\Service\ServiceTrait\DoctrineManagerTrait;
use Application\Service\ServiceTrait\PersonSettingServiceTrait;
use Application\Service\ServiceTrait\PredisServiceTrait;
use Updashd\Model\NodeServiceZone;
use Updashd\Scheduler\Scheduler;
use Laminas\View\Model\JsonModel;

class SchedulerController extends AbstractController implements DoctrineManagerInterface, PredisServiceInterface, AuthServiceInterface, PersonSettingServiceInterface {
    use DoctrineManagerTrait;
    use PredisServiceTrait;
    use AuthServiceTrait;
    use PersonSettingServiceTrait;

    public function checkStateAction () {
        $nszId = $this->getParam('nszid');

        $nodeServiceZone = $this->getNodeServiceZone($nszId);
        // TODO: Check permissions for access to the node
    
        $client = $this->getPredisService()->getClient();
        $scheduler = new Scheduler($client, $nodeServiceZone->getZone()->getZoneId());
        $task = $scheduler->getTaskEntity($nszId);
        
        $scheduledTime = $task->getScheduledTime();
        $lastCompletionTime = $task->getLastCompletionTime();

        return new JsonModel([
            'Zone' => $nodeServiceZone->getZone()->getZoneName(),
            'Enabled' => $task->getEnabled(),
            'LastCompletionTime' => $lastCompletionTime ? $this->getDate($lastCompletionTime) : null,
            'State' => $task->getState(),
            'ScheduledTime' => $scheduledTime ? $this->getDate($scheduledTime) : null,
            'RunCount' => $task->getRunCount()
        ]);
    }

    protected function getDate ($timestamp, $format = 'Y-m-d H:i:s') {
        $pss = $this->getPersonSettingService();

        $person = $this->getAuthService()->getPerson();

        $timezone = $pss->getPersonSetting($person,  'timezone');

        $format = $pss->getPersonSetting($person,  'datetime_format');

        $localTime = new \DateTime('now', new \DateTimeZone('UTC'));
        $localTime->setTimestamp($timestamp);

        $localTime->setTimezone(new \DateTimeZone($timezone));

        return $localTime->format($format);
    }

    /**
     * @param $nszId
     * @return NodeServiceZone|null
     */
    protected function getNodeServiceZone ($nszId) {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();
        $qb
            ->select('nsz, ns, z')
            ->from(NodeServiceZone::class, 'nsz')
            ->innerJoin('nsz.zone', 'z')
            ->innerJoin('nsz.nodeService', 'ns')
            ->where($qb->expr()->eq('nsz.nodeServiceZoneId', ':nszid'));

        $q = $qb->getQuery();
        $q->setParameter('nszid', $nszId);

        return $q->getOneOrNullResult();
    }
}