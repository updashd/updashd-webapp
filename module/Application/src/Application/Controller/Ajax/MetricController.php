<?php
namespace Application\Controller\Ajax;

use Application\Controller\AbstractController;
use Application\Service\ServiceInterface\DoctrineManagerInterface;
use Application\Service\ServiceTrait\DoctrineManagerTrait;
use Updashd\Model\ResultMetric;
use Updashd\Worker\Result\Metric;
use Laminas\View\Model\JsonModel;

class MetricController extends AbstractController implements DoctrineManagerInterface {
    use DoctrineManagerTrait;

    public function seriesAction () {
        $em = $this->getEntityManager();

        $slug = $this->getParam('slug');
        $fieldName = $this->getParam('field');
        $nodeServiceId = $this->getParam('nsid');

        if (! $slug || ! $fieldName || !$nodeServiceId) {
            return $this->jsonException();
        }

        $account = $this->getAuthService()->getAccount($slug, $this->getDoctrineManager());

        if (! $account) {
            return $this->jsonException();
        }

        $qb = $em->createQueryBuilder();
        $qb->select('r.startTime, r.endTime, r.elapsedTime, rm.fieldName, mt.metricTypeId, rm.valueI, rm.valueS, rm.valueF')
            ->from(ResultMetric::class, 'rm')
            ->innerJoin('rm.metricType', 'mt')
            ->innerJoin('rm.result', 'r')
            ->innerJoin('r.nodeService', 'ns')
            ->innerJoin('ns.node', 'n')
            ->innerJoin('n.account', 'a')
            ->where($qb->expr()->andX(
                $qb->expr()->eq('a.slug', ':slug'),
                $qb->expr()->eq('rm.fieldName', ':fieldName'),
                $qb->expr()->eq('ns.nodeServiceId', ':nodeServiceId')
            ))
            ->orderBy('r.startTime', 'ASC');

        $qb->setParameter('slug', $slug);
        $qb->setParameter('fieldName', $fieldName);
        $qb->setParameter('nodeServiceId', $nodeServiceId);

//        $qb->setMaxResults(10);
        $query = $qb->getQuery();

        $result = $query->getResult();

        $fields = [
            'date',
            'value'
        ];

        $data = [];

        foreach ($result as $row) {
            /** @var \DateTime $startTime */
            $startTime = $row['startTime'];
            $data[] = [
                $startTime ? $startTime->getTimestamp() * 1000 : null,
                $this->getMetricFromRow($row)
            ];
        }

        return new JsonModel([
            'fields' => $fields,
            'data' => $data
        ]);
    }

    public function pieAction () {
        $em = $this->getEntityManager();

        $slug = $this->getParam('slug');
        $fieldName = $this->getParam('field');
        $nodeServiceId = $this->getParam('nsid');
        $type = $this->getParam('type');

        if (! $slug || ! $fieldName || !$nodeServiceId || !$type) {
            return $this->jsonException();
        }

        $typeField = $this->getMetricFieldFromType($type);

        $account = $this->getAuthService()->getAccount($slug, $this->getDoctrineManager());

        if (! $account) {
            return $this->jsonException();
        }

        $qb = $em->createQueryBuilder();
        $qb
            ->addSelect('rm.' . $typeField . ' as val')
            ->addSelect('COUNT(rm.resultMetricId) as cnt')
            ->from(ResultMetric::class, 'rm')
            ->innerJoin('rm.metricType', 'mt')
            ->innerJoin('rm.result', 'r')
            ->innerJoin('r.nodeService', 'ns')
            ->innerJoin('ns.node', 'n')
            ->innerJoin('n.account', 'a')
            ->where($qb->expr()->andX(
                $qb->expr()->eq('a.slug', ':slug'),
                $qb->expr()->eq('rm.fieldName', ':fieldName'),
                $qb->expr()->eq('ns.nodeServiceId', ':nodeServiceId')
            ))
            ->groupBy('rm.' . $typeField);

        $qb->setParameter('slug', $slug);
        $qb->setParameter('fieldName', $fieldName);
        $qb->setParameter('nodeServiceId', $nodeServiceId);

        $query = $qb->getQuery();

        $result = $query->getResult();

        return new JsonModel($result);
    }

    /**
     * @param $row
     * @return int|float|string|null
     */
    protected function getMetricFromRow ($row) {
        $field = $this->getMetricFieldFromType($row['metricTypeId']);
        return $row[$field];
    }

    protected function getMetricFieldFromType ($type) {
        switch ($type) {
            case Metric::TYPE_INT: return 'valueI';
            case Metric::TYPE_FLOAT: return 'valueF';
            case Metric::TYPE_STRING: return 'valueS';
            case Metric::TYPE_TEXT: return 'valueT';
            default: return null;
        }
    }

    protected function jsonException () {
        return new JsonModel(null);
    }
}