<?php
namespace Application\Controller\Ajax;

use Application\Controller\AbstractController;
use Application\Service\ServiceInterface\DoctrineManagerInterface;
use Application\Service\ServiceTrait\DoctrineManagerTrait;
use Updashd\Model\Account;
use Laminas\View\Model\JsonModel;

class AccountController extends AbstractController implements DoctrineManagerInterface {
    use DoctrineManagerTrait;

    public function searchAction () {
        $searchQuery = $this->getParam('q');
        
        $em = $this->getEntityManager();
        
        $qb = $em->createQueryBuilder();
    
        $qb->select('a')
            ->from(Account::class, 'a')
            ->orderBy('a.name', 'ASC')
            ->orderBy('a.ownerId', 'ASC')
            ->orderBy('a.accountId', 'ASC')
            ->setMaxResults(20);
        
        // Search by ID
        if (preg_match('/^[0-9]+$/', $searchQuery)) {
            $qb->where('a.accountId = ?1');
            $qb->setParameter(1, $searchQuery);
        }
        // Search by Owner
//        elseif (preg_match('/[ ]/', $searchQuery)) {
//            $qb->where($qb->expr()->like('a.name', $qb->expr()->literal($searchQuery . '%')));
//        }
        // Search by Name
        else {
            $qb->where($qb->expr()->like('a.name', $qb->expr()->literal($searchQuery . '%')));
        }
        
        $query = $qb->getQuery();
        
        /** @var Account[] $accounts */
        $accounts = $query->getResult();
        
        $results = [];
        foreach ($accounts as $account) {
            $results[] = [
                'id' => $account->getAccountId(),
                'name' => $account->getName(),
                'owner' => $account->getOwner()->getName(),
            ];
        }
        
        return new JsonModel($results);
    }
}