<?php
namespace Application\Controller\Ajax;

use Application\Controller\AbstractController;
use Application\Service\ServiceInterface\DoctrineManagerInterface;
use Application\Service\ServiceTrait\DoctrineManagerTrait;

abstract class AbstractAjaxController extends AbstractController implements DoctrineManagerInterface {
    use DoctrineManagerTrait;
}