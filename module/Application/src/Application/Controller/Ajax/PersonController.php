<?php
namespace Application\Controller\Ajax;

use Application\Controller\AbstractController;
use Application\Service\ServiceInterface\DoctrineManagerInterface;
use Application\Service\ServiceTrait\DoctrineManagerTrait;
use Updashd\Model\Person;
use Laminas\View\Model\JsonModel;

class PersonController extends AbstractController implements DoctrineManagerInterface {
    use DoctrineManagerTrait;

    public function searchAction () {
        $searchQuery = $this->getParam('q');
        
        $em = $this->getEntityManager();
        
        $qb = $em->createQueryBuilder();
        
        $qb->select('p')
            ->from(Person::class, 'p')
            ->orderBy('p.name', 'ASC')
            ->orderBy('p.username', 'ASC')
            ->orderBy('p.personId', 'ASC')
            ->setMaxResults(20);
        
        // Search by ID
        if (preg_match('/^[0-9]+$/', $searchQuery)) {
            $qb->where('p.personId = ?1');
            $qb->setParameter(1, $searchQuery);
        }
        // Search by Name only
        elseif (preg_match('/[ ,]/', $searchQuery)) {
            $qb->where($qb->expr()->like('p.name', $qb->expr()->literal($searchQuery . '%')));
        }
        // Search by Username or Name
        else {
            $qb->where($qb->expr()->orX(
                $qb->expr()->like('p.username', $qb->expr()->literal($searchQuery . '%')),
                $qb->expr()->like('p.name', $qb->expr()->literal($searchQuery . '%'))
            ));
        }
        
        $query = $qb->getQuery();
        
        /** @var Person[] $people */
        $people = $query->getResult();
        
        $results = [];
        foreach ($people as $person) {
            $results[] = [
                'id' => $person->getPersonId(),
                'username' => $person->getUsername(),
                'name' => $person->getName()
            ];
        }
        
        return new JsonModel($results);
    }
}