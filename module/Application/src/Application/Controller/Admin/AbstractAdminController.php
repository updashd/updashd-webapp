<?php
namespace Application\Controller\Admin;

use Application\Controller\AbstractController;

abstract class AbstractAdminController extends AbstractController {
    public function init () {
        parent::init();
        
        // Set the admin menu to default
        $this->getMenu()->setDefaultItemById('backend');

        // Require admin privileges.
        $person = $this->getAuthService()->getPerson();

        if ($person->getRoleType() != 'ADMIN') {
            return $this->redirectTo('index', [], 'index', 'account');
        }
    }
}