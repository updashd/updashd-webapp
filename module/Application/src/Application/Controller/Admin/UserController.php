<?php
namespace Application\Controller\Admin;

use Application\Pagination\Paginator;
use Application\Service\ServiceInterface\DoctrineManagerInterface;
use Application\Service\ServiceTrait\DoctrineManagerTrait;
use Updashd\Model\Account;
use Updashd\Model\Person;
use Laminas\View\Model\ViewModel;

class UserController extends AbstractAdminController implements DoctrineManagerInterface {
    use DoctrineManagerTrait;

    public function indexAction () {
        return $this->redirectTo('list');
    }
    
    public function listAction () {
        $this->getLayoutService()->setTitle('User List');
        $this->getMenu()->setActiveId('admin-user-list');
    
        $em  = $this->getEntityManager();
    
        $accountQuery = $em->createQueryBuilder()
            ->select('p')
            ->from(Person::class, 'p')
            ->getQuery();
    
        $paginator = new Paginator($accountQuery, $this->getRequest()->getQuery());
    
        return new ViewModel(['paginator' => $paginator]);
    }
    
    public function addAction () {
        $this->getLayoutService()->setTitle('Add User');
        $this->getMenu()->setActiveId('admin-user-add');
        
        $form = new \Application\Form\Person();
        
        if ($this->isPost()) {
            $form->setData($this->getRequest()->getPost());
            if ($form->isValid()) {
                // Create person
                $user = new Person();

                // Hydrate entity from form
                $form->saveToEntity($user);

                // Save the entity
                $em = $this->getEntityManager();
                $em->persist($user);
                $em->flush();

                return $this->redirectTo('edit', ['pid' => $user->getPersonId()]);
            }
        }
        
        return new ViewModel([
            'form' => $form
        ]);
    }
    
    public function editAction () {
        $this->getLayoutService()->setTitle('Edit User');
        $this->getMenu()->setActiveId('admin-user-edit');
        
        /** @var \Laminas\Http\Request $request */
        $request = $this->getRequest();
        
        $pid = $request->getQuery()->get('pid');
        
        if (! $pid) {
            return $this->redirectTo('list');
        }

        $em = $this->getEntityManager();

        $personRepo = $em->getRepository(Person::class);
        $user = $personRepo->findOneBy(['personId' => $pid]);

        $accountRepo = $em->getRepository(Account::class);
        
        /** @var Account[] $userAccounts */
        $userAccounts = $accountRepo->findBy(['owner' => $user]);
        
        if (! $user) {
            throw new \Exception('User does not exist');
        }
        
        $form = new \Application\Form\Person();
        
        if ($this->isPost()) {
            $form->setData($this->getRequest()->getPost());
            
            if ($form->isValid()) {
                $form->saveToEntity($user);
                
                $em->persist($user);
                $em->flush();

                return $this->redirectTo('edit', ['pid' => $user->getPersonId()]);
            }
        }
        else {
            $form->hydrateFromEntity($user);
            $form->get('person_id')->setValue($pid);
        }
        
        return new ViewModel([
            'form' => $form,
            'person' => $user,
            'personAccounts' => $userAccounts
        ]);
    }
}
