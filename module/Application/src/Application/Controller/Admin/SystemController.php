<?php
namespace Application\Controller\Admin;

use Application\Service\ServiceInterface\AclServiceInterface;
use Application\Service\ServiceInterface\DoctrineManagerInterface;
use Application\Service\ServiceInterface\PredisServiceInterface;
use Application\Service\ServiceTrait\AclServiceTrait;
use Application\Service\ServiceTrait\DoctrineManagerTrait;
use Application\Service\ServiceTrait\PredisServiceTrait;
use Updashd\Model\Service;
use Updashd\Model\Zone;
use Updashd\Scheduler\Scheduler;
use Laminas\View\Model\ViewModel;

class SystemController extends AbstractAdminController implements AclServiceInterface, DoctrineManagerInterface, PredisServiceInterface {
    use AclServiceTrait;
    use DoctrineManagerTrait;
    use PredisServiceTrait;

    public function indexAction () {
        return $this->redirectTo('doctrine');
    }

    public function doctrineAction () {
        $this->getLayoutService()->setTitle('Doctrine');
        $this->getMenu()->setActiveId('admin-system-doctrine');

        $em = $this->getEntityManager();
        
        $config = $em->getConfiguration();
        $caches = [
            'Hydration' => $config->getHydrationCacheImpl(),
            'Metadata' => $config->getMetadataCacheImpl(),
            'Query' => $config->getQueryCacheImpl(),
            'Result' => $config->getResultCacheImpl(),
        ];
        
        return new ViewModel([
            'caches' => $caches
        ]);
    }

    public function schedulerAction () {
        $this->getLayoutService()->setTitle('Scheduler');
        $this->getMenu()->setActiveId('admin-system-scheduler');

        $em = $this->getEntityManager();
        $zqb = $em->createQueryBuilder();
        $zqb->select('z')
            ->from(Zone::class, 'z')
            ->orderBy('z.sortOrder');

        /** @var Zone[] $zones */
        $zones = $zqb->getQuery()->getResult();

        $sqb = $em->createQueryBuilder();
        $sqb->select('s')
            ->from(Service::class, 's')
            ->orderBy('s.sortOrder');

        /** @var Service[] $services */
        $services = $sqb->getQuery()->getResult();

        $stats = [];

        foreach ($zones as $zone) {
            $client = $this->getPredisService()->getClient();
            $zoneId = $zone->getZoneId();
            $scheduler = new Scheduler($client, $zoneId);

            $pending = [];

            foreach ($services as $service) {
                $pending[$service->getServiceName()] = $scheduler->getPendingSize($service->getModuleName());
            }

            $stats[$zone->getZoneName()] = [
                'Rescheduling' => $scheduler->getRescheduleSize(),
                'Scheduled' => $scheduler->getScheduleSize(),
                'Pending' => $pending,
                'Processing' => $scheduler->getProcessingSize(),
                'Complete' => $scheduler->getCompleteSize(),
                'Error' => $scheduler->getErrorSize(),
            ];
        }

        return new ViewModel([
            'stats' => $stats
        ]);
    }
}
