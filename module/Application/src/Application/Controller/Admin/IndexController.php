<?php
namespace Application\Controller\Admin;

class IndexController extends AbstractAdminController {
    public function indexAction () {
        return $this->redirectTo('index', [], 'dashboard');
    }
}