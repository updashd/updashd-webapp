<?php
namespace Application\Controller\Admin;

use Application\Pagination\Paginator;
use Application\Service\ServiceInterface\DoctrineManagerInterface;
use Application\Service\ServiceTrait\DoctrineManagerTrait;
use Application\Service\ServiceTrait\SlugHelperServiceTrait;
use Updashd\Model\Account;
use Updashd\Model\Account as AccountEntity;
use Updashd\Model\Person;
use Laminas\View\Model\ViewModel;

class AccountController extends AbstractAdminController implements DoctrineManagerInterface {
    use DoctrineManagerTrait;
    use SlugHelperServiceTrait;

    public function indexAction () {
        return $this->redirectTo('list');
    }

    public function listAction () {
        $this->getLayoutService()->setTitle('Account List');
        $this->getMenu()->setActiveId('admin-account-list');

        $em = $this->getEntityManager();
        
        $accountQuery = $em->createQueryBuilder()
            ->select('a')
            ->from(AccountEntity::class, 'a')
        ->getQuery();
        
        $paginator = new Paginator($accountQuery, $this->getRequest()->getQuery());

        return new ViewModel(['paginator' => $paginator]);
    }

    public function addAction () {
        $this->getLayoutService()->setTitle('Add Account');
        $this->getMenu()->setActiveId('admin-account-add');

        $form = new \Application\Form\Account();

        if ($this->isPost()) {
            $form->setData($this->getRequest()->getPost());
            if ($form->isValid()) {
                $em = $this->getEntityManager();

                $account = new Account();
                $form->saveToEntity($account);
                $account->setOwner($em->getReference(Person::class, $form->getElementValue('owner_id')));

                $em->persist($account);
                $em->flush();

                return $this->redirectTo('edit', ['aid' => $account->getAccountId()]);
            }
        }

        return new ViewModel([
            'form' => $form
        ]);
    }

    public function editAction () {
        $this->getLayoutService()->setTitle('Edit Account');
        $this->getMenu()->setActiveId('admin-account-edit');

        $aid = $this->getParam('aid');

        if (! $aid) {
            return $this->redirectTo('list');
        }

        $em = $this->getEntityManager();
        $repo = $em->getRepository(Account::class);
        
        /** @var Account $account */
        $account = $repo->findOneBy(['accountId' => $aid]);

        if (! $account) {
            throw new \Exception('User does not exist');
        }

        $form = new \Application\Form\Account();

        if ($this->isPost()) {
            $form->setData($this->getRequest()->getPost());

            if ($form->isValid()) {
                $account->setName($form->get('name')->getValue());
                $form->saveToEntity($account);
                
                $account->setOwner($em->getReference(Person::class, $form->get('owner_id')->getValue()));

                $em->persist($account);
                $em->flush();
    
                return $this->redirectTo('edit', ['aid' => $account->getAccountId()]);
            }
        }
        else {
            $owner = $account->getOwner();
            $ownerId = $owner->getPersonId();
            $ownerName = $owner->getName() . ' (' . $owner->getUsername() . ')';
            $form->hydrateFromEntity($account);
            $form->get('account_id')->setValue($aid);
            $form->get('owner_id')
                ->setValueOptions([$ownerId => $ownerName])
                ->setValue($ownerId);
        }

        return new ViewModel([
            'form' => $form,
            'account' => $account
        ]);
    }
}
