<?php
namespace Application\Controller\Admin;

use Application\Mail\Template;
use Application\Service\ServiceInterface\AclServiceInterface;
use Application\Service\ServiceInterface\DoctrineManagerInterface;
use Application\Service\ServiceInterface\MailServiceInterface;
use Application\Service\ServiceInterface\PersonSettingServiceInterface;
use Application\Service\ServiceInterface\PredisServiceInterface;
use Application\Service\ServiceTrait\AclServiceTrait;
use Application\Service\ServiceTrait\DoctrineManagerTrait;
use Application\Service\ServiceTrait\MailServiceTrait;
use Application\Service\ServiceTrait\PersonSettingServiceTrait;
use Application\Service\ServiceTrait\PredisServiceTrait;
use Laminas\Session\Container;
use Laminas\View\Model\ViewModel;

class TestController extends AbstractAdminController implements
            AclServiceInterface,
            DoctrineManagerInterface,
            PredisServiceInterface,
            PersonSettingServiceInterface,
            MailServiceInterface {
    use AclServiceTrait;
    use DoctrineManagerTrait;
    use PredisServiceTrait;
    use PersonSettingServiceTrait;
    use MailServiceTrait;

    public function indexAction () {
        $this->getLayoutService()->setTitle('Test');
        $this->getMenu()->setActiveId('admin-test-greeting');

        return new ViewModel();
    }

    public function aclAction () {
        $this->getLayoutService()->setTitle('ACL');
        $this->getMenu()->setActiveId('admin-test-acl');

        $acl = $this->getAclService();

        $accessTable = [];

        foreach ($acl->getResources() as $resource) {
            $accessTable[$resource] = [];

            foreach ($acl->getRoles() as $role) {
                $accessTable[$resource][$role] = [];

                foreach ($acl->getActions() as $action) {
                    $accessTable[$resource][$role][$action] = $acl->isAllowed($role, $resource, $action);
                }
            }
        }

        return new ViewModel(array(
            'roles' => $acl->getRoles(),
            'resources' => $acl->getResources(),
            'actions' => $acl->getActions(),
            'accessTable' => $accessTable
        ));
    }

    public function predisAction () {
        $this->getLayoutService()->setTitle('Predis Test');
        $this->getMenu()->setActiveId('admin-test-predis');

        $client = $this->getPredisService()->getClient();
        $client->set('admin-test-key', 'Predis is working!');

        return new ViewModel([
            'testKey' => $client->get('admin-test-key')
        ]);
    }

    public function sessionAction () {
        $this->getLayoutService()->setTitle('Session Test');
        $this->getMenu()->setActiveId('admin-test-session');

        $sessionTest = new Container('testpage');

        $content = $sessionTest->content ? $sessionTest->content : 'Variable set. Refresh to test.';

        $sessionTest->content = 'It is working!';

        return new ViewModel([
            'content' => $content
        ]);
    }

    public function authAction () {
        $this->getLayoutService()->setTitle('Auth Test');
        $this->getMenu()->setActiveId('admin-test-auth');

        $this->getAuthService()->requireAuth();

        return new ViewModel();
    }
    
    public function selectAction () {
        $this->getLayoutService()->setTitle('Select2 Test');
        $this->getMenu()->setActiveId('admin-test-select');
    
        return new ViewModel();
    }

    public function routeurlAction () {
        $this->getLayoutService()->setTitle('RouteUrl Test');
        $this->getMenu()->setActiveId('admin-test-routeurl');

        return new ViewModel();
    }

    public function personsettingsAction () {
        $this->getLayoutService()->setTitle('Person Settings Test');
        $this->getMenu()->setActiveId('admin-test-personsettings');

        $person = $this->getAuthService()->getPerson();

        $settingService = $this->getPersonSettingService();

        $settingService->setPersonSetting($person, 'timezone', 'America/Denver');
        $settingService->save();

        return new ViewModel([
            'timezone' => $settingService->getPersonSetting($person, 'timezone', true)
        ]);
    }

    public function emailAction () {
        $this->getLayoutService()->setTitle('Email Test');
        $this->getMenu()->setActiveId('admin-test-email');

        $authService = $this->getAuthService();

        $toPerson = $authService->getPerson();
        $toAddress = $toPerson->getEmail();

        if ($this->isPost() && $this->getParam('email')) {
            $emailService = $this->getMailService();

            // Create a new mail template
            $template = new Template();
            $template->setLayout('layout/transaction.phtml');
            $template->setLayoutParameters([
                'title' => 'Welcome'
            ]);

            // Set the email template file
            $template->setTemplate('template/welcome.phtml');
            $template->setTemplateParameters([
                'name' => $toPerson->getName()
            ]);

            // Set the content HTML for the template/layout
            $template->setHtml('<p>This is a test email.</p>');

            $message = $emailService->createMessage();
            $message->setFrom('accounts@updashd.com', "Updashd Accounts");
            $message->setTo($toAddress);
            $message->setSubject('Welcome to Updashd');
            $message->setContentBody($template->render());

            $emailService->send($message);

            $this->addFlashMessage('Email Sent', 'success');

            return $this->redirectTo('email');
        }

        return new ViewModel([
            'to' => $toAddress
        ]);
    }
}
