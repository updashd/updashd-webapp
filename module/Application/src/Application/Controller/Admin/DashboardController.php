<?php
namespace Application\Controller\Admin;

class DashboardController extends AbstractAdminController {
    public function indexAction () {
        $this->getMenu()->setActiveId('admin-dashboard');
        $this->getLayoutService()->setTitle('Administrator Dashboard');
    }
}