<?php
namespace Application\Controller\Admin;

use Application\Pagination\Paginator as ApplicationPaginator;
use Application\Service\ServiceInterface\DoctrineManagerInterface;
use Application\Service\ServiceTrait\DoctrineManagerTrait;
use Updashd\Model\Account;
use Updashd\Model\Zone;
use Updashd\Model\Zone as ZoneEntity;
use Laminas\View\Model\ViewModel;

class ZoneController extends AbstractAdminController implements DoctrineManagerInterface {
    use DoctrineManagerTrait;

    public function indexAction () {
        return $this->redirectTo('list');
    }
    
    public function listAction () {
        $this->getLayoutService()->setTitle('List Zones');
        $this->getMenu()->setActiveId('admin-zone-list');
    
        $em = $this->getEntityManager();
        
        $query = $em->createQueryBuilder()
            ->select('z')
            ->from(ZoneEntity::class, 'z')
            ->orderBy('z.sortOrder')
            ->getQuery();
        
        $paginator = new ApplicationPaginator($query, $this->getQueryParam());
        
        return new ViewModel([
            'paginator' => $paginator,
        ]);
    }
    
    public function addAction () {
        $this->getLayoutService()->setTitle('Add Zone');
        $this->getMenu()->setActiveId('admin-zone-add');
    
        $em = $this->getEntityManager();
        
        $form = new \Application\Form\Zone();
        
        if ($this->isPost()) {
            $form->setData($this->getRequest()->getPost());
            
            if ($form->isValid()) {
                $zone = new ZoneEntity();
                $form->saveToEntity($zone);
                
                // Set an account ID if specified
                if ($form->getElementValue('account_id')) {
                    $zone->setAccount($em->getReference(Zone::class, $form->getElementValue('account_id')));
                }
                
                $em->persist($zone);
                $em->flush();
    
                return $this->redirectTo('edit', ['zid' => $zone->getZoneId()]);
            }
        }
    
        return new ViewModel([
            'form' => $form
        ]);
    }
    
    public function editAction () {
        $this->getLayoutService()->setTitle('Edit Zone');
        $this->getMenu()->setActiveId('admin-zone-edit');
    
        $em = $this->getEntityManager();
        $zoneRepo = $em->getRepository(Zone::class);
        
        $zoneId = $this->getParam('zid');
        
        if (! $zoneId) {
            throw new \Exception('No zone ID specified');
        }
        
        /** @var Zone $zone */
        $zone = $zoneRepo->findOneBy(['zoneId' => $zoneId]);
        
        if (! $zone) {
            throw new \Exception('Zone not found');
        }
    
        $form = new \Application\Form\Zone();
        
        if ($this->isPost()) {
            $form->setData($this->getRequest()->getPost());
            
            if ($form->isValid()) {
                $form->saveToEntity($zone);
                
                // Set an account ID if specified
                if ($form->getElementValue('account_id')) {
                    $zone->setAccount($em->getReference(Account::class, $form->getElementValue('account_id')));
                }
                
                $em->persist($zone);
                $em->flush();
    
                return $this->redirectTo('edit', ['zid' => $zone->getZoneId()]);
            }
        }
        else {
            $form->hydrateFromEntity($zone);
            $account = $zone->getAccount();
            
            if ($account) {
                $form->get('account_id')
                    ->setValueOptions([
                        $account->getAccountId() => $account->getName() . ' (' . $account->getOwner()->getName() . ')'
                    ])
                    ->setValue($account->getAccountId());
            }
        }
    
        return new ViewModel([
            'form' => $form,
            'zone' => $zone,
        ]);
    }
}
