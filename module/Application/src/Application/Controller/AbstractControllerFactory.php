<?php
namespace Application\Controller;

use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\AbstractFactoryInterface;

class AbstractControllerFactory implements AbstractFactoryInterface {

    /**
     * Can the factory create an instance for the service?
     *
     * @param  ContainerInterface $container
     * @param  string $requestedName
     * @return bool
     */
    public function canCreate (ContainerInterface $container, $requestedName) {
        try {
            $reflector = new \ReflectionClass($requestedName);
        }
        catch (\ReflectionException $e) {
            return false;
        }

        return preg_match('/Controller$/', $reflector->getName());
    }

    /**
     * Create an object
     *
     * @param  ContainerInterface $container
     * @param  string $requestedName
     * @param  null|array $options
     * @return AbstractActionController
     * @throws ServiceNotFoundException if unable to resolve the service.
     * @throws ServiceNotCreatedException if an exception is raised when
     *     creating a service.
     * @throws ContainerException if any other error occurs
     */
    public function __invoke (ContainerInterface $container, $requestedName, array $options = null) {
        $controller = new $requestedName();

        try {
            $class = new \ReflectionClass($requestedName);

            $interfaces = $class->getInterfaces();

            foreach ($interfaces as $interface) {
                $namespaceName = $interface->getNamespaceName();
                $interfaceName = $interface->getName();


                // Only care about matching traits
                if (preg_match('/ServiceInterface$/', $namespaceName) && preg_match('/Interface/', $interfaceName)) {
                    $hasMatch = preg_match('/([A-Za-z]*)Interface$/', $interfaceName, $matches);

                    if (! $hasMatch) {
                        throw new ServiceNotCreatedException('Cannot determine service name from interface. ' . $interfaceName);
                    }

                    $serviceName = $matches[1];

                    $setMethod = $class->getMethod('set' . $serviceName);
                    $parameters = $setMethod->getParameters();

                    $requestedService = null;

                    foreach ($parameters as $parameter) {
                        $requestedService = $parameter->getClass()->getName();
                    }

                    if (! $requestedService) {
                        throw new ServiceNotCreatedException('Setter for service exists, but setter method does not accept service class.');
                    }

                    $setMethod->invoke($controller, $container->get($requestedService));
                }
            }
        }
        catch (\ReflectionException $e) {
            throw new ServiceNotCreatedException('Interface for service found, but no setter method exists');
        }

        return $controller;
    }
}
