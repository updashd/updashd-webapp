<?php
namespace Application\Controller\Home;

use Application\Controller\AbstractController;

class IndexController extends AbstractController {
    public function indexAction () {
        $this->redirectTo('index', [], 'signin', 'auth');
    }
}