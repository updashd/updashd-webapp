<?php
namespace Application\Controller\Account;

class IndexController extends AbstractAccountController {
    public function indexAction () {
        return $this->redirectTo('index', [], 'dashboard', 'account', [
            'account' => $this->getAuthService()->getPerson()->getUsername()
        ]);
    }
}
