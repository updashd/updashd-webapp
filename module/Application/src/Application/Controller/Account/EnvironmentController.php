<?php
namespace Application\Controller\Account;

use Application\Service\ServiceInterface\DoctrineManagerInterface;
use Application\Service\ServiceTrait\DoctrineManagerTrait;
use Updashd\Model\Environment;
use Updashd\Model\Environment as EnvironmentEntity;
use Laminas\View\Model\ViewModel;

class EnvironmentController extends AbstractAccountController implements DoctrineManagerInterface {
    use DoctrineManagerTrait;

    public function indexAction () {
        return $this->redirectTo('list');
    }

    public function listAction () {
        $this->getLayoutService()->setTitle('Environment List');
        $this->getMenu()->setActiveId('environment-list');

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();
        $query = $qb->select('e AS environment, COUNT(n.nodeId) as nodeCount')
            ->from(Environment::class, 'e')
            ->leftJoin('e.nodes', 'n')
            ->where($qb->expr()->eq('e.account', ':account'))
            ->groupBy('e.environmentId')
            ->orderBy('e.sortOrder')
            ->getQuery();

        $query->setParameter('account', $this->getAccount());

        return new ViewModel([
            'results' => $query->getResult(),
        ]);
    }

    public function addAction () {
        $this->getLayoutService()->setTitle('Add Environment');
        $this->getMenu()->setActiveId('environment-add');

        $em = $this->getEntityManager();

        $form = new \Application\Form\Environment();

        if ($this->isPost()) {
            $form->setData($this->getRequest()->getPost());

            if ($form->isValid()) {
                $environment = new EnvironmentEntity();

                $form->saveToEntity($environment);
                $environment->setAccount($this->getAccount());

                $em->persist($environment);
                $em->flush();
            }
        }

        return new ViewModel([
            'form' => $form,
        ]);
    }

    public function editAction () {
        $this->getLayoutService()->setTitle('Edit Environment');
        $this->getMenu()->setActiveId('environment-edit');

        $em = $this->getEntityManager();

        $environmentId = $this->getParam('eid');

        $environmentRepo = $em->getRepository(EnvironmentEntity::class);

        $environment = $environmentRepo->findOneBy([
            'account' => $this->getAccount(),
            'environmentId' => $environmentId,
        ]);

        if (! $environment) {
            return $this->redirectTo('list');
        }

        $form = new \Application\Form\Environment();

        if ($this->isPost()) {
            $form->setData($this->getRequest()->getPost());

            if ($form->isValid()) {
                $form->saveToEntity($environment);
                $em->persist($environment);
                $em->flush();
            }
        }
        else {
            $form->hydrateFromEntity($environment);
        }

        return new ViewModel([
            'form' => $form,
        ]);
    }
}
