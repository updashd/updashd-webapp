<?php
namespace Application\Controller\Account;

use Application\Form\Element\Select;
use Application\Form\Notifier as NotifierForm;
use Application\Form\NotifierConfig as NotifierConfigForm;
use Application\Service\ServiceInterface\NotifierServiceInterface;
use Application\Service\ServiceTrait\NotifierServiceTrait;
use Updashd\Model\AccountNotifier;
use Updashd\Model\Notifier;
use Laminas\View\Model\ViewModel;

class NotifierController extends AbstractAccountController implements NotifierServiceInterface {
    use NotifierServiceTrait;

    public function indexAction () {
        return $this->redirectTo('list');
    }

    public function listAction () {
        $this->getLayoutService()->setTitle('Notifier List');
        $this->getMenu()->setActiveId('notifier-list');

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        $anqb = $qb->select('an, n')
            ->from(AccountNotifier::class, 'an')
            ->innerJoin('an.notifier', 'n')
            ->innerJoin('an.account', 'a')
            ->addOrderBy('an.sortOrder');

        $anqb->where($anqb->expr()->eq('a', ':account'));

        $anqb->setParameter('account', $this->getAccount());

        $accountNotifiers = $anqb->getQuery()->getResult();

        return new ViewModel([
            'accountNotifiers' => $accountNotifiers
        ]);
    }

    public function editAction () {
        $notifierId = $this->getParam('nid');

        $form = new NotifierForm();

        /** @var Select $notifierTypeElem */
        $notifierTypeElem = $form->get('notifier_id');

        $notifierTypeElem->setValueOptions($this->getNotifierOptions());

        $em = $this->getEntityManager();

        if ($notifierId) {
            $this->getLayoutService()->setTitle('Edit Notifier');
            $this->getMenu()->setActiveId('notifier-edit-basic');

            $accountNotifier = $this->getNotifier($notifierId);

            if (! $accountNotifier) {
                $this->flashMessenger()->addMessage('Notifier not found.', 'error');
                return $this->redirectTo('list');
            }

            $form->hydrateFromEntity($accountNotifier);

            $notifierTypeElem->setValue($accountNotifier->getNotifier()->getNotifierId());
        }
        else {
            $this->getLayoutService()->setTitle('Add Notifier');
            $this->getMenu()->setActiveId('notifier-edit-basic');

            $accountNotifier = new AccountNotifier();
        }

        if ($this->isPost() && $form->isDataValid($this->getPostParam())) {
            $form->saveToEntity($accountNotifier);
            $accountNotifier->setAccount($this->getAccount());

            $notifier = $em->getReference(Notifier::class, $form->getElementValue('notifier_id'));
            $accountNotifier->setNotifier($notifier);

            $em->persist($accountNotifier);
            $em->flush();

            $this->flashMessenger()->addMessage('Notifier saved.', 'success');
            return $this->redirectTo('edit', ['nid' => $accountNotifier->getAccountNotifierId()]);
        }

        return new ViewModel([
            'form' => $form,
            'accountNotifier' => $accountNotifier
        ]);
    }

    public function configAction () {
        $this->getMenu()->setActiveId('notifier-edit-config');
        $this->getLayoutService()->setTitle('Edit Notifier');

        $em = $this->getEntityManager();

        $notifierId = $this->getParam('nid');

        $accountNotifier = $this->getNotifier($notifierId);

        if (! $accountNotifier) {
            $this->flashMessenger()->addMessage('Notifier not found.', 'error');
            return $this->redirectTo('list');
        }

        $config = $this->getNotifierService()->getNotifierConfig($accountNotifier->getNotifier()->getModuleName());

        $form = new NotifierConfigForm($config);
        $form->hydrateFromJson($accountNotifier->getConfig());

        if ($this->isPost() && $form->isDataValid($this->getPostParam())) {
            $accountNotifier->setConfig($form->saveToJson());
            $em->persist($accountNotifier);
            $em->flush();

            $this->flashMessenger()->addMessage('Notifier configuration saved.', 'success');
            return $this->redirectTo('config', ['nid' => $notifierId]);
        }

        return new ViewModel([
            'form' => $form,
            'accountNotifier' => $accountNotifier
        ]);
    }

    public function setStatusAction () {
        $notifierId = $this->getParam('nid');
        $enabled = $this->getParam('enabled') ? true : false;
        $formAction = $this->getParam('from_action');

        if (! $notifierId) {
            $this->flashMessenger()->addMessage('No Notifier ID specified.', 'error');
            $this->redirectTo('list');
        }

        $notifier = $this->getNotifier($notifierId);

        if (! $notifierId) {
            $this->flashMessenger()->addMessage('Notifier ID not found.', 'error');
            $this->redirectTo('list');
        }

        $notifier->setIsEnabled($enabled);

        $em = $this->getEntityManager();
        $em->persist($notifier);
        $em->flush();

        $this->flashMessenger()->addMessage('Notifier ' . ($enabled ? 'Enabled' : 'Disabled'), 'success');

        return $this->redirectTo($formAction, ['nid' => $notifierId]);
    }

    /**
     * @param int $notifierId
     * @return AccountNotifier|null
     */
    protected function getNotifier ($notifierId) {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();
        $anq = $qb->select('an')
            ->from(AccountNotifier::class, 'an')
            ->innerJoin('an.account', 'a')
            ->addOrderBy('an.sortOrder')
            ->where($qb->expr()->andX(
                $qb->expr()->eq('an.account', ':account'),
                $qb->expr()->eq('an.accountNotifierId', ':accountNotifierId')
            ));

        $notifierQuery = $anq->getQuery();
        $notifierQuery->setParameter('account', $this->getAccount());
        $notifierQuery->setParameter('accountNotifierId', $notifierId);

        return $notifierQuery->getOneOrNullResult();
    }

    protected function getNotifierOptions ($includeNullOption = true) {
        $em = $this->getEntityManager();

        $eqb = $em->createQueryBuilder();
        $envQuery = $eqb->select('n')
            ->from(Notifier::class, 'n')
            ->where($eqb->expr()->orX(
                $eqb->expr()->eq('n.account', ':account'),
                $eqb->expr()->isNull('n.account')
            ))
            ->addOrderBy('n.sortOrder')
            ->getQuery();

        $envQuery->setParameter('account', $this->getAccount());

        /** @var Notifier[] $notifiers */
        $notifiers = $envQuery->getResult();

        $options = $includeNullOption ? ['' => 'Select...'] : [];

        foreach ($notifiers as $notifier) {
            $options[$notifier->getNotifierId()] = $notifier->getNotifierName();
        }

        return $options;
    }
}
