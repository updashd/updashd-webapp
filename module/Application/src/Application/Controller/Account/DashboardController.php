<?php
namespace Application\Controller\Account;

use Application\Service\ServiceInterface\AuthServiceInterface;
use Application\Service\ServiceInterface\JWTServiceInterface;
use Application\Service\ServiceInterface\DoctrineManagerInterface;
use Application\Service\ServiceTrait\AuthServiceTrait;
use Application\Service\ServiceTrait\JWTServiceTrait;
use Application\Service\ServiceTrait\DoctrineManagerTrait;
use Doctrine\ORM\Query\Expr\Join;
use Updashd\Model\Incident;
use Updashd\Model\NodeService;
use Updashd\Model\NodeServiceZone;
use Updashd\Model\Result;
use Updashd\Model\ResultLatest;
use Updashd\Model\Severity;
use Laminas\View\Model\ViewModel;

class DashboardController extends AbstractAccountController
    implements DoctrineManagerInterface, AuthServiceInterface, JWTServiceInterface {
    use DoctrineManagerTrait;
    use AuthServiceTrait;
    use JWTServiceTrait;

    use DateRangeTrait;

    public function init () {
        parent::init();

        $userTimeZone = $this->getPersonSettingService()->getPersonSetting(
            $this->getAuthService()->getPerson(),
            'timezone'
        );

        // This is implemented on the trait
        $this->initDateRange($userTimeZone, $this->getParam('range'));
    }

    public function indexAction () {
        return $this->redirectTo('status');
    }

    public function summaryAction () {
        $this->getLayoutService()->setTitle('Dashboard');
        $this->getLayoutService()->setDescription('Main');
        $this->getMenu()->setActiveId('dashboard-summary');

        $em = $this->getEntityManager();

        //////////////////////
        // Incident Summary //
        //////////////////////
        $incidentSummaryQb = $em->createQueryBuilder();
        $incidentSummaryQb->select('
        partial s.{severityId, severityName, layoutColor, layoutIcon, layoutIconVendor} AS severity,
        COUNT(i.incidentId) AS cnt')
            ->from(Severity::class, 's')
            ->innerJoin(Incident::class, 'i', Join::WITH, 'i.severity = s')
            ->groupBy('s.severityId')
            ->orderBy('s.sortOrder')
            ->where($incidentSummaryQb->expr()->andX(
                $incidentSummaryQb->expr()->eq('i.account', ':account'),
                $incidentSummaryQb->expr()->between('i.dateLastSeen', ':start', ':end'),
                $incidentSummaryQb->expr()->eq('i.isRead', ':isRead')
            ));

        $incidentSummaryQb->setParameter('account', $this->getAccount());
        $incidentSummaryQb->setParameter('start', $this->getStart());
        $incidentSummaryQb->setParameter('end', $this->getEnd());
        $incidentSummaryQb->setParameter('isRead', false);

        $incidentSummary = $incidentSummaryQb->getQuery()->getResult();

        /////////////////////
        // Overall Summary //
        /////////////////////
        $overallSummaryQb = $em->createQueryBuilder();
        $overallSummaryQb->select('
        partial s.{severityId, severityName, layoutColor, layoutIcon, layoutIconVendor} AS severity,
        COUNT(DISTINCT r.resultId) AS cnt
        ')
            ->from(Severity::class, 's')
            ->innerJoin(Result::class, 'r', Join::WITH, 'r.statusCode = s.severityId')
            ->innerJoin('r.nodeService', 'ns')
            ->innerJoin('ns.node', 'n')
            ->groupBy('s.severityId')
            ->orderBy('s.sortOrder')
            ->where(
                $overallSummaryQb->expr()->andX(
                    $overallSummaryQb->expr()->eq('n.account', ':account'),
                    $overallSummaryQb->expr()->between('r.startTime', ':start', ':end')
                )
            );

        $overallSummaryQb->setParameter('account', $this->getAccount());
        $overallSummaryQb->setParameter('start', $this->getStart());
        $overallSummaryQb->setParameter('end', $this->getEnd());

        $overallSummary = $overallSummaryQb->getQuery()->getResult();

        /////////////////////////
        // Environment Summary //
        /////////////////////////
        $environmentSummaryQb = $em->createQueryBuilder();
        $environmentSummaryQb->select('
        s.severityId,
        s.severityName,
        s.layoutColor,
        s.bootstrapColor,
        e.environmentId,
        e.environmentName,
        COUNT(r.resultId) AS cnt
        ')
            ->from(Severity::class, 's')
            ->innerJoin(Result::class, 'r', Join::WITH, 'r.statusCode = s.severityId')
            ->innerJoin('r.nodeService', 'ns')
            ->innerJoin('ns.node', 'n')
            ->innerJoin('n.environment', 'e')
            ->addGroupBy('e.environmentId')
            ->addGroupBy('s.severityId')
            ->addOrderBy('e.sortOrder')
            ->addOrderBy('s.sortOrder')
            ->where(
                $environmentSummaryQb->expr()->andX(
                    $environmentSummaryQb->expr()->eq('n.account', ':account'),
                    $environmentSummaryQb->expr()->between('r.startTime', ':start', ':end')
                )
            );

        $environmentSummaryQb->setParameter('account', $this->getAccount());
        $environmentSummaryQb->setParameter('start', $this->getStart());
        $environmentSummaryQb->setParameter('end', $this->getEnd());

        $environmentSummary = $environmentSummaryQb->getQuery()->getResult();

        $environmentSummaryGrouped = array();

        foreach ($environmentSummary as $value) {
            $environmentSummaryGrouped[$value['environmentId']][] = $value;
        }


        /////////////////////
        // Service Summary //
        /////////////////////
        $serviceSummaryQb = $em->createQueryBuilder();
        $serviceSummaryQb->select('
        sv.severityId,
        sv.severityName,
        sv.layoutColor,
        sv.bootstrapColor,
        sr.serviceId,
        sr.serviceName,
        COUNT(r.resultId) AS cnt
        ')
            ->from(Severity::class, 'sv')
            ->innerJoin(Result::class, 'r', Join::WITH, 'r.statusCode = sv.severityId')
            ->innerJoin('r.nodeService', 'ns')
            ->innerJoin('ns.service', 'sr')
            ->innerJoin('ns.node', 'n')
            ->addGroupBy('sr.serviceId')
            ->addGroupBy('sv.severityId')
            ->addOrderBy('sr.sortOrder')
            ->addOrderBy('sv.sortOrder')
            ->where(
                $serviceSummaryQb->expr()->andX(
                    $serviceSummaryQb->expr()->eq('n.account', ':account'),
                    $serviceSummaryQb->expr()->between('r.startTime', ':start', ':end')
                )
            );

        $serviceSummaryQb->setParameter('account', $this->getAccount());
        $serviceSummaryQb->setParameter('start', $this->getStart());
        $serviceSummaryQb->setParameter('end', $this->getEnd());

        $serviceSummary = $serviceSummaryQb->getQuery()->getResult();

        $serviceSummaryGrouped = array();

        foreach ($serviceSummary as $value) {
            $serviceSummaryGrouped[$value['serviceId']][] = $value;
        }

        return new ViewModel([
            'start' => $this->getStart(),
            'end' => $this->getEnd(),
            'incidentSummary' => $incidentSummary,
            'overallSummary' => $overallSummary,
            'environmentSummary' => $environmentSummaryGrouped,
            'serviceSummary' => $serviceSummaryGrouped,
        ]);
    }

    public function statusAction () {
        $this->getLayoutService()->setTitle('Dashboard');
        $this->getLayoutService()->setDescription('Current Status');
        $this->getMenu()->setActiveId('dashboard-status');

        $em = $this->getEntityManager();

        $statusQ = $em->createQueryBuilder();
        $statusQ
            ->select('rs, s, ns, n, e')
            ->from(ResultLatest::class, 'rs')
            ->innerJoin(NodeServiceZone::class, 'nsz', Join::WITH, $statusQ->expr()->andX(
                $statusQ->expr()->eq('nsz.nodeService', 'rs.nodeService'),
                $statusQ->expr()->eq('nsz.zone', 'rs.zone')
            ))
            ->innerJoin('rs.statusCode', 's')
            ->innerJoin('rs.nodeService', 'ns')
            ->innerJoin('ns.node', 'n')
            ->innerJoin('n.environment', 'e')
            ->addOrderBy('s.sortOrder', 'DESC')
            ->addOrderBy('e.sortOrder', 'ASC')
            ->addOrderBy('n.sortOrder', 'ASC')
            ->addOrderBy('n.nodeName', 'ASC')
            ->addOrderBy('ns.sortOrder', 'ASC')
            ->addOrderBy('ns.nodeServiceName', 'ASC')
            ->where($statusQ->expr()->andX(
                $statusQ->expr()->eq('n.account', ':account')),
                $statusQ->expr()->eq('n.isEnabled', 1),
                $statusQ->expr()->eq('ns.isEnabled', 1)
            );

        $statusQ->setParameter('account', $this->getAccount());

        $statusesSummary = $statusQ->getQuery()->getResult();

        return new ViewModel([
            'canView' => true,
            'start' => $this->getStart(),
            'end' => $this->getEnd(),
            'statusesSummary' => $statusesSummary
        ]);
    }

    public function nodeServiceStatusAction () {
        return $this->getAllNodeServiceZones();
    }

    public function nodeServiceIconsAction () {
        return $this->getAllNodeServiceZones();
    }

    /**
     * @return \Laminas\View\Model\ViewModel
     */
    public function getAllNodeServiceZones () {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();
        $qb
            ->addSelect('n.nodeId')
            ->addSelect('n.nodeName')
            ->addSelect('ns.nodeServiceId')
            ->addSelect('ns.nodeServiceName')
            ->addSelect('nsz.nodeServiceZoneId')
            ->addSelect('z.zoneId')
            ->addSelect('z.zoneName')
            ->addSelect('s.serviceId')
            ->addSelect('s.serviceName')
            ->addSelect('\'\' AS jwt')
            ->from(NodeService::class, 'ns')
            ->innerJoin('ns.node', 'n')
            ->innerJoin('ns.service', 's')
            ->innerJoin('n.account', 'a')
            ->innerJoin('ns.nodeServiceZones', 'nsz')
            ->innerJoin('nsz.zone', 'z')
            ->where($qb->expr()->eq('n.account', ':account'))
            ->addOrderBy('n.sortOrder')
            ->addOrderBy('ns.sortOrder')
            ->addOrderBy('s.sortOrder')
            ->addOrderBy('z.sortOrder');

        $qb->setParameter('account', $this->getAccount());

        $result = $qb->getQuery()->getResult();

        $jwtService = $this->getJwtService();

        // Create JWTs
        $result = array_map(function ($e) use ($jwtService) {
            $e['jwt'] = $jwtService->encode([
                'iat' => time(),
                'taskId' => $e['nodeServiceZoneId']
            ]);

            return $e;
        }, $result);

        return new ViewModel([
            'nodeServiceZones' => $result
        ]);
    }
}
