<?php
namespace Application\Controller\Account;

use Application\Form\TrendFilter;
use Laminas\View\Model\ViewModel;

class TrendController extends AbstractAccountController {
    use DateRangeTrait;

    public function init () {
        parent::init();

        $userTimeZone = $this->getPersonSettingService()->getPersonSetting(
            $this->getAuthService()->getPerson(),
            'timezone'
        );

        // This is implemented on the trait
        $this->initDateRange($userTimeZone, $this->getParam('range'));
    }

    public function indexAction () {
        $this->getMenu()->setActiveId('trends');
        $this->getLayoutService()->setTitle('Trends');
        $this->getLayoutService()->setDescription('Performance Visualization');

        $form = new TrendFilter($this->getAccount()->getSlug());
        $data = $this->getQueryParam();

        if ($this->getParam('generate') && $form->isDataValid($data)) {
            // Do Something
        }

        return new ViewModel([
            'start' => $this->getStart(),
            'end' => $this->getEnd(),
            'form' => $form,
        ]);
    }
}
