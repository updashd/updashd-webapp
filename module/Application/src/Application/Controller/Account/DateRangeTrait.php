<?php
namespace Application\Controller\Account;

trait DateRangeTrait {
    private $start;
    private $end;

    protected function initDateRange ($timezone, $rangeParam, $defaultStart = 'today midnight', $defaultEnd = '23:59:59') {
        $range = $rangeParam;
        $range = explode('-', $range);

        if (count($range) == 2) {
            list($start, $end) = $range;

            $start = new \DateTime($start, new \DateTimeZone($timezone));
            $end = new \DateTime($end, new \DateTimeZone($timezone));
        }
        else {
            $start = new \DateTime($defaultStart, new \DateTimeZone($timezone));
            $end = new \DateTime($defaultEnd, new \DateTimeZone($timezone));
        }

        $this->setStart($start);
        $this->setEnd($end);
    }

    /**
     * @return \DateTime
     */
    public function getStart () {
        return $this->start;
    }

    /**
     * @param \DateTime $start
     */
    public function setStart ($start) {
        $this->start = $start;
    }

    /**
     * @return \DateTime
     */
    public function getEnd () {
        return $this->end;
    }

    /**
     * @param \DateTime $end
     */
    public function setEnd ($end) {
        $this->end = $end;
    }
}