<?php
namespace Application\Controller\Account;

use Application\Service\ServiceInterface\AuthServiceInterface;
use Application\Service\ServiceInterface\DoctrineManagerInterface;
use Application\Service\ServiceInterface\ElasticSearchServiceInterface;
use Application\Service\ServiceTrait\AuthServiceTrait;
use Application\Service\ServiceTrait\DoctrineManagerTrait;
use Application\Service\ServiceTrait\ElasticSearchServiceTrait;
use Laminas\View\Model\ViewModel;

class SearchController extends AbstractAccountController implements
    DoctrineManagerInterface,
    AuthServiceInterface,
    ElasticSearchServiceInterface {
    use DoctrineManagerTrait;
    use AuthServiceTrait;
    use ElasticSearchServiceTrait;

    use DateRangeTrait;

    public function init () {
        parent::init();
    }

    public function indexAction () {
//        $this->getMenu()->setActiveId('dashboard');
        $esClient = $this->getElasticSearchService()->getClient();

        $query = $this->getParam('q');
        $type = $this->getParam('type');

        $params = [];
        $params['index'] = 'search';
        $params['body']['query']['bool']['must']['query_string']['query'] = $query;
        $params['body']['query']['bool']['filter']['term']['account_id'] = $this->getAccount()->getAccountId();
//        $params['body']['query']['bool']['must']['match']['name_english']['query'] = $query;
        $params['body']['aggs']['types']['terms']['field'] = 'type';
//        $params['body']['highlight'] = [
//            'pre_tags' => ['<em>'],
//            'post_tags' => ['</em>'],
//        ];
//
//        $params['body']['highlight']['fields']['name_english']['highlight_query']['bool']['must']['match']['name_english']['query'] = $query;
//        $params['body']['highlight']['fields']['name']['highlight_query']['bool']['must']['match']['name']['query'] = $query;
//        $params['body']['highlight']['fields']['service_name']['highlight_query']['bool']['must']['match']['service_name']['query'] = $query;
//        $params['body']['highlight']['fields']['service_name_english']['highlight_query']['bool']['must']['match']['service_name_english']['query'] = $query;
//        $params['body']['highlight']['fields']['node_name']['highlight_query']['bool']['must']['match']['node_name']['query'] = $query;
//        $params['body']['highlight']['fields']['node_name_english']['highlight_query']['bool']['must']['match']['node_name_english']['query'] = $query;
//        $params['body']['highlight']['fields']['node_service_name']['highlight_query']['bool']['must']['match']['node_service_name']['query'] = $query;
//        $params['body']['highlight']['fields']['node_service_name_english']['highlight_query']['bool']['must']['match']['node_service_name_english']['query'] = $query;

        if ($type) {
            $params['body']['post_filter']['term']['type'] = $type;
        }

        $results = $esClient->search($params);

//        new \Application\Util\Debug($results);exit;

        $this->getLayoutService()->setTitle('Search');
        $this->getLayoutService()->setDescription($results['hits']['total'] . ' Results');

        return new ViewModel([
            'params' => [
                'q' => $query,
            ],
            'query' => $query,
            'type' => $this->getParam('type'),
            'milliseconds' => $results['took'],
            'count' => $results['hits']['total'],
            'maxScore' => $results['hits']['max_score'],
            'hits' => $results['hits']['hits'],
            'types' => $results['aggregations']['types']['buckets']
        ]);
    }
}
