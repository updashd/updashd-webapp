<?php
namespace Application\Controller\Account;

use Application\Doctrine\SearchHelper;
use Application\Form\Element\MultiSelect;
use Application\Form\Node as NodeForm;
use Application\Form\NodeFilter;
use Application\Form\NodeServiceBasic as NodeServiceForm;
use Application\Form\NodeServiceConfig;
use Application\Service\ServiceInterface\DoctrineManagerInterface;
use Application\Service\ServiceInterface\WorkerServiceInterface;
use Application\Service\ServiceTrait\DoctrineManagerTrait;
use Application\Service\ServiceTrait\WorkerServiceTrait;
use Updashd\Model\Environment as EnvironmentEntity;
use Updashd\Model\Node as NodeEntity;
use Updashd\Model\NodeService as NodeServiceEntity;
use Updashd\Model\NodeServiceZone;
use Updashd\Model\Service as ServiceEntity;
use Updashd\Model\Zone as ZoneEntity;
use Laminas\View\Model\ViewModel;

class NodeController extends AbstractAccountController implements DoctrineManagerInterface, WorkerServiceInterface {
    use DoctrineManagerTrait;
    use WorkerServiceTrait;

    public function indexAction () {
        return $this->redirectTo('list');
    }

    public function listAction () {
        $this->getLayoutService()->setTitle('Nodes');
        $this->getMenu()->setActiveId('node-list');
    
        $em = $this->getEntityManager();

        $filterForm = new NodeFilter($this->getEnvironmentOptions(null, false), $this->getServicesOptions(false));
        $filterForm->setData($this->getQueryParam());
    
        ////////////////////////////////////////////////////////
        // Query Nodes joined to their services and environments
        ////////////////////////////////////////////////////////
        $qb = $em->createQueryBuilder();
        $nqb = $qb->select('n, e, ns, s, nsz, z')
            ->from(NodeEntity::class, 'n')
            ->leftJoin('n.nodeServices', 'ns')
            ->leftJoin('ns.service', 's')
            ->leftJoin('ns.nodeServiceZones', 'nsz')
            ->leftJoin('nsz.zone', 'z')
            ->leftJoin('n.environment', 'e')
            ->addOrderBy('e.sortOrder')
            ->addOrderBy('n.sortOrder')
            ->addOrderBy('ns.sortOrder')
            ->addOrderBy('s.sortOrder');

        $sh = new SearchHelper($nqb);

        $sh->addWhereCondition('n.account', $this->getAccount());

        $sh->addWhereCondition('e.environmentId', $filterForm->getElementValue('environment'));
        $sh->addWhereCondition('n.nodeName', $filterForm->getElementValue('node_name'), true);
        $sh->addWhereCondition('n.hostname', $filterForm->getElementValue('node_hostname'), true);

        $sh->addWhereCondition('s.serviceId', $filterForm->getElementValue('service'));
        $sh->addWhereCondition('ns.nodeServiceName', $filterForm->getElementValue('node_service_name'), true);

        return new ViewModel([
            'nodes' => $sh->getResult(),
            'filterForm' => $filterForm
        ]);
    }

    public function editAction () {
        $em = $this->getEntityManager();

        $nodeId = $this->getParam('nid');

        if ($nodeId) {
            $this->getLayoutService()->setTitle('Edit Node');
            $this->getMenu()->setActiveId('node-edit');

            $nodeRepo = $em->getRepository(NodeEntity::class);

            /** @var \Updashd\Model\Node $node */
            $node = $nodeRepo->findOneBy(['nodeId' => $this->getParam('nid')]);

            if (! $node) {
                $this->flashMessenger()->addMessage('Node not found.', 'error');
                return $this->redirectTo('list');
            }
        }
        else {
            $this->getLayoutService()->setTitle('Add Node');
            $this->getMenu()->setActiveId('node-add');

            $node = new NodeEntity();
        }

        $form = new NodeForm();
        $form->get('environment_id')->setValueOptions($this->getEnvironmentOptions());

        $environmentId = $this->getParam('env');
    
        if ($this->isPost() && $form->isDataValid($this->getPostParam())) {
            // Save the entity
            $node->setEnvironment($em->getReference(EnvironmentEntity::class, $form->getElementValue('environment_id')));
            $node->setAccount($this->getAccount());
        
            $form->saveToEntity($node);
        
            $em->persist($node);
            $em->flush();

            $this->flashMessenger()->addMessage('Node saved.', 'success');
            return $this->redirectTo('edit', ['nid' => $node->getNodeId()]);
        }
        else {
            $form->hydrateFromEntity($node);
            $environment = $node->getEnvironment();
            $form->get('environment_id')->setValue($node->getEnvironment() ? $environment->getEnvironmentId() : $environmentId);
        }

        return new ViewModel([
            'form' => $form,
            'node' => $node,
            'environmentId' => $environmentId,
        ]);
    }
    
    public function serviceBasicAction () {
        $em = $this->getEntityManager();
        $this->getMenu()->setActiveId('node-service-basic');

        $form = new NodeServiceForm();

        /** @var MultiSelect $serviceFormElem */
        $serviceFormElem = $form->get('service_id');
        $serviceFormElem->setValueOptions($this->getServicesOptions());

        /** @var MultiSelect $zonesFormElem */
        $zonesFormElem = $form->get('zone_ids');
        $zonesFormElem->setValueOptions($this->getZoneOptions());

        $nodeServiceId = $this->getParam('nsid');
        $nodeId = $this->getParam('nid');

        $nodeService = null;
        $node = null;
        $currentZoneIds = [];

        if ($nodeServiceId) {
            $this->getLayoutService()->setTitle('Edit Service For Node');

            $nodeService = $this->getNodeService($nodeServiceId);

            if (! $nodeService) {
                $this->flashMessenger()->addMessage('Node Service not found.', 'error');
                return $this->redirectTo('list');
            }

            $form->hydrateFromEntity($nodeService);

            $serviceFormElem->setValue($nodeService->getService()->getServiceId());

            $nszRepo = $em->getRepository(NodeServiceZone::class);

            /** @var NodeServiceZone[] $nodeServiceZones */
            $nodeServiceZones = $nszRepo->findBy(['nodeService' => $nodeService]);

            foreach ($nodeServiceZones as $nodeServiceZone) {
                $currentZoneIds[] = $nodeServiceZone->getZone()->getZoneId();
            }

            $zonesFormElem->setValue($currentZoneIds);
        }
        elseif ($nodeId) {
            $this->getLayoutService()->setTitle('Add Service For Node');

            $node = $this->getNode($nodeId);

            $nodeService = new NodeServiceEntity();
            $nodeService->setNode($node);

            // Create the service as disabled
            $nodeService->setIsEnabled(false);

            $nodeServiceZones = [];
        }
        else {
            $this->flashMessenger()->addMessage('No Node Id or Node Service Id specified.', 'error');
            return $this->redirectTo('list');
        }

        if ($this->isPost() && $form->isDataValid($this->getPostParam())) {
            // Save the entity
            $service = $em->getReference(ServiceEntity::class, $form->getElementValue('service_id'));

            $form->saveToEntity($nodeService);

            $nodeService->setService($service);
            
            $newZoneIds = $form->getElementValue('zone_ids');
            
            $this->updateZones($nodeService, $nodeServiceZones, $newZoneIds);
            
            $em->persist($nodeService);
        
            $em->flush();

            $this->flashMessenger()->addMessage('Node Service saved.', 'success');
            return $this->redirectTo('service_basic', ['nsid' => $nodeService->getNodeServiceId()]);
        }
    
        return new ViewModel([
            'form' => $form,
            'nodeService' => $nodeService
        ]);
    }

    public function serviceConfigAction () {
        $this->getMenu()->setActiveId('node-service-config');
        $this->getLayoutService()->setTitle('Edit Service For Node');

        $em = $this->getEntityManager();

        $nodeServiceId = $this->getParam('nsid');

        $nodeService = $this->getNodeService($nodeServiceId);

        if (! $nodeService) {
            $this->flashMessenger()->addMessage('Node Service not found.', 'error');
            return $this->redirectTo('list');
        }

        $config = $this->getWorkerService()->getServiceWorkerConfig($nodeService->getService()->getModuleName());

        $form = new NodeServiceConfig($config);
        $form->hydrateFromJson($nodeService->getConfig());

        if ($this->isPost() && $form->isDataValid($this->getPostParam())) {
            $nodeService->setConfig($form->saveToJson());
            $em->persist($nodeService);
            $em->flush();

            $this->flashMessenger()->addMessage('Service configuration saved.', 'success');
            return $this->redirectTo('service_config', ['nsid' => $nodeServiceId]);
        }

        return new ViewModel([
            'form' => $form,
            'nodeService' => $nodeService
        ]);
    }

    public function setStatusAction () {
        $em = $this->getEntityManager();

        $nodeId = $this->getParam('nid');
        $nodeServiceId = $this->getParam('nsid');
        $enabled = $this->getParam('enabled') ? true : false;
        $fromAction = $this->getParam('from_action');

        $object = null;
        $type = null;
        $params = null;

        if ($nodeId) {
            $object = $this->getNode($nodeId);
            $type = 'Node';
            $params = ['nid' => $nodeId];
            $fromAction = $fromAction ?: 'edit';
        }
        elseif ($nodeServiceId) {
            $object = $this->getNodeService($nodeServiceId);
            $type = 'Service';
            $params = ['nsid' => $nodeServiceId];
            $fromAction = $fromAction ?: 'service_basic';
        }
        else {
            $this->flashMessenger()->addMessage('No Node Id or Node Service Id Provided', 'error');
            return $this->redirectTo('list');
        }

        if (! $object) {
            $this->flashMessenger()->addMessage($type . ' not found.', 'error');
            return $this->redirectTo('list');
        }

        $object->setIsEnabled($enabled);

        $em->persist($object);
        $em->flush();

        $this->flashMessenger()->addMessage($type . ' ' . ($object->getIsEnabled() ? 'Enabled' : 'Disabled'), 'success');
        return $this->redirectTo($fromAction, $params);
    }

    public function deleteAction () {
        $em = $this->getEntityManager();

        $nodeId = $this->getParam('nid');

        if (! $nodeId) {
            $this->flashMessenger()->addMessage('Node Id not specified.', 'error');
            return $this->redirectTo('list');
        }

        $node = $this->getNode($nodeId);

        if (! $node) {
            $this->flashMessenger()->addMessage('Node not found.', 'error');
            return $this->redirectTo('list');
        }

        $em->remove($node);
        $em->flush();

        $this->flashMessenger()->addMessage('Node Deleted', 'success');
        return $this->redirectTo('list');
    }

    public function deleteServiceAction () {
        $em = $this->getEntityManager();

        $nodeServiceId = $this->getParam('nsid');

        if (! $nodeServiceId) {
            $this->flashMessenger()->addMessage('Node Service Id not specified.', 'error');
            return $this->redirectTo('list');
        }

        $nodeService = $this->getNodeService($nodeServiceId);

        if (! $nodeService) {
            $this->flashMessenger()->addMessage('Node Service not found.', 'error');
            return $this->redirectTo('list');
        }

        $em->remove($nodeService);
        $em->flush();

        $this->flashMessenger()->addMessage('Service Deleted', 'success');
        return $this->redirectTo('list');
    }

    /**
     * @param $nodeId
     * @return NodeEntity|null|object
     */
    protected function getNode ($nodeId) {
        $em = $this->getEntityManager();

        $nodeRepo = $em->getRepository(NodeEntity::class);

        return $nodeRepo->find($nodeId);
    }

    /**
     * Get the specified node service
     * @param $nodeServiceId
     * @return NodeServiceEntity|null
     */
    protected function getNodeService ($nodeServiceId) {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();
        $nqb = $qb->select('ns, n, s, nsz, z')
            ->from(NodeServiceEntity::class, 'ns')
            ->leftJoin('ns.node', 'n')
            ->leftJoin('ns.service', 's')
            ->leftJoin('ns.nodeServiceZones', 'nsz')
            ->leftJoin('nsz.zone', 'z')
            ->addOrderBy('ns.sortOrder')
            ->addOrderBy('s.sortOrder')
            ->where($qb->expr()->andX(
                $qb->expr()->eq('n.account', ':account'),
                $qb->expr()->eq('ns.nodeServiceId', ':nodeServiceId')
            ));

        $nodeQuery = $nqb->getQuery();
        $nodeQuery->setParameter('account', $this->getAccount());
        $nodeQuery->setParameter('nodeServiceId', $nodeServiceId);

        /** @var NodeServiceEntity $nodeService */
        return $nodeQuery->getOneOrNullResult();
    }
    
    /**
     * @param NodeServiceEntity $nodeService
     * @param NodeServiceZone[] $nodeServiceZones
     * @param array $newZoneIds
     */
    protected function updateZones ($nodeService, $nodeServiceZones, $newZoneIds) {
        $em = $this->getEntityManager();
        
        foreach ($nodeServiceZones as $nodeServiceZone) {
            $zoneId = $nodeServiceZone->getZone()->getZoneId();
            
            // If the zone should stay assigned
            if (($index = array_search($zoneId, $newZoneIds)) !== false) {
                // Remove it from new ids (so it isn't inserted later
                unset($newZoneIds[$index]);
            }
            else {
                // Remove the entity
                $em->remove($nodeServiceZone);
            }
        }
        
        foreach ($newZoneIds as $newZoneId) {
            $nodeServiceZone = new NodeServiceZone();
            $nodeServiceZone->setNodeService($nodeService);
            $nodeServiceZone->setZone($em->getReference(ZoneEntity::class, $newZoneId));
            $em->persist($nodeServiceZone);
        }
    }
    
    protected function getServicesOptions ($includeNullOption = true) {
        $em = $this->getEntityManager();

        $eqb = $em->createQueryBuilder();
        $envQuery = $eqb->select('s')
            ->from(ServiceEntity::class, 's')
            ->where($eqb->expr()->orX(
                $eqb->expr()->eq('s.account', ':account'),
                $eqb->expr()->isNull('s.account')
            ))
            ->addOrderBy('s.sortOrder')
            ->getQuery();

        $envQuery->setParameter('account', $this->getAccount());

        /** @var ServiceEntity[] $services */
        $services = $envQuery->getResult();

        $options = $includeNullOption ? ['' => 'Select...'] : [];
        
        foreach ($services as $service) {
            $options[$service->getServiceId()] = $service->getServiceName();
        }

        return $options;
    }
    
    protected function getZoneOptions () {
        $em = $this->getEntityManager();
        
        $qb = $em->createQueryBuilder();
        $zqb = $qb->select('z')
            ->from(ZoneEntity::class, 'z')
            ->addOrderBy('z.sortOrder')
            ->where($qb->expr()->orX(
                $qb->expr()->eq('z.account', ':account'),
                $qb->expr()->isNull('z.account')
            ));

        $zoneQuery = $zqb->getQuery();
        $zoneQuery->setParameter('account', $this->getAccount());

        $options = [];
        /** @var ZoneEntity $zone */
        foreach ($zoneQuery->getResult() as $zone) {
            $options[$zone->getZoneId()] = $zone->getZoneName();
        }
        
        return $options;
    }

    /**
     * @param EnvironmentEntity[] $environments
     * @return array
     */
    protected function getEnvironmentOptions ($environments = null, $includeNullOption = true) {
        if (! $environments) {
            $em = $this->getEntityManager();

            $environmentRepo = $em->getRepository(EnvironmentEntity::class);

            /** @var EnvironmentEntity[] $environments */
            $environments = $environmentRepo->findBy(['account' => $this->getAccount()]);
        }

        $options = $includeNullOption ? ['' => 'Select...'] : [];

        foreach ($environments as $environment) {
            $options[$environment->getEnvironmentId()] = $environment->getEnvironmentName();
        }

        return $options;
    }
}
