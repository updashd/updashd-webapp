<?php
namespace Application\Controller\Account;

use Application\Doctrine\SearchHelper;
use Application\Form\ResultFilter;
use Application\Pagination\Paginator as ApplicationPaginator;
use Application\Service\ServiceInterface\AuthServiceInterface;
use Application\Service\ServiceInterface\DoctrineManagerInterface;
use Application\Service\ServiceTrait\AuthServiceTrait;
use Application\Service\ServiceTrait\DoctrineManagerTrait;
use Updashd\Model\Environment;
use Updashd\Model\Result;
use Updashd\Model\Service;
use Updashd\Model\Severity;
use Updashd\Model\Zone;
use Laminas\View\Model\ViewModel;

class ResultController extends AbstractAccountController implements DoctrineManagerInterface, AuthServiceInterface {
    use DoctrineManagerTrait;
    use AuthServiceTrait;

    use DateRangeTrait;

    public function init () {
        parent::init();

        $userTimeZone = $this->getPersonSettingService()->getPersonSetting(
            $this->getAuthService()->getPerson(),
            'timezone'
        );

        // This is implemented on the trait
        $this->initDateRange($userTimeZone, $this->getParam('range'));
    }


    public function indexAction () {
        return $this->redirectTo('list');
    }

    public function listAction () {
        $this->getLayoutService()->setTitle('Result List');
        $this->getMenu()->setActiveId('result-list');

        $em = $this->getEntityManager();

        $resultFilter = new ResultFilter(
            $this->getEnvironmentOptions(),
            $this->getServicesOptions(),
            $this->getZoneOptions(),
            $this->getSeverityOptions()
        );
        $resultFilter->setData($this->getQueryParam());

        $qb = $em->createQueryBuilder();
        $iqb = $qb->select('r, sv, ns, z, n, s, e, i')
            ->from(Result::class, 'r')
            ->innerJoin('r.statusCode', 'sv')
            ->innerJoin('r.nodeService', 'ns')
            ->innerJoin('r.zone', 'z')
            ->innerJoin('ns.node', 'n')
            ->innerJoin('ns.service', 's')
            ->innerJoin('n.environment', 'e')
            ->leftJoin('r.incident', 'i')
            ->groupBy('r.resultId')
            ->addOrderBy('r.endTime')
            ->addOrderBy('r.resultId');

        $sh = new SearchHelper($iqb);

        $sh->addWhereConditionBetween('r.startTime', $this->getStart(), $this->getEnd());

        $sh->addWhereCondition('n.account', $this->getAccount());

        $sh->addWhereCondition('e.environmentId', $resultFilter->getElementValue('environment'));
        $sh->addWhereCondition('sv.severityId', $resultFilter->getElementValue('severity'));
        $sh->addWhereCondition('z.zoneId', $resultFilter->getElementValue('zone'));

        $sh->addWhereCondition('n.nodeName', $resultFilter->getElementValue('node_name'), true);
        $sh->addWhereCondition('ns.nodeServiceName', $resultFilter->getElementValue('node_service_name'), true);

        $sh->addWhereCondition('s.serviceId', $resultFilter->getElementValue('service'));

        $paginator = new ApplicationPaginator($sh->getQuery(), $this->getQueryParam(), true);

        return new ViewModel([
            'start' => $this->getStart(),
            'end' => $this->getEnd(),
            'form' => $resultFilter,
            'paginator' => $paginator
        ]);
    }

    public function detailAction () {
        $this->getLayoutService()->setTitle('Result Detail');
        $this->getMenu()->setActiveId('result-detail');

        $resultId = $this->getParam('rid');

        if (! $resultId) {
            $this->flashMessenger()->addMessage('Result Id missing.', 'error');
            return $this->redirectTo('list');
        }

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();
        $iqb = $qb->select('r, rm, ns, z, n, s, e')
            ->from(Result::class, 'r')
            ->innerJoin('r.nodeService', 'ns')
            ->innerJoin('r.zone', 'z')
            ->innerJoin('ns.node', 'n')
            ->innerJoin('ns.service', 's')
            ->innerJoin('n.environment', 'e')
            ->leftJoin('r.incident', 'i')
            ->leftJoin('r.resultMetrics', 'rm')
            ->addOrderBy('r.endTime')
            ->addOrderBy('r.resultId');

        $iqb->where($iqb->expr()->andX(
            $iqb->expr()->eq('n.account', ':account'),
            $iqb->expr()->eq('r.resultId', ':resultId')
        ));

        $query = $iqb->getQuery();
        $query->setParameter('account', $this->getAccount());
        $query->setParameter('resultId', $resultId);

        /** @var Result $result */
        $result = $query->getOneOrNullResult();

        if ($result === null) {
            $this->flashMessenger()->addMessage('Result not found.', 'error');
            return $this->redirectTo('list');
        }

        return new ViewModel([
            'result' => $result
        ]);
    }

    protected function getServicesOptions ($includeNullOption = true) {
        $em = $this->getEntityManager();

        $eqb = $em->createQueryBuilder();
        $envQuery = $eqb->select('s')
            ->from(Service::class, 's')
            ->where($eqb->expr()->orX(
                $eqb->expr()->eq('s.account', ':account'),
                $eqb->expr()->isNull('s.account')
            ))
            ->addOrderBy('s.sortOrder')
            ->getQuery();

        $envQuery->setParameter('account', $this->getAccount());

        /** @var Service[] $services */
        $services = $envQuery->getResult();

        $options = $includeNullOption ? ['' => 'Select...'] : [];

        foreach ($services as $service) {
            $options[$service->getServiceId()] = $service->getServiceName();
        }

        return $options;
    }

    protected function getZoneOptions () {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();
        $zqb = $qb->select('z')
            ->from(Zone::class, 'z')
            ->addOrderBy('z.sortOrder')
            ->where($qb->expr()->orX(
                $qb->expr()->eq('z.account', ':account'),
                $qb->expr()->isNull('z.account')
            ));

        $zoneQuery = $zqb->getQuery();
        $zoneQuery->setParameter('account', $this->getAccount());

        $options = [];
        /** @var Zone $zone */
        foreach ($zoneQuery->getResult() as $zone) {
            $options[$zone->getZoneId()] = $zone->getZoneName();
        }

        return $options;
    }

    /**
     * @param Environment[] $environments
     * @param bool $includeNullOption
     * @return array
     */
    protected function getEnvironmentOptions ($environments = null, $includeNullOption = true) {
        if (! $environments) {
            $em = $this->getEntityManager();

            $zqb = $em->createQueryBuilder();
            $zqb
                ->select('e')
                ->from(Environment::class, 'e')
                ->addOrderBy('e.sortOrder')
                ->where($zqb->expr()->eq('e.account', ':account'));

            $envQ = $zqb->getQuery();
            $envQ->setParameter('account', $this->getAccount());


            /** @var Environment[] $environments */
            $environments = $envQ->getResult();
        }

        $options = $includeNullOption ? ['' => 'Select...'] : [];

        foreach ($environments as $environment) {
            $options[$environment->getEnvironmentId()] = $environment->getEnvironmentName();
        }

        return $options;
    }

    /**
     * @param Severity[] $severities
     * @param bool $includeNullOption
     * @return array
     */
    protected function getSeverityOptions ($severities = null, $includeNullOption = true) {
        if (! $severities) {
            $em = $this->getEntityManager();

            $sqb = $em->createQueryBuilder();
            $sqb
                ->select('s')
                ->from(Severity::class, 's')
                ->addOrderBy('s.sortOrder');

            $sevQ = $sqb->getQuery();

            /** @var Severity[] $severities */
            $severities = $sevQ->getResult();
        }

        $options = $includeNullOption ? ['' => 'Select...'] : [];

        foreach ($severities as $severity) {
            $options[$severity->getSeverityId()] = $severity->getSeverityName();
        }

        return $options;
    }
}
