<?php
namespace Application\Controller\Account;

use Application\Doctrine\SearchHelper;
use Application\Form\IncidentFilter;
use Application\Pagination\Paginator as ApplicationPaginator;
use Application\Service\ServiceInterface\AuthServiceInterface;
use Application\Service\ServiceInterface\DoctrineManagerInterface;
use Application\Service\ServiceTrait\AuthServiceTrait;
use Application\Service\ServiceTrait\DoctrineManagerTrait;
use Updashd\Model\Environment;
use Updashd\Model\Incident;
use Updashd\Model\Incident as IncidentEntity;
use Updashd\Model\Service;
use Updashd\Model\Severity;
use Updashd\Model\Zone;
use Laminas\View\Model\ViewModel;

class IncidentController extends AbstractAccountController implements DoctrineManagerInterface, AuthServiceInterface {
    use DoctrineManagerTrait;
    use AuthServiceTrait;

    use DateRangeTrait;

    public function init () {
        parent::init();

        $userTimeZone = $this->getPersonSettingService()->getPersonSetting(
            $this->getAuthService()->getPerson(),
            'timezone'
        );

        // This is implemented on the trait
        $this->initDateRange($userTimeZone, $this->getParam('range'));
    }

    public function indexAction () {
        return $this->redirectTo('list');
    }

    public function listAction () {
        $this->getLayoutService()->setTitle('Incident List');
        $this->getMenu()->setActiveId('incident-list');

        $em = $this->getEntityManager();

        $incidentFilterForm = new IncidentFilter(
            $this->getEnvironmentOptions(),
            $this->getServicesOptions(),
            $this->getZoneOptions(),
            $this->getSeverityOptions()
        );
        $incidentFilterForm->setData($this->getQueryParam());

        $qb = $em->createQueryBuilder();
        $iqb = $qb->select('i, sv, ns, z, n, s, e')
            ->from(IncidentEntity::class, 'i')
            ->innerJoin('i.severity', 'sv')
            ->innerJoin('i.nodeService', 'ns')
            ->innerJoin('i.zone', 'z')
            ->innerJoin('ns.node', 'n')
            ->innerJoin('ns.service', 's')
            ->innerJoin('n.environment', 'e')
            ->groupBy('i.incidentId')
            ->addOrderBy('i.dateLastSeen')
            ->addOrderBy('i.incidentId');

        $sh = new SearchHelper($iqb);

        $sh->addWhereConditionBetween('i.dateLastSeen', $this->getStart(), $this->getEnd());

        $sh->addWhereCondition('i.account', $this->getAccount());

        $sh->addWhereCondition('e.environmentId', $incidentFilterForm->getElementValue('environment'));
        $sh->addWhereCondition('sv.severityId', $incidentFilterForm->getElementValue('severity'));
        $sh->addWhereCondition('z.zoneId', $incidentFilterForm->getElementValue('zone'));

        $sh->addWhereCondition('i.isRead', $incidentFilterForm->getElementValue('read'));
        $sh->addWhereCondition('i.isResolved', $incidentFilterForm->getElementValue('resolved'));

        $sh->addWhereCondition('n.nodeName', $incidentFilterForm->getElementValue('node_name'), true);
        $sh->addWhereCondition('ns.nodeServiceName', $incidentFilterForm->getElementValue('node_service_name'), true);

        $sh->addHavingCondition('s.serviceId', $incidentFilterForm->getElementValue('service'));

        $paginator = new ApplicationPaginator($sh->getQuery(), $this->getQueryParam(), true);

        return new ViewModel([
            'start' => $this->getStart(),
            'end' => $this->getEnd(),
            'form' => $incidentFilterForm,
            'paginator' => $paginator
        ]);
    }

    public function detailAction () {
        $this->getLayoutService()->setTitle('Incident Detail');
        $this->getMenu()->setActiveId('incident-detail');

        $incidentId = $this->getParam('iid');

        if (! $incidentId) {
            $this->flashMessenger()->addMessage('Incident Id missing.', 'error');
            return $this->redirectTo('list');
        }

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();
        $iqb = $qb->select('i, ns, z, n, s, e, rs, rm')
            ->from(IncidentEntity::class, 'i')
            ->innerJoin('i.nodeService', 'ns')
            ->innerJoin('i.zone', 'z')
            ->innerJoin('ns.node', 'n')
            ->innerJoin('ns.service', 's')
            ->innerJoin('n.environment', 'e')
            ->leftJoin('i.results', 'rs')
            ->leftJoin('rs.resultMetrics', 'rm')
            ->addOrderBy('i.dateLastSeen')
            ->addOrderBy('i.incidentId');

        $iqb->where($iqb->expr()->andX(
            $iqb->expr()->eq('i.account', ':account'),
            $iqb->expr()->eq('i.incidentId', ':incidentId')
        ));

        $query = $iqb->getQuery();
        $query->setParameter('account', $this->getAccount());
        $query->setParameter('incidentId', $incidentId);

        /** @var Incident $incident */
        $incident = $query->getOneOrNullResult();

        if ($incident === null) {
            $this->flashMessenger()->addMessage('Incident not found.', 'error');
            return $this->redirectTo('list');
        }

        // Mark as read
        if (! $incident->getIsRead()) {
            $incident->setIsRead(true);
            $em->persist($incident);
            $em->flush();
        }

        return new ViewModel([
            'incident' => $incident
        ]);
    }

    protected function getServicesOptions ($includeNullOption = true) {
        $em = $this->getEntityManager();

        $eqb = $em->createQueryBuilder();
        $envQuery = $eqb->select('s')
            ->from(Service::class, 's')
            ->where($eqb->expr()->orX(
                $eqb->expr()->eq('s.account', ':account'),
                $eqb->expr()->isNull('s.account')
            ))
            ->addOrderBy('s.sortOrder')
            ->getQuery();

        $envQuery->setParameter('account', $this->getAccount());

        /** @var Service[] $services */
        $services = $envQuery->getResult();

        $options = $includeNullOption ? ['' => 'Select...'] : [];

        foreach ($services as $service) {
            $options[$service->getServiceId()] = $service->getServiceName();
        }

        return $options;
    }

    protected function getZoneOptions () {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();
        $zqb = $qb->select('z')
            ->from(Zone::class, 'z')
            ->addOrderBy('z.sortOrder')
            ->where($qb->expr()->orX(
                $qb->expr()->eq('z.account', ':account'),
                $qb->expr()->isNull('z.account')
            ));

        $zoneQuery = $zqb->getQuery();
        $zoneQuery->setParameter('account', $this->getAccount());

        $options = [];
        /** @var Zone $zone */
        foreach ($zoneQuery->getResult() as $zone) {
            $options[$zone->getZoneId()] = $zone->getZoneName();
        }

        return $options;
    }

    /**
     * @param Environment[] $environments
     * @param bool $includeNullOption
     * @return array
     */
    protected function getEnvironmentOptions ($environments = null, $includeNullOption = true) {
        if (! $environments) {
            $em = $this->getEntityManager();

            $zqb = $em->createQueryBuilder();
            $zqb
                ->select('e')
                ->from(Environment::class, 'e')
                ->addOrderBy('e.sortOrder')
                ->where($zqb->expr()->eq('e.account', ':account'));

            $envQ = $zqb->getQuery();
            $envQ->setParameter('account', $this->getAccount());


            /** @var Environment[] $environments */
            $environments = $envQ->getResult();
        }

        $options = $includeNullOption ? ['' => 'Select...'] : [];

        foreach ($environments as $environment) {
            $options[$environment->getEnvironmentId()] = $environment->getEnvironmentName();
        }

        return $options;
    }

    /**
     * @param Severity[] $severities
     * @param bool $includeNullOption
     * @return array
     */
    protected function getSeverityOptions ($severities = null, $includeNullOption = true) {
        if (! $severities) {
            $em = $this->getEntityManager();

            $sqb = $em->createQueryBuilder();
            $sqb
                ->select('s')
                ->from(Severity::class, 's')
                ->addOrderBy('s.sortOrder');

            $sevQ = $sqb->getQuery();

            /** @var Severity[] $severities */
            $severities = $sevQ->getResult();
        }

        $options = $includeNullOption ? ['' => 'Select...'] : [];

        foreach ($severities as $severity) {
            $options[$severity->getSeverityId()] = $severity->getSeverityName();
        }

        return $options;
    }
}
