<?php
namespace Application\Controller\Account;

use Application\Controller\AbstractController;
use Application\Service\ServiceInterface\DoctrineManagerInterface;
use Application\Service\ServiceInterface\PersonSettingServiceInterface;
use Application\Service\ServiceTrait\DoctrineManagerTrait;
use Application\Service\ServiceTrait\PersonSettingServiceTrait;
use Updashd\Model\Account;

abstract class AbstractAccountController extends AbstractController
    implements DoctrineManagerInterface, PersonSettingServiceInterface {
    use DoctrineManagerTrait;
    use PersonSettingServiceTrait;
    
    private $account;

    public function init () {
        parent::init();
        
        // Set the frontend menu to default
        $this->getMenu()->setDefaultItemById('account-frontend');

        // Get account nme from URL
        $slug = $this->getEvent()->getRouteMatch()->getParam('account');

        // Redirect to dashboard for the current user if no account is specified
        if (! $slug) {
            return $this->redirectTo('index', [], 'index', 'portal');
        }

        // Get the entity manager
        $em = $this->getEntityManager();

        $accountRepo = $em->getRepository(Account::class);

        /** @var Account $account */
        $account = $accountRepo->findOneBy(['slug' => $slug]);

        // Redrect to the dashboard if the requested account does not exist
        if (! $account) {
            throw new \Exception('Account does not exist.', 404);
        }

        $this->setAccount($account);
    }

    /**
     * @return Account|null
     */
    public function getAccount () {
        return $this->account;
    }

    /**
     * @param Account $account
     */
    public function setAccount (Account $account) {
        $this->account = $account;
    }
}