<?php
namespace Application\Controller\Open;

use Application\Service\ServiceInterface\AuthServiceInterface;
use Application\Service\ServiceInterface\DoctrineManagerInterface;
use Application\Service\ServiceTrait\AuthServiceTrait;
use Application\Service\ServiceTrait\DoctrineManagerTrait;
use Doctrine\ORM\Query\Expr\Join;
use Updashd\Model\Account;
use Updashd\Model\NodeServiceZone;
use Updashd\Model\ResultLatest;
use Laminas\View\Model\ViewModel;

class DashboardController extends \Application\Controller\AbstractController
    implements DoctrineManagerInterface, AuthServiceInterface {
    use DoctrineManagerTrait;
    use AuthServiceTrait;

    protected $account;

    public function init () {
        // Intentionally don't require auth

        // Get account nme from URL
        $slug = $this->getEvent()->getRouteMatch()->getParam('account');

        // Redirect to dashboard for the current user if no account is specified
        if (! $slug) {
            throw new \Exception('No account slug provided.');
        }

        // Get the entity manager
        $em = $this->getEntityManager();

        $accountRepo = $em->getRepository(Account::class);

        /** @var Account $account */
        $account = $accountRepo->findOneBy(['slug' => $slug]);

        // Redrect to the dashboard if the requested account does not exist
        if (! $account) {
            throw new \Exception('Account does not exist.', 404);
        }

        $this->setAccount($account);

        $this->getMenu()->setDefaultItemById('open-frontend');
    }

    public function indexAction () {
        return $this->redirectTo('status');
    }

    public function statusAction () {
        $this->getLayoutService()->setTitle('Dashboard');
        $this->getLayoutService()->setDescription('Current Status');
        $this->getMenu()->setActiveId('open-dashboard-status');

        $em = $this->getEntityManager();

        $statusQ = $em->createQueryBuilder();
        $statusQ
            ->select('rs, s, ns, n, e')
            ->from(ResultLatest::class, 'rs')
            ->innerJoin(NodeServiceZone::class, 'nsz', Join::WITH, $statusQ->expr()->andX(
                $statusQ->expr()->eq('nsz.nodeService', 'rs.nodeService'),
                $statusQ->expr()->eq('nsz.zone', 'rs.zone')
            ))
            ->innerJoin('rs.statusCode', 's')
            ->innerJoin('rs.nodeService', 'ns')
            ->innerJoin('ns.node', 'n')
            ->innerJoin('n.environment', 'e')
            ->addOrderBy('s.sortOrder', 'DESC')
            ->addOrderBy('e.sortOrder', 'ASC')
            ->addOrderBy('n.sortOrder', 'ASC')
            ->addOrderBy('n.nodeName', 'ASC')
            ->addOrderBy('ns.sortOrder', 'ASC')
            ->addOrderBy('ns.nodeServiceName', 'ASC')
            ->where($statusQ->expr()->andX(
                $statusQ->expr()->eq('n.account', ':account')),
                $statusQ->expr()->eq('n.isEnabled', 1),
                $statusQ->expr()->eq('ns.isEnabled', 1)
            );

        $statusQ->setParameter('account', $this->getAccount());

        $statusesSummary = $statusQ->getQuery()->getResult();

        return new ViewModel([
            'statusesSummary' => $statusesSummary
        ]);
    }

    /**
     * @return \Updashd\Model\Account
     */
    public function getAccount () {
        return $this->account;
    }

    /**
     * @param \Updashd\Model\Account $account
     */
    public function setAccount ($account) {
        $this->account = $account;
    }
}