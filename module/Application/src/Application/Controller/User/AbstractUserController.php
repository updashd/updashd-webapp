<?php
namespace Application\Controller\User;


use Application\Controller\AbstractController;
use Application\Service\ServiceInterface\AccountServiceInterface;
use Application\Service\ServiceInterface\DoctrineManagerInterface;
use Application\Service\ServiceInterface\PersonSettingServiceInterface;
use Application\Service\ServiceTrait\AccountServiceTrait;
use Application\Service\ServiceTrait\DoctrineManagerTrait;
use Application\Service\ServiceTrait\PersonSettingServiceTrait;
use Updashd\Model\Person;

class AbstractUserController extends AbstractController
    implements DoctrineManagerInterface, PersonSettingServiceInterface, AccountServiceInterface {
    use DoctrineManagerTrait;
    use PersonSettingServiceTrait;
    use AccountServiceTrait;

    protected $person;

    public function init () {
        parent::init();

        // Set the frontend menu to default
        $this->getMenu()->setDefaultItemById('user-frontend');

        // Get the person entity attached to the account
        $personId = $this->getAuthService()->getPersonId();
        $em = $this->getEntityManager();
        $personRepo = $em->getRepository(Person::class);

        /** @var Person $person */
        $person = $personRepo->find($personId);

        $this->setPerson($person);
    }

    /**
     * @return Person
     */
    protected function getPerson () {
        return $this->person;
    }

    /**
     * @param Person $person
     */
    protected function setPerson ($person) {
        $this->person = $person;
    }
}