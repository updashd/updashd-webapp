<?php
namespace Application\Controller\User;

use Application\Form\Element;
use Application\Form\User\Overview as OverviewForm;
use Application\Form\User\Password as PasswordForm;
use Application\Form\User\Settings as SettingsForm;
use Laminas\View\Model\ViewModel;

class IndexController extends AbstractUserController {
    public function indexAction () {
        return $this->redirectTo('overview');
    }

    public function overviewAction () {
        $this->getLayoutService()->setTitle('User');
        $this->getLayoutService()->setDescription('Overview');
        $this->getMenu()->setActiveId('user-overview');

        $person = $this->getPerson();

        $form = new OverviewForm();
        $form->hydrateFromEntity($person);

        if ($this->isPost() && $form->isDataValid($this->getPostParam())) {
            $person->setName($form->getElementValue($form::FIELD_NAME));
            $person->setEmail($form->getElementValue($form::FIELD_EMAIL));

            $em = $this->getEntityManager();
            $em->persist($person);
            $em->flush();

            $this->redirectTo('overview');
        }

        return new ViewModel([
            'person' => $person,
            'form' => $form
        ]);
    }

    public function passwordAction () {
        $this->getLayoutService()->setTitle('User');
        $this->getLayoutService()->setDescription('Password');
        $this->getMenu()->setActiveId('user-password');

        $form = new PasswordForm();
        $person = $this->getPerson();

        if ($this->isPost() && $form->isDataValid($this->getPostParam())) {
            $currentPassword = $form->getElementValue($form::FIELD_CURRENT_PASSWORD);
            $newPassword = $form->getElementValue($form::FIELD_NEW_PASSWORD);

            $accountService = $this->getAccountService();

            if ($accountService->checkPassword($person, $currentPassword)) {
                // Set the new password
                $accountService->setPassword($person, $newPassword);

                // Flash a message that it was changed successfully
                $this->addFlashMessage('Successfully changed password.', 'success');

                $this->redirectTo('password');
            }
            else {
                $form->setPasswordInvalid();
            }
        }

        return new ViewModel([
            'form' => $form
        ]);
    }

    public function settingsAction () {
        $this->getLayoutService()->setTitle('User');
        $this->getLayoutService()->setDescription('Settings');
        $this->getMenu()->setActiveId('user-settings');

        $form = new SettingsForm();
        $person = $this->getPerson();

        $personSettingService = $this->getPersonSettingService();

        /** @var Element $element */
        foreach ($form->getElements() as $element) {
            $name = $element->getName();

            $value = $personSettingService->getPersonSetting($person, $name);

            if ($value !== null) {
                $element->setValue($value);
            }
        }

        if ($this->isPost() && $form->isDataValid($this->getPostParam())) {
            /** @var Element $element */
            foreach ($form->getElements() as $element) {
                $name = $element->getName();
                $personSettingService->setPersonSetting($person, $name, $element->getValue());
                $personSettingService->save();
            }

            $this->addFlashMessage('Successfully updated settings.', 'success');

            $this->redirectTo('settings');
        }

        return new ViewModel([
            'form' => $form
        ]);
    }
}
