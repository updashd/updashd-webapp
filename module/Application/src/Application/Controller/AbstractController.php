<?php
namespace Application\Controller;

use Application\Service\ServiceInterface\AuthServiceInterface;
use Application\Service\ServiceInterface\LayoutServiceInterface;
use Application\Service\ServiceInterface\MenuInterface;
use Application\Service\ServiceTrait\AuthServiceTrait;
use Application\Service\ServiceTrait\LayoutServiceTrait;
use Application\Service\ServiceTrait\MenuTrait;
use Laminas\Http\Request;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\Mvc\MvcEvent;
use Laminas\Mvc\Plugin\FlashMessenger\FlashMessenger;
use Laminas\Stdlib\RequestInterface;

/**
 * Class AbstractController
 * @method FlashMessenger flashMessenger()
 * @package Application\Controller
 */
abstract class AbstractController extends AbstractActionController implements AuthServiceInterface, MenuInterface, LayoutServiceInterface  {
    use AuthServiceTrait;
    use MenuTrait;
    use LayoutServiceTrait;

    public function init () {
        $this->getAuthService()->requireAuth();
    }

    public function done () {
        // Hold some places
    }

    /**
     * Determine if this was a POST request.
     *
     * @return bool true if POST
     */
    public function isPost () {
        return ($this->getRequest()->getMethod() == Request::METHOD_POST);
    }

    /**
     * Determine if this was a GET request.
     *
     * @return bool true if GET
     */
    public function isGet () {
        return ($this->getRequest()->getMethod() == Request::METHOD_GET);
    }

    /**
     * Get a POST parameter by name.
     *
     * @param string $name
     * @param mixed $default
     * @return mixed|\Laminas\Stdlib\ParametersInterface
     */
    public function getPostParam ($name = null, $default = null) {
        return $this->getRequest()->getPost($name, $default);
    }

    /**
     * Get a GET parameter by name.
     *
     * @param string $name
     * @param mixed $default
     * @return mixed|\Laminas\Stdlib\ParametersInterface
     */
    public function getQueryParam ($name = null, $default = null) {
        return $this->getRequest()->getQuery($name, $default);
    }

    /**
     * Get a parameter by name. First, try GET query parameters, then POST parameters, otherwise null.
     *
     * @param string $key
     * @return mixed|null|\Laminas\Stdlib\ParametersInterface
     */
    public function getParam ($key) {
        $request = $this->getRequest();

        if ($request->getQuery($key, null)) {
            return $request->getQuery($key);
        }
        elseif ($request->getPost($key, null)) {
            return $request->getPost($key);
        }
        else {
            return null;
        }
    }

    /**
     * Get request object
     * I'm adding this to get rid of stupid warnings.
     * @return Request|RequestInterface
     */
    public function getRequest() {
        return parent::getRequest();
    }

    /**
     * Add a flash message.
     *
     * @param string $message
     * @param string $type One of: error, info, default, or success
     */
    public function addFlashMessage ($message, $type = 'default') {
        $this->flashMessenger()->addMessage($message, $type);
    }

    /**
     * Get the url for a certain route.
     *
     * @param string $action
     * @param array $getParams
     * @param string $controllerName
     * @param string $routeName
     * @param array $routeParams
     * @param bool $reuseParams
     * @return mixed|string
     */
    public function getUrl ($action, $getParams = [], $controllerName = null, $routeName = null, $routeParams = [], $reuseParams = true) {
        $event = $this->getEvent();
        $routeMatch = $event->getRouteMatch();

        $controllerName = $controllerName ?: ($routeMatch->getParam('_controller') ?: $routeMatch->getParam('controller'));
        $routeName = $routeName ?: $routeMatch->getMatchedRouteName();

        if ($reuseParams && $routeMatch) {
            $routeParams = array_replace_recursive($routeParams, $routeMatch->getParams());
        }

        // Replace any given parameters with the ones from the call
        $routeParams = array_replace_recursive($routeParams, ['controller' => $controllerName, 'action' => $action]);

        $url = $event->getRouter()->assemble($routeParams, ['name' => $routeName]);

        if (count($getParams)) {
            $url .= '?' . http_build_query($getParams);
        }

        return $url;
    }
    
    public function redirectTo($action, $getParams = [], $controllerName = null, $routeName = null, $routeParams = [], $reuseParams = true) {
        $url = $this->getUrl($action, $getParams, $controllerName, $routeName, $routeParams, $reuseParams);

        return $this->redirect()->toUrl($url);
    }
    
    public function attachDefaultListeners () {
        parent::attachDefaultListeners();

        $em = $this->getEventManager();

        $em->attach(MvcEvent::EVENT_DISPATCH, [$this, 'init'], 100);
        $em->attach(MvcEvent::EVENT_DISPATCH, [$this, 'done'], -100);
    }
}
