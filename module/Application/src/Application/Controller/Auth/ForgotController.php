<?php
namespace Application\Controller\Auth;

use Application\Form\Auth\ForgotPassword;
use Application\Form\Auth\ResetPassword;
use Application\Service\ServiceInterface\DoctrineManagerInterface;
use Application\Service\ServiceInterface\MailServiceInterface;
use Application\Service\ServiceInterface\SlugHelperServiceInterface;
use Application\Service\ServiceTrait\DoctrineManagerTrait;
use Application\Service\ServiceTrait\MailServiceTrait;
use Application\Service\ServiceTrait\SlugHelperServiceTrait;
use Laminas\View\Model\ViewModel;

class ForgotController extends AbstractAuthController
    implements DoctrineManagerInterface, SlugHelperServiceInterface, MailServiceInterface {
    use DoctrineManagerTrait;
    use SlugHelperServiceTrait;
    use MailServiceTrait;

    public function init () {
        parent::init();
        $this->layout('layout/auth');
    }

    public function indexAction () {
        $this->getLayoutService()->setTitle('Forgot Password');

        $form = new ForgotPassword();

        if ($this->isPost() && $form->isDataValid($this->getPostParam())) {
            $email = $form->getElementValue('email');

            $accountService = $this->getAccountService();
            $user = $accountService->getUserByIdentifier($email);

            if ($user) {
                $accountService->sendResetPasswordEmail($user);
            }

            $this->redirectTo('sent');
        }

        return new ViewModel([
            'form' => $form
        ]);
    }

    public function sentAction () {

    }

    public function resetAction () {
        $token = $this->getParam('t');

        $accountService = $this->getAccountService();

        $form = null;
        $reason = null;

        try {
            $pid = $accountService->processResetToken($token);

            $form = new ResetPassword();
            $form->setToken($token);
            $form->setAction($this->getUrl('reset'));

            if ($this->isPost() && $form->isDataValid($this->getPostParam())) {
                $person = $accountService->getPerson($pid);
                $accountService->setPassword($person, $form->getElementValue('password'));

                $this->redirectTo('success');
            }
        }
        catch (\Exception $e) {
            $reason = $e->getMessage();
        }

        return new ViewModel([
            'form' => $form,
            'reason' => $reason
        ]);
    }

    public function successAction () {

    }
}