<?php
namespace Application\Controller\Auth;

class IndexController extends AbstractAuthController {
    public function indexAction () {
        $this->redirectTo('index', [], 'signin', 'auth');
    }
}
