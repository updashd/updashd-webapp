<?php
namespace Application\Controller\Auth;

use Application\Form\Auth\SignIn as SignInForm;
use Application\Service\ServiceInterface\DoctrineManagerInterface;
use Application\Service\ServiceTrait\DoctrineManagerTrait;
use Application\Service\ServiceTrait\SlugHelperServiceTrait;
use Laminas\View\Model\ViewModel;

class SigninController extends AbstractAuthController implements DoctrineManagerInterface {
    use DoctrineManagerTrait;
    use SlugHelperServiceTrait;

    public function indexAction () {
        $this->getLayoutService()->setTitle('Sign In');
        $this->layout('layout/auth');

        if ($this->getAuthService()->isLoggedIn()) {
            return $this->smartRedirect();
        }

        $form = new SignInForm();

        if ($this->isPost() && $form->isDataValid($this->getPostParam())) {
            $givenUsername = $form->get('username')->getValue();
            $givenPassword = $form->get('password')->getValue();

            $accountService = $this->getAccountService();

            $user = $accountService->checkLogin($givenUsername, $givenPassword);

            if ($user !== false) {
                $this->getAuthService()->login($user);

                return $this->smartRedirect();
            }
            else {
                $form->addNotice('Invalid credentials.', 'Either the username/password combination is incorrect, or the account does not exist', 'danger');
            }
        }

        return new ViewModel([
            'form' => $form,
            'continue' => $this->getParam('url') ? true : false
        ]);
    }

    protected function smartRedirect () {
        $url = $this->getParam('url');

        if ($url) {
            $this->redirect()->toUrl($url);
        }
        else {
            return $this->redirectTo('index', [], 'index', 'portal');
        }
    }
}
