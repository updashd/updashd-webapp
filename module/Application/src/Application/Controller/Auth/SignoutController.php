<?php
namespace Application\Controller\Auth;

use Application\Service\ServiceInterface\DoctrineManagerInterface;
use Application\Service\ServiceTrait\DoctrineManagerTrait;
use Application\Service\ServiceTrait\SlugHelperServiceTrait;

class SignoutController extends AbstractAuthController implements DoctrineManagerInterface {
    use DoctrineManagerTrait;
    use SlugHelperServiceTrait;

    public function indexAction () {
        $this->getLayoutService()->setTitle('Sign Out');
    
        $this->layout('layout/auth');
    
        if ($this->getAuthService()->isLoggedIn()) {
            $this->getAuthService()->logout();
        }

        return $this->redirectTo('index', [], 'signin');
    }
}
