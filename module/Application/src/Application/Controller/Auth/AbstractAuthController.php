<?php
namespace Application\Controller\Auth;

use Application\Controller\AbstractController;
use Application\Service\ServiceInterface\AccountServiceInterface;
use Application\Service\ServiceInterface\DoctrineManagerInterface;
use Application\Service\ServiceInterface\MailServiceInterface;
use Application\Service\ServiceTrait\AccountServiceTrait;
use Application\Service\ServiceTrait\DoctrineManagerTrait;
use Application\Service\ServiceTrait\MailServiceTrait;
use Application\Service\ServiceTrait\SlugHelperServiceTrait;

abstract class AbstractAuthController extends AbstractController
    implements DoctrineManagerInterface, MailServiceInterface, AccountServiceInterface {
    use DoctrineManagerTrait;
    use SlugHelperServiceTrait;
    use MailServiceTrait;
    use AccountServiceTrait;

    public function init () {
        // Do not require login
    }
}