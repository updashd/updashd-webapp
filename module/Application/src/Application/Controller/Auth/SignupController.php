<?php
namespace Application\Controller\Auth;

use Application\Form\Auth\SignUp as SignUpForm;
use Application\Validator\Username;
use Laminas\View\Model\JsonModel;
use Laminas\View\Model\ViewModel;

class SignupController extends AbstractAuthController {
    public function init () {
        parent::init();

        $this->layout('layout/auth');
    }

    public function indexAction () {
        $this->getLayoutService()->setTitle('Sign Up');
        $accountService = $this->getAccountService();
        $form = new SignUpForm($accountService);

        if ($this->isPost() && $form->isDataValid($this->getPostParam())) {

            $accountService->addUser(
                $form->getElementValue('name'),
                $form->getElementValue('email'),
                $form->getElementValue('username'),
                $form->getElementValue('password')
            );

            return $this->redirectTo('check');
        }

        return new ViewModel([
            'form' => $form
        ]);
    }

    public function checkAction () {
        // Content page
    }

    public function verifyAction () {
        $this->getAuthService()->requireAuth();

        $person = $this->getAuthService()->getPerson();

        $jwt = $this->getParam('t');

        $verified = false;

        $reason = 'Unknown';

        try {
            $verified = $this->getAccountService()->processVerifyToken($person, $jwt);
        }
        catch (\Exception $e) {
            $reason = $e->getMessage();
        }

        if ($verified) {
            $this->redirectTo('index', [], 'index', 'portal');
        }

        return new ViewModel([
            'reason' => $reason
        ]);
    }

    public function checkusernameAction () {
        $username = $this->getParam('username');

        $valid = false;

        $usernameValidator = new Username();

        if ($usernameValidator->isValid($username)) {
            $accountService = $this->getAccountService();
            $valid = $accountService->isUsernameAvailable($username);
        }

        return new JsonModel([
            'available' => $valid
        ]);
    }
}
