<?php

namespace Application\Controller\Portal;

use Application\Controller\AbstractController;
use Application\Form\Portal\CreateAccount;
use Application\Service\ServiceInterface\AccountServiceInterface;
use Application\Service\ServiceInterface\DoctrineManagerInterface;
use Application\Service\ServiceTrait\AccountServiceTrait;
use Application\Service\ServiceTrait\DoctrineManagerTrait;
use Laminas\View\Model\JsonModel;
use Laminas\View\Model\ViewModel;

class IndexController extends AbstractController
    implements AccountServiceInterface, DoctrineManagerInterface {

    use AccountServiceTrait;
    use DoctrineManagerTrait;
    public function init () {
        parent::init();

        $this->getMenu()->setDefaultItemById('portal-frontend');
    }

    public function indexAction () {
        $accounts = $this->getAuthService()->getAccounts($this->getDoctrineManager());

        // If the user has no accounts yet, send them to the welcome process.
        if (! count($accounts)) {
            $this->redirectTo('welcome', [], 'index', 'portal');
        }
        else {
            // TODO: Send them to their default account.
            return $this->redirectTo('index', [], 'dashboard', 'account', [
                'account' => $accounts[key($accounts)]->getSlug()
            ]);
        }
    }

    public function welcomeAction () {
        $this->getLayoutService()->setTitle('Welcome');
        $this->getMenu()->setActiveId('portal-welcome');
    }

    public function createaccountAction () {
        $this->getLayoutService()->setTitle('Create Account');
        $this->getMenu()->setActiveId('portal-create-account');

        $accountService = $this->getAccountService();

        $form = new CreateAccount($accountService);

        if ($this->isPost() && $form->isDataValid($this->getPostParam())) {
            $name = $form->getElementValue('name');
            $slug = $form->getElementValue('slug');

            $accountService->addAccount($name, $slug, $this->getAuthService()->getPerson());

            return $this->redirectTo('index', [], 'dashboard', 'account', [
                'account' => $slug
            ]);
        }

        return new ViewModel([
            'form' => $form
        ]);
    }

    public function checkslugAction () {
        $accountService = $this->getAccountService();

        return new JsonModel([
            'available' => $accountService->isAccountSlugAvailable($this->getParam('slug'))
        ]);
    }

    public function joinaccountAction () {

    }
}