<?php

namespace Application\Event;

use Laminas\EventManager\EventManager;

class Manager extends EventManager {

    public static $em;

    public static function get () : EventManager {
        if (! self::$em) {
            self::$em = new self();
        }

        return self::$em;
    }
}