<?php
namespace Application\Pagination;

use Doctrine\ORM\Query;
use Doctrine\ORM\Tools\Pagination\Paginator as DoctrinePaginator;

class Paginator {
    private $numPerPage;
    private $paginator;
    private $page;
    private $params;

    /**
     * Create paginator from a query and parameters.
     * @param Query $query The Doctrine query
     * @param array $params The parameters from the request
     * @param bool $startOnLastPage
     * @param int $startingPage The default starting page
     * @param int $numPerPage The number of items to display per page
     * @param bool $fetchJoinCollection Set this to true if you are going to be doing any cartesian joins.
     */
    public function __construct (Query $query, $params, $startOnLastPage = false,
                                 $startingPage = 1, $numPerPage = 10, $fetchJoinCollection = false) {

        if ($params instanceof \Laminas\Stdlib\Parameters) {
            $params = $params->toArray();
        }

        $this->setParams($params);

        // Set Page and Number per Page
        $requestedPage = isset($params['page']) ? (int) $params['page'] : null;
        $numPerPage = isset($params['numPerPage']) ? (int) $params['numPerPage'] : $numPerPage;
        
        // Correct bad input
        if ($requestedPage !== null && $requestedPage >= 1) {
            $page = $requestedPage;
        }
        else {
            $page = $startingPage;
        }

        $this->setPage($page);

        if ($numPerPage < 1) {
            $numPerPage = 1;
        }
        
        // Set the number of items per page
        $this->setNumPerPage($numPerPage);
        
        // Set Query Offset and Max Results
        $query
            ->setMaxResults($this->getNumPerPage())
            ->setFirstResult($this->calcOffset());

        // Create the paginator
        $paginator = new DoctrinePaginator($query, $fetchJoinCollection);
        $this->setPaginator($paginator);
    
        // Max out the paginator at the number of pages, if needed
        $pageCount = $this->calcPageCount();

        // Start on the last page if no page number is set
        if ($requestedPage === null && $startOnLastPage) {
            $page = $pageCount;
            $this->setPage($page);
            $query->setFirstResult($this->calcOffset());
        }

        if ($page > $pageCount) {
            $this->setPage($pageCount);
            $query->setFirstResult($this->calcOffset());
        }
    }
    
    public function count () {
        return count($this->getPaginator());
    }
    
    public function calcOffset () {
        return $this->getNumPerPage() * ($this->getPage() - 1);
    }
    
    public function calcPageCount () {
        return ceil($this->count() / $this->getNumPerPage());
    }
    
    /**
     * @return int
     */
    public function getNumPerPage () {
        return $this->numPerPage;
    }
    
    /**
     * @param int $numPerPage
     */
    public function setNumPerPage ($numPerPage) {
        $this->numPerPage = $numPerPage;
    }
    
    /**
     * @return DoctrinePaginator
     */
    public function getPaginator () {
        return $this->paginator;
    }
    
    /**
     * @param DoctrinePaginator $paginator
     */
    public function setPaginator (DoctrinePaginator $paginator) {
        $this->paginator = $paginator;
    }
    
    /**
     * @return int
     */
    public function getPage () {
        return $this->page;
    }
    
    /**
     * @param int $page
     */
    public function setPage ($page) {
        $this->page = $page;
    }

    /**
     * @return array
     */
    public function getParams () {
        return $this->params;
    }

    /**
     * @param array $params
     */
    public function setParams ($params) {
        $this->params = $params;
    }
}