<?php
namespace Application\Service;

use Application\Mail\Message;
use Laminas\Mail\Address;
use Laminas\Mail\AddressList;
use Laminas\Mail\Transport\InMemory;
use Laminas\Mail\Transport\Sendmail;
use Laminas\Mail\Transport\Smtp;
use Laminas\Mail\Transport\SmtpOptions;
use Laminas\Mail\Transport\TransportInterface;

class MailService {
    protected $config;
    protected $transport;

    public function __construct ($config) {
        $this->setConfig($config);

        $transportType = $config['transport'];
        $transport = null;

        switch ($transportType) {
            case 'sendmail':
                $transport = new Sendmail();
                break;
            case 'smtp':
                $transportOptions = new SmtpOptions($config['transport_options']);
                $transport = new Smtp($transportOptions);
                break;
//            case 'file':
//                $transportOptions = new FileOptions([
//                    'path' => $config['transport_options']['path'],
//                    'callback' => function (File $transport) use ($config) {
//                        $transportConfig = $config['transport_options'];
//                        return
//                            $transportConfig['file_prefix'] .
//                            date($transportConfig['file_date_format']) .
//                            '_' . mt_rand() .
//                            $transportConfig['file_txt_suffix'];
//                    }
//                ]);
//                $transport = new File($transportOptions);
//                break;
            case 'memory':
                $transport = new InMemory();
                break;
            default:
                $transport = new InMemory();
                break;
        }

        $this->setTransport($transport);
    }

    /**
     * @return Message
     */
    public function createMessage () {
        $message = new Message();
        $message->setEncoding('UTF-8');

        return $message;
    }

    /**
     * @param Address|AddressList|string $list
     * @param bool $asString
     * @return array|string
     */
    protected function getAddresses ($list, $asString = true) {
        $emails = [];

        if ($list instanceof Address) {
            $emails[] = ($list->getName() ? '"' . $list->getName() . '" ' : '') . '<' . $list->getEmail() . '>';
        }
        elseif ($list instanceof AddressList) {
            foreach ($list as $item) {
                $emails[] = ($item->getName() ? '"' . $item->getName() . '" ' : '') . '<' . $item->getEmail() . '>';
            }
        }
        elseif (is_string($list)) {
            $emails[] = $list;
        }
        else {
            $emails[] = 'Unknown';
        }

        return $asString ? implode(',', $emails) : $emails;
    }

    /**
     * @param Message $message
     */
    public function send ($message) {
        $config = $this->getConfig();

        if ($config['logging']['level'] >= 1) {
            $properties = [
                'Date' => date('Y-m-d H:i:s O '),
                'Sender' => $this->getAddresses($message->getSender()),
                'From' => $this->getAddresses($message->getFrom()),
                'To' => $this->getAddresses($message->getTo()),
                'Cc' => $this->getAddresses($message->getCc()),
                'Bcc' => $this->getAddresses($message->getBcc()),
                'Subject' => $this->getAddresses($message->getSubject()),
                'Length' => strlen($message->getBodyText()),
            ];

            $lineOfDetail = '';

            array_walk($properties, function ($value, $key) use (&$lineOfDetail) {
                $lineOfDetail .= $key . ': ' . $value . '; ';
            });

            file_put_contents($config['logging']['general_log'], $lineOfDetail . PHP_EOL, FILE_APPEND);
        }


        if ($config['logging']['level'] >= 2) {
            $headers = $message->getHeaders()->toArray();

            $path = $config['logging']['path'];
            $fileName = date('Y_m_d_H_i_s_' . str_replace(' ', '_', microtime()));
            $viewHtml = $fileName . '_view.html';
            $srcHtml = $fileName . '_src.html';
            $plainText = $fileName . '_src.txt';

            $content = '';

            $content .= '<h1>Properties:</h1>';
            $content .= '<ul>';
            array_walk($properties, function ($value, $key) use (&$content) {
                $content .=  '<li><b>' . $key . '</b>: ' . nl2br(htmlspecialchars($value)) . '</li>';
            });
            $content .= '</ul>';

            $content .= '<h1>Headers:</h1>';
            $content .= '<ul>';
            array_walk($headers, function ($value, $key) use (&$content) {
                $content .=  '<li><b>' . $key . '</b>: ' . nl2br(htmlspecialchars($value)) . '</li>';
            });
            $content .= '</ul>';

            $content .= '<h1>HTML:</h1>';
            $content .= '<iframe src="./' . $srcHtml . '" style="height: 500px; width: 100%;" frameborder="0"></iframe>';

            $content .= '<h1>Plain Text:</h1>';
            $content .= '<iframe src="./' . $plainText . '" style="height: 500px; width: 100%;" frameborder="0"></iframe>';

            file_put_contents($path . $viewHtml, $content);
            file_put_contents($path . $srcHtml, $message->getHtml());
            file_put_contents($path . $plainText, $message->getPlain());
        }

        $transport = $this->getTransport();
        $transport->send($message);
    }

    /**
     * When using InMemory mail transport, you can get the last message sent.
     * @return Message
     * @throws \Exception
     */
    public function getLastMessage () {
        $transport = $this->getTransport();

        if (! $transport instanceof InMemory) {
            throw new \Exception('Must use InMemory transport to be able to get last message.');
        }

        return $transport->getLastMessage();
    }

    /**
     * @return TransportInterface
     */
    protected function getTransport () {
        return $this->transport;
    }

    /**
     * @param TransportInterface $transport
     */
    protected function setTransport ($transport) {
        $this->transport = $transport;
    }

    /**
     * @return mixed
     */
    protected function getConfig () {
        return $this->config;
    }

    /**
     * @param mixed $config
     */
    protected function setConfig ($config) {
        $this->config = $config;
    }
}