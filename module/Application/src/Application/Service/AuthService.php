<?php
namespace Application\Service;

use Updashd\Model\Account as AccountEntity;
use Updashd\Model\Person as PersonEntity;
use Laminas\Session\Container;

class AuthService {
    private $sessionContainer;
    
    public function __construct (Container $sessionContainer) {
        $this->sessionContainer = $sessionContainer;
    }
    
    public function requireAuth () {
        if (! self::isLoggedIn()) {
            // If this is a post request, give a denied page.
            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                header('403 Access Denied');
                exit(0);
            }

            $uri = $_SERVER['REQUEST_URI'];

            header('Location: /auth/signin?url=' . urlencode($uri));
            exit(0);
        }
    }

    /**
     * @param PersonEntity $person
     */
    public function login ($person) {
        $this->getSessionContainer()->person = $person;
    }

    /**
     * @param PersonEntity $person
     */
    public function proxy ($person) {
        $container = $this->getSessionContainer();

        // Store who they really are.
        if (! $container->realPerson) {
            $container->realPerson = self::getPersonId();
        }

        // Change to the new person
        $container->person = $person;
    }

    public function logout () {
        $container = $this->getSessionContainer();
        $container->realPerson = null;
        $container->person = null;
        $container->accounts = null;
    }

    public function getPersonId () {
        return $this->getPerson() ? $this->getPerson()->getPersonId() : null;
    }

    /**
     * @return PersonEntity|null
     */
    public function getPerson () {
        return $this->getSessionContainer()->person;
    }
    
    /**
     * @param DoctrineManager $doctrineManager
     * @return AccountEntity[]
     */
    public function getAccounts (DoctrineManager $doctrineManager) {
        $container = $this->getSessionContainer();

        if (! $container->accounts) {
            return $container->accounts = $this->retrieveAccounts($doctrineManager);
        }
        
        return $container->accounts;
    }
    
    /**
     * @param string $accountSlug
     * @param DoctrineManager $doctrineManager
     * @return AccountEntity|null Account entity if found, null otherwise
     */
    public function getAccount ($accountSlug, DoctrineManager $doctrineManager) {

        // Try using the session accounts first
        $accounts = $this->getAccounts($doctrineManager);

        // If it is defined, use the one on the session
        if (isset($accounts[$accountSlug])) {
            return $accounts[$accountSlug];
        }

        // Otherwise, we will need to retrieve it on our own
        $em = $doctrineManager->getEntityManager();

        $accountRepo = $em->getRepository(AccountEntity::class);

        /** @var AccountEntity $account */
        $account = $accountRepo->findOneBy(['slug' => $accountSlug]);

        return $account;
    }
    
    public function retrieveAccounts (DoctrineManager $doctrineManager) {
        $em = $doctrineManager->getEntityManager();
        $accountRepo = $em->getRepository(AccountEntity::class);
        /** @var AccountEntity[] $accounts */
        $accounts = $accountRepo->findBy(['owner' => $this->getPerson()]);
        $accountArray = [];
        
        foreach ($accounts as $account) {
            $accountArray[$account->getSlug()] = $account;
        }
        
        return $accountArray;
    }

    public function isLoggedIn () {
        return $this->getSessionContainer()->person ? true : false;
    }
    
    /**
     * @return Container
     */
    public function getSessionContainer () {
        return $this->sessionContainer;
    }
}