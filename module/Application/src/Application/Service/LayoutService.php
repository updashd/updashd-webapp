<?php
namespace Application\Service;

class LayoutService {
    protected $config;
    protected $title = '';
    protected $description = '';

    public function __construct ($config) {
        $this->setConfig($config);
    }

    /**
     * @return string
     */
    public function getTitle () {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle ($title) {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getDescription () {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription ($description) {
        $this->description = $description;
    }

    /**
     * @return array
     */
    public function getConfig () {
        return $this->config;
    }

    /**
     * @param array $config
     */
    protected function setConfig ($config) : void {
        $this->config = $config;
    }
}