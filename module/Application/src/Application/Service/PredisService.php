<?php
namespace Application\Service;

use Predis\Client as PredisClient;

class PredisService {
    private $client;
    private $config;

    public function __construct ($config) {
        $this->setConfig($config);
    }

    /**
     * @return PredisClient
     */
    public function getClient () {
        if (! $this->client) {
            $client = new PredisClient($this->getConfig());
            $this->setClient($client);
        }

        return $this->client;
    }

    /**
     * @param PredisClient $client
     */
    public function setClient (PredisClient $client) {
        $this->client = $client;
    }

    /**
     * @return mixed
     */
    public function getConfig () {
        return $this->config;
    }

    /**
     * @param mixed $config
     */
    public function setConfig ($config) {
        $this->config = $config;
    }


}