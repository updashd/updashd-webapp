<?php

namespace Application\Service;

use Doctrine\ORM\Decorator\EntityManagerDecorator;
use Doctrine\ORM\EntityManager;
use Updashd\Model\Person;
use Updashd\Model\Person as PersonModel;
use Updashd\Model\PersonSetting as PersonSettingModel;
use Updashd\Model\Setting as SettingModel;

class PersonSettingService {
    private $config;
    private $entityManager;
    private $settings = array();

    public function __construct ($config, DoctrineManager $doctrineManager) {
        $this->setConfig($config);
        $this->setEntityManager($doctrineManager->createEntityManager());
    }

    /**
     * @param PersonModel $person
     * @return PersonSettingModel[]
     */
    public function getPersonSettings (PersonModel $person) {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();
        $qb
            ->select('ps, s')
            ->from(PersonSettingModel::class, 'ps')
            ->innerJoin(SettingModel::class, 's')
            ->where($qb->expr()->eq('ps.person', ':person'));

        $query = $qb->getQuery();
        $query->setParameter('person', $person);

        return $query->getResult();
    }

    /**
     * @param Person $person
     * @param string $settingKey
     * @param bool $useDefault
     * @return mixed|null
     */
    public function getPersonSetting (PersonModel $person, $settingKey, $useDefault = true) {
        $key = $this->getCacheKey($person, $settingKey);

        $value = null;

        if (! array_key_exists($key, $this->settings)) {
            $em = $this->getEntityManager();

            $personSettingRepo = $em->getRepository(PersonSettingModel::class);
            $settingRepo = $em->getRepository(SettingModel::class);

            /** @var SettingModel $setting */
            $setting = $settingRepo->findOneBy(['settingKey' => $settingKey]);

            if ($setting) {
                /** @var PersonSettingModel $personSetting */
                $personSetting = $personSettingRepo->findOneBy(['setting' => $setting, 'person' => $person]);

                $value = null;

                if ($personSetting) {
                    $value = $personSetting->getSettingValue();
                }
                elseif ($useDefault) {
                    $value = $setting->getSettingDefault();
                }

                // Save the value
                $this->settings[$key] = $value;
            }
            else {
                return null;
            }
        }
        else {
            $value = $this->settings[$key];
        }

        return $value;
    }

    /**
     * @param PersonModel $person
     * @param string $settingKey
     * @param string $settingValue
     * @return bool
     */
    public function setPersonSetting (PersonModel $person, $settingKey, $settingValue) {
        $em = $this->getEntityManager();

        $personSettingRepo = $em->getRepository(PersonSettingModel::class);
        $settingRepo = $em->getRepository(SettingModel::class);

        /** @var SettingModel $setting */
        $setting = $settingRepo->findOneBy(['settingKey' => $settingKey]);

        if (! $setting) {
            return false;
        }

        /** @var PersonSettingModel $personSetting */
        $personSetting = $personSettingRepo->findOneBy(['setting' => $setting, 'person' => $person]);

        if (! $personSetting) {
            $personSetting = new PersonSettingModel();
            $personSetting->setPerson($em->getReference(PersonModel::class, $person->getPersonId()));
            $personSetting->setSetting($setting);
        }

        $personSetting->setSettingValue($settingValue);

        $key = $this->getCacheKey($person, $settingKey);

        $this->settings[$key] = $settingValue;

        $em->persist($personSetting);

        return true;
    }

    /**
     * @param PersonModel $person
     * @param string $settingKey
     */
    public function removePersonSetting (PersonModel $person, $settingKey) {
        $em = $this->getEntityManager();

        $personSettingRepo = $em->getRepository(PersonSettingModel::class);
        $settingRepo = $em->getRepository(SettingModel::class);

        /** @var SettingModel $setting */
        $setting = $settingRepo->findOneBy(['settingKey' => $settingKey]);

        /** @var PersonSettingModel $personSetting */
        $personSetting = $personSettingRepo->findOneBy(['setting' => $setting, 'person' => $person]);

        $key = $this->getCacheKey($person, $settingKey);

        unset($this->settings[$key]);

        $em->remove($personSetting);
    }

    public function save () {
        $em = $this->getEntityManager();
        $em->flush();
    }

    public function reset () {
        $em = $this->getEntityManager();
        $em->clear();

        $this->settings = array();
    }

    /**
     * @param PersonModel $person
     * @param string $settingKey
     * @return string
     */
    protected function getCacheKey ($person, $settingKey) {
        return $person->getPersonId() . '-' . $settingKey;
    }

    /**
     * @return mixed
     */
    protected function getConfig () {
        return $this->config;
    }

    /**
     * @param mixed $config
     */
    protected function setConfig ($config) {
        $this->config = $config;
    }

    /**
     * @return EntityManagerDecorator|EntityManager
     */
    protected function getEntityManager () {
        return $this->entityManager;
    }

    /**
     * @param EntityManagerDecorator|EntityManager $entityManager
     */
    protected function setEntityManager ($entityManager) {
        $this->entityManager = $entityManager;
    }
}