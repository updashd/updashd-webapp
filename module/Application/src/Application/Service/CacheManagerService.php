<?php
namespace Application\Service;

use Interop\Container\ContainerInterface;

class CacheManagerService {
    protected $container;
    protected $config;

    public function __construct ($config, ContainerInterface $container) {
        $this->setConfig($config);
        $this->setContainer($container);
    }

    /**
     * @param string $cache
     * @return \Laminas\Cache\Storage\Adapter\AbstractAdapter
     */
    public function getCache ($cache) {
        $container = $this->getContainer();

        return $container->get($cache);
    }

    /**
     * @return mixed
     */
    protected function getConfig () {
        return $this->config;
    }

    /**
     * @param mixed $config
     */
    protected function setConfig ($config) {
        $this->config = $config;
    }

    /**
     * @return ContainerInterface
     */
    protected function getContainer () {
        return $this->container;
    }

    /**
     * @param ContainerInterface $container
     */
    protected function setContainer ($container) {
        $this->container = $container;
    }
}