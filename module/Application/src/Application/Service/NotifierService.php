<?php
namespace Application\Service;

use Updashd\Configlib\Config;
use Updashd\Worker\Exception\WorkerRuntimeException;

class NotifierService {
    private $config;
    private $publicKey;
    private $serviceWorkers = [];

    public function __construct ($config) {
        $this->setConfig($config);

        // Set up the workers
        foreach ($config['notifiers'] as $notifierFQCN) {
            $serviceName = forward_static_call(array($notifierFQCN, 'getNotifierName'));
            $this->setNotifierClassName($serviceName, $notifierFQCN);
        }

        $key = file_get_contents($config['public_key_file']);

        if (! $key) {
            throw new \Exception('Private key is required! Please see config/config.default.php');
        }

        $this->setPublicKey($key);
    }

    /**
     * Set the workerClassName for a given serviceName
     * @param $serviceName
     * @param $workerClassName
     */
    private function setNotifierClassName ($serviceName, $workerClassName) {
        $this->serviceWorkers[$serviceName] = $workerClassName;
    }

    /**
     * Get the service workerClassName for a given serviceName
     * @param $serviceName
     * @return mixed
     * @throws WorkerRuntimeException
     */
    public function getNotifierClassName ($serviceName) {
        if (! array_key_exists($serviceName, $this->serviceWorkers)) {
            throw new WorkerRuntimeException('A worker was request for a service that is not provided.');
        }

        return $this->serviceWorkers[$serviceName];
    }

    /**
     * @param $serviceName
     * @return Config
     */
    public function getNotifierConfig ($serviceName) {
        $notifierClassName = $this->getNotifierClassName($serviceName);

        /** @var Config $config */
        $config = forward_static_call(array($notifierClassName, 'createConfig'));

        $config->setPublicEncryptKey($this->getPublicKey());

        return $config;
    }

    /**
     * @return mixed
     */
    public function getConfig () {
        return $this->config;
    }

    /**
     * @param mixed $config
     */
    public function setConfig ($config) {
        $this->config = $config;
    }

    /**
     * @return string
     */
    public function getPublicKey () {
        return $this->publicKey;
    }

    /**
     * @param string $publicKey
     */
    public function setPublicKey ($publicKey) {
        $this->publicKey = $publicKey;
    }
}