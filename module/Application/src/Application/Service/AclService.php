<?php
namespace Application\Service;

use Laminas\Permissions\Acl\Resource\GenericResource as Resource;
use Laminas\Permissions\Acl\Role\GenericRole as Role;

class AclService extends \Laminas\Permissions\Acl\Acl {
    private $actions;

    public function __construct ($aclConfig) {
        $this->setActions($aclConfig['actions']);

        foreach ($aclConfig['roles'] as $role) {
            $this->addRole(new Role($role));
        }

        foreach ($aclConfig['resources'] as $resource) {
            $this->addResource(new Resource($resource));
        }

        foreach ($aclConfig['mapping'] as $roleName => $roleResources) {
            foreach ($roleResources as $resourceName => $actions) {
                // _OWN Wildcard
                if (in_array('*_OWN', $actions)) {
                    foreach (preg_grep('/_OWN$/', $aclConfig['actions']) as $action) {
                        $this->allow($roleName, $resourceName, $action);
                    }
                }
                // All Wildcard
                else if (in_array('*', $actions)) {
                    foreach ($aclConfig['actions'] as $action) {
                        $this->allow($roleName, $resourceName, $action);
                    }
                }
                // Explicit
                else {
                    foreach ($actions as $action) {
                        $this->allow($roleName, $resourceName, $action);
                    }
                }
            }
        }
    }

    public function isAllowed ($role = null, $resource = null, $privilege = null, $isOwn = false) {
        // Check both RESOURCE and RESOURCE_OWN if they own the resource in question.
        if ($isOwn && ! preg_match('/_OWN$/', $privilege)) {
            return parent::isAllowed($role, $resource, $privilege) || parent::isAllowed($role, $resource, $privilege . '_OWN');
        }
        else {
            return parent::isAllowed($role, $resource, $privilege);
        }
    }

    /**
     * @return mixed
     */
    public function getActions () {
        return $this->actions;
    }

    /**
     * @param mixed $actions
     */
    public function setActions ($actions) {
        $this->actions = $actions;
    }
}