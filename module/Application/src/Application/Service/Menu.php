<?php
namespace Application\Service;

use Application\Menu\Item;

class Menu {
    private $rootItem;
    private $defaultItem;
    private $xml;
    private $activeId;
    private $items = [];

    public function __construct ($config) {
        $content = file_get_contents($config['location']);
        $this->xml = simplexml_load_string($content);

        $this->rootItem = $this->createItemFromElem($this->xml);
        $this->defaultItem = $this->rootItem;
    }

    private function createItemFromElem (\SimpleXMLElement $elem, Item $parent = null) {
        /** @var \SimpleXMLElement[] $attrs */
        $attrs = $elem->attributes();

        // Determine the type first so we know what else to do.
        $type = Item::TYPE_ITEM;
        if (isset($attrs['type'])) {
            switch ($attrs['type']) {
                case 'item': $type = Item::TYPE_ITEM; break;
                case 'divider': $type = Item::TYPE_DIVIDER; break;
            }
        }

        $id = $attrs['id']->__toString();
        $name = $attrs['name']->__toString();

        $item = new Item($id, $name);

        $item->setType($type);

        // URL method
        if (isset($attrs['url'])) {
            $item->setUrl($attrs['url']->__toString());
        }

        // Router method
        if (isset($attrs['route'])) {
            $item->setRoute($attrs['route']->__toString());
        }
        elseif ($parent && $parent->getRoute()) {
            $item->setRoute($parent->getRoute());
        }

        if (isset($attrs['controller'])) {
            $item->setController($attrs['controller']->__toString());
        }
        elseif ($parent && $parent->getController()) {
            $item->setController($parent->getController());
        }

        if (isset($attrs['action'])) {
            $item->setAction($attrs['action']->__toString());
        }
        elseif ($parent && $parent->getAction()) {
            $item->setAction($parent->getAction());
        }

        if (isset($attrs['icon'])) {
            $item->setIcon($attrs['icon']->__toString());
        }

        if (isset($attrs['hidden'])) {
            $item->setIsHidden($attrs['hidden']->__toString());
        }

        if (isset($attrs['collapse_when_not_active'])) {
            $item->setCollapseWhenNotActive($attrs['collapse_when_not_active']->__toString());
        }

        // Add the item to the ids
        $this->setItemById($id, $item);

        $item->setParent($parent);

        $children = null;

        if ($elem->count()) {
            $children = [];

            foreach ($elem->children() as $child) {
                $children[] = $this->createItemFromElem($child, $item);
            }
        }

        $item->setChildren($children);

        return $item;
    }

    /**
     * @param int $id the id of the menu item to retrieve.
     * @param Item $item the item to set
     */
    public function setItemById ($id, Item $item) {
        $this->items[$id] = $item;
    }

    /**
     * @return Item
     */
    public function getRootItem () {
        return $this->rootItem;
    }

    /**
     * @param Item $rootItem
     */
    public function setRootItem (Item $rootItem) {
        $this->rootItem = $rootItem;
    }

    /**
     * @return string
     */
    public function getActiveId () {
        return $this->activeId;
    }

    /**
     * @param string $activeId
     */
    public function setActiveId ($activeId) {
        $this->activeId = $activeId;
    }

    /**
     * @return Item[]|null
     */
    public function getItems () {
        return $this->items;
    }

    /**
     * @param array $items
     */
    public function setItems ($items) {
        $this->items = $items;
    }

    /**
     * Set the visibility of menu items.
     * @param array $items array(itemId => isVisible, ...)
     */
    public function setMenuPermissions (array $items) {
        foreach ($items as $itemId => $isVisible) {
            $item = $this->getItemById($itemId);
            if ($item && ! $isVisible) {
                $item->setIsHidden(true);
            }
        }
    }

    /**
     * @param string $id the id of the menu item to retrieve.
     * @return Item|null
     */
    public function getItemById ($id) {
        return isset($this->items[$id]) ? $this->items[$id] : null;
    }
    
    /**
     * @return Item
     */
    public function getDefaultItem () {
        return $this->defaultItem;
    }
    
    /**
     * @param Item $defaultItem
     */
    public function setDefaultItem ($defaultItem) {
        $this->defaultItem = $defaultItem;
    }
    
    /**
     * @param string $id
     */
    public function setDefaultItemById($id) {
        $this->setDefaultItem($this->getItemById($id));
    }
}