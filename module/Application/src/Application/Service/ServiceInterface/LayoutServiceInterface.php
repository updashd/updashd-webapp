<?php
namespace Application\Service\ServiceInterface;

use Application\Service\LayoutService;

interface LayoutServiceInterface {
    /**
     * @return LayoutService
     */
    public function getLayoutService ();
    
    /**
     * @param LayoutService $menu
     */
    public function setLayoutService (LayoutService $menu);
}