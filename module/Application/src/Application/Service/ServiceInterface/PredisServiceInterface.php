<?php
namespace Application\Service\ServiceInterface;

use Application\Service\PredisService;

interface PredisServiceInterface {
    /**
     * @return PredisService
     */
    public function getPredisService ();

    /**
     * @param PredisService $predisService
     */
    public function setPredisService (PredisService $predisService);
}