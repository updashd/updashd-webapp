<?php
namespace Application\Service\ServiceInterface;

use Application\Service\PersonSettingService;

interface PersonSettingServiceInterface {
    /**
     * @return PersonSettingService
     */
    public function getPersonSettingService ();
    
    /**
     * @param PersonSettingService $personSettingService
     */
    public function setPersonSettingService (PersonSettingService $personSettingService);
}