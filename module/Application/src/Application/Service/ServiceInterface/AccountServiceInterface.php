<?php
namespace Application\Service\ServiceInterface;

use Application\Service\AccountService;

interface AccountServiceInterface {

    /**
     * @return AccountService
     */
    public function getAccountService ();

    /**
     * @param AccountService $accountService
     */
    public function setAccountService (AccountService $accountService);
}