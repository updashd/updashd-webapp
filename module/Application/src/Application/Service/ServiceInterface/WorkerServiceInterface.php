<?php
namespace Application\Service\ServiceInterface;

use Application\Service\WorkerService;

interface WorkerServiceInterface {
    /**
     * @return WorkerService
     */
    public function getWorkerService ();

    /**
     * @param WorkerService $workerService
     * @return
     * @internal param WorkerService $workerConfigService
     */
    public function setWorkerService (WorkerService $workerService);
}