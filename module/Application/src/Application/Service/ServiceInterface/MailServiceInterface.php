<?php
namespace Application\Service\ServiceInterface;

use Application\Service\MailService;

interface MailServiceInterface {
    /**
     * @return MailService
     */
    public function getMailService ();

    /**
     * @param MailService $mailService
     */
    public function setMailService (MailService $mailService);
}