<?php
namespace Application\Service\ServiceInterface;

use Application\Service\AclService;

interface AclServiceInterface {
    /**
     * @return AclService
     */
    public function getAclService ();

    /**
     * @param AclService $aclService
     */
    public function setAclService (AclService $aclService);
}