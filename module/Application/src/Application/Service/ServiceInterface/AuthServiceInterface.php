<?php
namespace Application\Service\ServiceInterface;

use Application\Service\AuthService;

interface AuthServiceInterface {

    /**
     * @return AuthService
     */
    public function getAuthService ();

    /**
     * @param AuthService $authService
     */
    public function setAuthService (AuthService $authService);
}