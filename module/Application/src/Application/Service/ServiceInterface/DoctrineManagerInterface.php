<?php
namespace Application\Service\ServiceInterface;

use Application\Service\DoctrineManager;

interface DoctrineManagerInterface {
    /**
     * @return DoctrineManager
     */
    public function getDoctrineManager ();

    /**
     * @param DoctrineManager $doctrineManager
     */
    public function setDoctrineManager (DoctrineManager $doctrineManager);
}