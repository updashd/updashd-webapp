<?php
namespace Application\Service\ServiceInterface;

use Application\Service\Menu;

interface MenuInterface {
    /**
     * @return Menu
     */
    public function getMenu ();
    
    /**
     * @param Menu $menu
     */
    public function setMenu (Menu $menu);
}