<?php
namespace Application\Service\ServiceInterface;

use Application\Service\NotifierService;

interface NotifierServiceInterface {
    /**
     * @return NotifierService
     */
    public function getNotifierService ();

    /**
     * @param NotifierService $notifierService
     */
    public function setNotifierService (NotifierService $notifierService);
}