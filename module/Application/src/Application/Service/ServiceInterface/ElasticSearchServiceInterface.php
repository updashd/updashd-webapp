<?php
namespace Application\Service\ServiceInterface;

use Application\Service\ElasticSearchService;

interface ElasticSearchServiceInterface {
    /**
     * @return ElasticSearchService
     */
    public function getElasticSearchService ();

    /**
     * @param ElasticSearchService $elasticSearchService
     */
    public function setElasticSearchService (ElasticSearchService $elasticSearchService);
}