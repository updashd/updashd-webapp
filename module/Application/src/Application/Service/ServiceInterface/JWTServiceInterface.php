<?php
namespace Application\Service\ServiceInterface;

use Application\Service\JWTService;

interface JWTServiceInterface {
    /**
     * @return JWTService
     */
    public function getJwtService ();

    /**
     * @param JWTService $jwtService
     */
    public function setJwtService (JWTService $jwtService);
}