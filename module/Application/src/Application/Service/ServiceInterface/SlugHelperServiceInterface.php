<?php
namespace Application\Service\ServiceInterface;

use Application\Service\SlugHelperService;

interface SlugHelperServiceInterface {
    /**
     * @return SlugHelperService
     */
    public function getSlugHelperService ();

    /**
     * @param SlugHelperService $slugHelperService
     */
    public function setSlugHelperService (SlugHelperService $slugHelperService);
}