<?php
namespace Application\Service\ServiceInterface;

use Application\Service\CacheManagerService;

interface CacheManagerInterface {
    /**
     * @return CacheManagerService
     */
    public function getCacheManager();

    /**
     * @param CacheManagerService $cacheManager
     */
    public function setCacheManager(CacheManagerService $cacheManager);
}