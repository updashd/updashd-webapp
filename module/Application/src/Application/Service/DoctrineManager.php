<?php
namespace Application\Service;

use Application\Doctrine\AuditedEntityManager;
use Application\Doctrine\Logger;
use Doctrine\Common\Cache\ApcCache;
use Doctrine\Common\Cache\ApcuCache;
use Doctrine\Common\Cache\ArrayCache;
use Doctrine\Common\Cache\FilesystemCache;
use Doctrine\Common\Cache\MemcacheCache;
use Doctrine\Common\Cache\MemcachedCache;
use Doctrine\Common\Cache\PredisCache;
use Doctrine\Common\Cache\RedisCache;
use Doctrine\Common\Cache\XcacheCache;
use Doctrine\DBAL\Logging\SQLLogger;
use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\Configuration as ORMConfiguration;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;
use Predis\Client as PredisClient;
use Updashd\Doctrine\UTCDateTimeType;

class DoctrineManager {
    private $entityManager;
    private $authService;
    private $logger;

    private $config;

    public function __construct ($config, AuthService $authService) {
        $this->setConfig($config);
        $this->setAuthService($authService);
    }

    public function init () {
        Type::overrideType('datetime', UTCDateTimeType::class);
        Type::overrideType('datetimetz', UTCDateTimeType::class);
    
        $this->setEntityManager($this->createEntityManager());
    }

    protected function createSetup () {
        $config = $this->getConfig();

        return Setup::createAnnotationMetadataConfiguration(
            $config['options']['entity_paths'],
            $config['options']['dev_mode'],
            $config['options']['proxy_dir'],
            null,
            $config['options']['simple_annotation']
        );
    }

    public function createEntityManager () {
        $config = $this->getConfig();

        $setup = $this->createSetup();

        $entityManager = EntityManager::create($config['connection'], $setup);

        // Query logging (when enabled)
        if ($config['log_queries']) {
            $this->configureLogging($entityManager, $setup);
        }

        // Caching (when enabled)
        if ($config['caching']['enable']) {
            $this->configureCaching($config['caching'], $setup);
        }

        return new AuditedEntityManager($entityManager, $this->getAuthService());
    }
    
    public function configureLogging (EntityManager $entityManager, ORMConfiguration & $config) {
        $logger = new Logger(
            $entityManager->getConnection()->getDatabasePlatform(),
            [
                Logger::QUERY_TYPE_SELECT,
                Logger::QUERY_TYPE_INSERT,
                Logger::QUERY_TYPE_UPDATE,
                Logger::QUERY_TYPE_DELETE,
            ]
        );

        $config->setSQLLogger($logger);

        $this->setLogger($logger);
    }
    
    public function configureCaching ($config, ORMConfiguration & $setup) {
        $engine = $config['engine'];
        $engineConfig = $config['engine_configuration'][$engine];
        
        if ($engine == 'array') {
            $cacheDriver = new ArrayCache();
        }
        elseif ($engine == 'apc') {
            $cacheDriver = new ApcCache();
        }
        elseif ($engine == 'apcu') {
            $cacheDriver = new ApcuCache();
        }
        elseif ($engine == 'memcache') {
            $memcache = new \Memcache();
            $memcache->connect($engineConfig['host'], $engineConfig['port'], $engineConfig['timeout']);
            $cacheDriver = new MemcacheCache();
            $cacheDriver->setMemcache($memcache);
        }
        elseif ($engine == 'memcached') {
            $memcached = new \Memcached();
            
            foreach ($engineConfig['servers'] as $server) {
                $memcached->addServer($server['host'], $server['port'], $server['weight']);
            }
            
            $cacheDriver = new MemcachedCache();
            $cacheDriver->setMemcached($memcached);
        }
        elseif ($engine == 'redis') {
            $redis = new \Redis();
            $redis->connect($engineConfig['host'], $engineConfig['port'], $engineConfig['timeout']);
            $cacheDriver = new RedisCache();
            $cacheDriver->setRedis($redis);
        }
        elseif ($engine == 'predis') {
            $client = new PredisClient($engineConfig);
            $cacheDriver = new PredisCache($client);
        }
        elseif ($engine == 'xcache') {
            $cacheDriver = new XcacheCache();
        }
        elseif ($engine == 'filesystem') {
            $cacheDriver = new FilesystemCache(
                $engineConfig['directory'],
                $engineConfig['extension'],
                $engineConfig['umask']
            );
        }
        else $cacheDriver = new ArrayCache();
        
        // Result Cache
        if ($config['result_cache']) {
            $setup->setResultCacheImpl($cacheDriver);
        }
        
        // Hydration Cache
        if ($config['hydration_cache']) {
            $setup->setHydrationCacheImpl($cacheDriver);
        }
        
        // Metadata Cache
        if ($config['metadata_cache']) {
            $setup->setMetadataCacheImpl($cacheDriver);
        }
    
        // Query Cache
        if ($config['query_cache']) {
            $setup->setQueryCacheImpl($cacheDriver);
        }
    }

    /**
     * @return AuditedEntityManager
     */
    public function getEntityManager () {
        if (! $this->entityManager) {
            $this->init();
        }

        return $this->entityManager;
    }

    /**
     * @param AuditedEntityManager $entityManager
     */
    public function setEntityManager ($entityManager) {
        $this->entityManager = $entityManager;
    }

    /**
     * @return mixed
     */
    public function getConfig () {
        return $this->config;
    }

    /**
     * @param mixed $config
     */
    public function setConfig ($config) {
        $this->config = $config;
    }

    /**
     * @return AuthService
     */
    public function getAuthService () {
        return $this->authService;
    }

    /**
     * @param AuthService $authService
     */
    public function setAuthService (AuthService $authService) {
        $this->authService = $authService;
    }
    
    /**
     * @return SQLLogger
     */
    public function getLogger () {
        return $this->logger;
    }
    
    /**
     * @param SQLLogger $logger
     */
    public function setLogger (SQLLogger $logger) {
        $this->logger = $logger;
    }
}