<?php
namespace Application\Service;

use Firebase\JWT\JWT;

class JWTService {
    protected $config;

    const EMAIL_KEY = 'email_key';
    const PUSHER_KEY = 'pusher_key';

    public function encode ($payload, $keyType = self::EMAIL_KEY) {
        return JWT::encode($payload, $this->getKey($keyType), $this->getAlgorithm());
    }

    public function decode ($jwt, $keyType = self::EMAIL_KEY) {
        return JWT::decode($jwt, $this->getKey($keyType), [$this->getAlgorithm()]);
    }

    public function __construct ($config) {
        $this->setConfig($config);
    }

    protected function getAlgorithm () {
        $config = $this->getConfig();
        return $config['algorithm'];
    }

    protected function getKey ($keyType) {
        $config = $this->getConfig();

        return $config[$keyType];
    }

    /**
     * @return mixed
     */
    protected function getConfig () {
        return $this->config;
    }

    /**
     * @param mixed $config
     */
    protected function setConfig ($config) {
        $this->config = $config;
    }
}