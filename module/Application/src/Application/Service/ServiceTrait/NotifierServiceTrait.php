<?php
namespace Application\Service\ServiceTrait;

use Application\Service\NotifierService;

trait NotifierServiceTrait {
    private $notifierService;

    /**
     * @return NotifierService
     */
    public function getNotifierService () {
        return $this->notifierService;
    }

    /**
     * @param NotifierService $notifierService
     */
    public function setNotifierService (NotifierService $notifierService) {
        $this->notifierService = $notifierService;
    }
}