<?php
namespace Application\Service\ServiceTrait;

use Application\Service\PredisService;

trait PredisServiceTrait {
    private $predisService;
    
    /**
     * @return PredisService
     */
    public function getPredisService () {
        return $this->predisService;
    }
    
    /**
     * @param PredisService $predisService
     */
    public function setPredisService (PredisService $predisService) {
        $this->predisService = $predisService;
    }
}