<?php
namespace Application\Service\ServiceTrait;

use Application\Service\LayoutService;

trait LayoutServiceTrait {
    private $layoutService;
    
    /**
     * @return LayoutService
     */
    public function getLayoutService () {
        return $this->layoutService;
    }
    
    /**
     * @param LayoutService $layoutService
     */
    public function setLayoutService (LayoutService $layoutService) {
        $this->layoutService = $layoutService;
    }
}