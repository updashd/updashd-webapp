<?php
namespace Application\Service\ServiceTrait;

use Application\Service\AuthService;

trait AuthServiceTrait {
    private $authService;
    
    /**
     * @return AuthService
     */
    public function getAuthService () {
        return $this->authService;
    }
    
    /**
     * @param AuthService $authService
     */
    public function setAuthService (AuthService $authService) {
        $this->authService = $authService;
    }
}