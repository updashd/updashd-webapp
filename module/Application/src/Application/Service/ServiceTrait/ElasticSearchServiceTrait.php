<?php

namespace Application\Service\ServiceTrait;

use Application\Service\ElasticSearchService;

trait ElasticSearchServiceTrait {

    private $elasticSearchService;

    /**
     * @return ElasticSearchService
     */
    public function getElasticSearchService () {
        return $this->elasticSearchService;
    }

    /**
     * @param ElasticSearchService $elasticSearchService
     */
    public function setElasticSearchService (ElasticSearchService $elasticSearchService) {
        $this->elasticSearchService = $elasticSearchService;
    }
}