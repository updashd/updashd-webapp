<?php
namespace Application\Service\ServiceTrait;

use Application\Service\CacheManagerService;

trait CacheManagerTrait {
    protected $cacheManager;

    /**
     * @return CacheManagerService
     */
    public function getCacheManager() {
        return $this->cacheManager;
    }

    /**
     * @param CacheManagerService $cacheManager
     */
    public function setCacheManager(CacheManagerService $cacheManager) {
        $this->cacheManager = $cacheManager;
    }
}