<?php
namespace Application\Service\ServiceTrait;

use Application\Service\AccountService;

trait AccountServiceTrait {
    private $accountService;
    
    /**
     * @return AccountService
     */
    public function getAccountService () {
        return $this->accountService;
    }
    
    /**
     * @param AccountService $accountService
     */
    public function setAccountService (AccountService $accountService) {
        $this->accountService = $accountService;
    }
}