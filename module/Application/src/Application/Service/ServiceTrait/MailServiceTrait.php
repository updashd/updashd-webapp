<?php
namespace Application\Service\ServiceTrait;

use Application\Service\MailService;

trait MailServiceTrait {
    private $mailService;
    
    /**
     * @return MailService
     */
    public function getMailService () {
        return $this->mailService;
    }
    
    /**
     * @param MailService $mailService
     */
    public function setMailService (MailService $mailService) {
        $this->mailService = $mailService;
    }
}