<?php
namespace Application\Service\ServiceTrait;

use Application\Service\AclService;

trait AclServiceTrait {
    private $aclService;
    
    /**
     * @return AclService
     */
    public function getAclService () {
        return $this->aclService;
    }
    
    /**
     * @param AclService $aclService
     */
    public function setAclService (AclService $aclService) {
        $this->aclService = $aclService;
    }
}