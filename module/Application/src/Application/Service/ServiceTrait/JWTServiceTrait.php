<?php
namespace Application\Service\ServiceTrait;

use Application\Service\JWTService;

trait JWTServiceTrait {
    private $jwtService;
    
    /**
     * @return JWTService
     */
    public function getJwtService () {
        return $this->jwtService;
    }
    
    /**
     * @param JWTService $jwtService
     */
    public function setJwtService (JWTService $jwtService) {
        $this->jwtService = $jwtService;
    }
}