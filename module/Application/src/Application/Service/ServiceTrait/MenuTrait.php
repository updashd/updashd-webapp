<?php
namespace Application\Service\ServiceTrait;

use Application\Service\Menu;

trait MenuTrait {
    private $menu;
    
    /**
     * @return Menu
     */
    public function getMenu () {
        return $this->menu;
    }
    
    /**
     * @param Menu $menu
     */
    public function setMenu (Menu $menu) {
        $this->menu = $menu;
    }
}