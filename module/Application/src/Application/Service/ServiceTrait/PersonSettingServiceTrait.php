<?php
namespace Application\Service\ServiceTrait;

use Application\Service\PersonSettingService;

trait PersonSettingServiceTrait {
    protected  $personSettingService;

    /**
     * @return PersonSettingService
     */
    public function getPersonSettingService () {
        return $this->personSettingService;
    }

    /**
     * @param PersonSettingService $personSettingService
     */
    public function setPersonSettingService (PersonSettingService $personSettingService) {
        $this->personSettingService = $personSettingService;
    }
}