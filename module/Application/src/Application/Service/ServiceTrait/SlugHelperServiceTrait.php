<?php
namespace Application\Service\ServiceTrait;

use Application\Service\SlugHelperService;

trait SlugHelperServiceTrait {
    private $slugHelperService;
    
    /**
     * @return SlugHelperService
     */
    public function getSlugHelperService () {
        return $this->slugHelperService;
    }
    
    /**
     * @param SlugHelperService $slugHelperService
     */
    public function setSlugHelperService (SlugHelperService $slugHelperService) {
        $this->slugHelperService = $slugHelperService;
    }
}