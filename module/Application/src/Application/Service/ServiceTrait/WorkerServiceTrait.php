<?php
namespace Application\Service\ServiceTrait;

use Application\Service\WorkerService;

trait WorkerServiceTrait {
    private $workerService;

    /**
     * @return WorkerService
     */
    public function getWorkerService () {
        return $this->workerService;
    }

    /**
     * @param WorkerService $workerService
     */
    public function setWorkerService (WorkerService $workerService) {
        $this->workerService = $workerService;
    }
}