<?php
namespace Application\Service\ServiceTrait;

use Application\Service\DoctrineManager;

trait DoctrineManagerTrait {
    private $doctrineManager;
    
    /**
     * @return DoctrineManager
     */
    public function getDoctrineManager () {
        return $this->doctrineManager;
    }
    
    /**
     * @param DoctrineManager $doctrineManager
     */
    public function setDoctrineManager (DoctrineManager $doctrineManager) {
        $this->doctrineManager = $doctrineManager;
    }

    /**
     * @return \Application\Doctrine\AuditedEntityManager
     */
    public function getEntityManager () {
        return $this->getDoctrineManager()->getEntityManager();
    }
}