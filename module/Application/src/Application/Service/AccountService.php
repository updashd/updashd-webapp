<?php
namespace Application\Service;

use Application\Mail\Template;
use Updashd\Model\Account;
use Updashd\Model\AccountPerson;
use Updashd\Model\Person;
use Laminas\Router\RouteStackInterface;

class AccountService {
    const TOKEN_VALID_TIME = 3600;

    protected $config;
    protected $doctrineManager;
    protected $mailService;
    protected $JWTService;
    protected $router;

    public function __construct ($config, DoctrineManager $doctrineManager,
                                 MailService $mailService, JWTService $jwtService,
                                 RouteStackInterface $router) {
        $this->setConfig($config);
        $this->setDoctrineManager($doctrineManager);
        $this->setMailService($mailService);
        $this->setJWTService($jwtService);
        $this->setRouter($router);
    }

    /**
     * Check if the given Username/Email and password combination is correct.
     *
     * @param string $userName
     * @param string $password
     * @return Person|bool Person if successful, false otherwise.
     */
    public function checkLogin ($userName, $password) {
        $user = $this->getUserByIdentifier($userName);

        if ($this->checkPassword($user, $password)) {
            $user->setLastLogin(new \DateTime());

            $em = $this->getEntityManager();
            $em->persist($user);
            $em->flush();

            return $user;
        }
        else {
            return false;
        }
    }

    /**
     * Get a user (person) by their username or email.
     * @param string $identifier
     * @return null|Person
     */
    public function getUserByIdentifier ($identifier) {
        $em = $this->getEntityManager();
        $repo = $em->getRepository(Person::class);

        /** @var Person|null $user */
        $user = null;

        // Search by email
        if (strpos($identifier, '@') !== false) {
            $user = $repo->findOneBy([
                'email' => $identifier,
                'isBlocked' => 0
            ]);
        }
        // Search by username
        else {
            $user = $repo->findOneBy([
                'username' => $identifier,
                'isBlocked' => 0
            ]);
        }

        return $user;
    }

    /**
     * This will check if the password is correct for a certain user.
     * It will also rehash the password when necessary (this must be saved later)
     *
     * @param Person $person
     * @param string $givenPassword
     * @return bool true if password was correct, false otherwise.
     */
    public function checkPassword ($person, $givenPassword) {
        $success = false;

        if ($person) {
            $person = $this->getPerson($person);

            $hash = $person->getPasswordHash();

            $success = password_verify($givenPassword, $hash);

            if ($success && password_needs_rehash($hash, PASSWORD_DEFAULT)) {
                $newHash = password_hash($givenPassword, PASSWORD_DEFAULT);

                $person->setPasswordHash($newHash);

                $em = $this->getEntityManager();
                $em->persist($person);
                $em->flush();
            }
        }

        return $success;
    }

    /**
     * Change user's password.
     *
     * @param Person $person
     * @param string $password
     */
    public function setPassword ($person, $password) {
        $person = $this->getPerson($person);

        $newHash = password_hash($password, PASSWORD_DEFAULT);
        $person->setPasswordHash($newHash);

        $em = $this->getEntityManager();
        $em->persist($person);
        $em->flush();
    }

    /**
     * Change a user's name.
     *
     * @param Person $person
     * @param string $name
     */
    public function setName ($person, $name) {
        $person = $this->getPerson($person);

        $person->setName($name);

        $em = $this->getEntityManager();
        $em->persist($person);
        $em->flush();
    }

    /**
     * Change a user's email.
     *
     * @param Person $person
     * @param string $email
     */
    public function setEmail ($person, $email) {
        $person = $this->getPerson($person);

        $person->setEmail($email);
        $person->setEmailVerified(false);

        $em = $this->getEntityManager();
        $em->persist($person);
        $em->flush();
    }

    /**
     * Check to see if the given username/account slug is available.
     *
     * @param string $username
     * @return bool true if available, false otherwise.
     */
    public function isUsernameAvailable ($username) {
        $em = $this->getEntityManager();
        $repo = $em->getRepository(Person::class);

        /** @var Person|null $user */
        $user = $repo->findOneBy([
            'username' => $username
        ]);

        return $user === null;
    }

    /**
     * Check to see if the given email is already used.
     *
     * @param string $email
     * @return bool true if available, false otherwise.
     */
    public function isEmailAvailable ($email) {
        $em = $this->getEntityManager();
        $repo = $em->getRepository(Person::class);

        /** @var Person|null $user */
        $user = $repo->findOneBy([
            'email' => $email
        ]);

        return $user === null;
    }

    /**
     * Check to see if the given username/account slug is available.
     *
     * @param string $slug
     * @return bool true if available, false otherwise.
     */
    public function isAccountSlugAvailable ($slug) {
        $em = $this->getEntityManager();
        $repo = $em->getRepository(Account::class);

        /** @var Account|null $user */
        $account = $repo->findOneBy([
            'slug' => $slug
        ]);

        return $account === null;
    }

    /**
     * Add a new user. This will create the person record, account record, and their associations.
     * It will also send the email address verification message.
     * @param string $name
     * @param string $email
     * @param string $username
     * @param string $password
     * @return Person|bool the person created by the service or false if something went wrong.
     */
    public function addUser ($name, $email, $username, $password) {
        $em = $this->getEntityManager();

        // Create the user account
        $user = new Person();
        $user->setName($name);
        $user->setEmail($email);
        $user->setUsername($username);
        $this->setPassword($user, $password);

        $user->setEmailVerified(false);

        $user->setIsBlocked(false);

        $em->persist($user);
        $em->flush();

        $this->sendWelcomeEmail($user);
        $this->sendVerificationEmail($user);

        return true;
    }

    /**
     * @param string $name
     * @param string $slug
     * @param Person $owner
     */
    public function addAccount ($name, $slug, $owner) {
        $em = $this->getEntityManager();

        // Create an account for the user
        $account = new Account();
        $account->setName($name);
        $account->setSlug($slug);
        $account->setOwner($this->getPerson($owner));

        $em->persist($account);
        $em->flush();
    }

    /**
     * @param Account $account
     */
    public function removeAccount ($account) {
        $em = $this->getEntityManager();
        $em->remove($account);
        $em->persist($account);
        $em->flush();
    }

    /**
     * @param Account $account
     * @param Person $user
     * @param bool|string $roleType One of: OWNER, ADMIN, MEMBER, or VIEWER. An empty string, null, or false will remove an association.
     * @return bool
     */
    public function setAccountUserRoleType ($account, $user, $roleType = false) {
        $em = $this->getEntityManager();

        $apRepo = $em->getRepository(AccountPerson::class);

        /** @var null|AccountPerson $record */
        $record = $apRepo->findOneBy([
            'account' => $account,
            'person' => $user
        ]);

        // If they are giving them a role, create or modify a record
        if ($roleType) {
            if (! $record) {
                $record = new AccountPerson();
                $record->setAccount($account);
                $record->setPerson($user);
            }

            $record->setRoleType($roleType);
        }
        // If they are removing a role, delete it if we have a record
        elseif ($record) {
            $em->remove($record);
        }

        if ($record) {
            $em->persist($record);
        }

        $em->flush();

        return true;
    }

    /**
     * Remove a user and all of their stuff.
     *
     * @param Person $person
     */
    public function removeUser ($person) {

    }

    /**
     * Mark a user's email as verified.
     *
     * @param Person $person
     * @param bool $isVerified
     * @return bool
     */
    public function setUserEmailVerified ($person, $isVerified = true) {
        $person = $this->getPerson($person);

        $person->setEmailVerified($isVerified);

        $em = $this->getEntityManager();
        $em->persist($person);
        $em->flush();

        return true;
    }

    /**
     * Block a user.
     *
     * @param Person $person
     * @param bool $isBlocked
     */
    public function setUserBlocked ($person, $isBlocked = true) {
        $person = $this->getPerson($person);

        $person->setIsBlocked($isBlocked);

        $em = $this->getEntityManager();
        $em->persist($person);
        $em->flush();
    }

    /**
     * Send the welcome email to the given user.
     * @param Person $toPerson
     */
    public function sendWelcomeEmail ($toPerson) {
        $emailService = $this->getMailService();
        $toAddress = $toPerson->getEmail();

        // Create a new mail template
        $template = new Template();
        $template->setLayout('layout/transaction.phtml');
        $template->setLayoutParameters([
            'title' => 'Welcome'
        ]);

        // Set the email template file
        $template->setTemplate('template/welcome.phtml');
        $template->setTemplateParameters([
            'name' => $toPerson->getName()
        ]);

        $message = $emailService->createMessage();
        $message->setFrom('accounts@updashd.com', "Updashd Accounts");
        $message->setTo($toAddress);
        $message->setSubject('Welcome to Updashd');
        $message->setContentBody($template->render());

        $emailService->send($message);
    }

    /**
     * Send the verification email to the given user.
     * @param Person $toPerson
     */
    public function sendVerificationEmail ($toPerson) {
        $emailService = $this->getMailService();
        $toAddress = $toPerson->getEmail();

        // Create a new mail template
        $template = new Template();
        $template->setLayout('layout/transaction.phtml');
        $template->setLayoutParameters([
            'title' => 'Verify Your Email Address'
        ]);

        // Set the email template file
        $template->setTemplate('template/verifyemail.phtml');
        $template->setTemplateParameters([
            'name' => $toPerson->getName(),
            'email' => $toAddress,
            'verifyUrl' => $this->getVerifyUrl($toAddress),
        ]);

        $message = $emailService->createMessage();
        $message->setFrom('accounts@updashd.com', "Updashd Accounts");
        $message->setTo($toAddress);
        $message->setSubject('Verify Your Email Address');
        $message->setContentBody($template->render());

        $emailService->send($message);
    }

    protected function getVerifyUrl ($email) {
        $token = [
            'exp' => time() + self::TOKEN_VALID_TIME,
            'email' => $email
        ];

        $jwt = $this->getJWTService()->encode($token);

        $config = $this->getConfig();
        $baseUrl = $config['application']['url'];

        // Remove a trailing / if present
        if ($baseUrl[strlen($baseUrl) - 1] == '/') {
            $baseUrl = substr($baseUrl, 0, -1);
        }

        $baseUrl .= $this->getRouter()->assemble([
            'controller' => 'signup',
            'action' => 'verify'
        ], ['name' => 'auth']);

        $baseUrl .= '?' . http_build_query(['t' => $jwt]);

        return $baseUrl;
    }

    /**
     * @param Person $person
     * @param string $jwt
     * @return bool
     */
    public function processVerifyToken ($person, $jwt) {
        $data = $this->getJWTService()->decode($jwt);

        if ($data->email == $person->getEmail()) {
            $this->setUserEmailVerified($person);
            return true;
        }

        return false;
    }

    /**
     * @param Person $toPerson
     */
    public function sendResetPasswordEmail ($toPerson) {
        $emailService = $this->getMailService();
        $toAddress = $toPerson->getEmail();

        // Create a new mail template
        $template = new Template();
        $template->setLayout('layout/transaction.phtml');
        $template->setLayoutParameters([
            'title' => 'Reset Password'
        ]);

        // Set the email template file
        $template->setTemplate('template/resetpassword.phtml');
        $template->setTemplateParameters([
            'name' => $toPerson->getName(),
            'email' => $toAddress,
            'resetUrl' => $this->getResetUrl($toPerson->getPersonId()),
        ]);

        $message = $emailService->createMessage();
        $message->setFrom('accounts@updashd.com', "Updashd Accounts");
        $message->setTo($toAddress);
        $message->setSubject('Reset Password');
        $message->setContentBody($template->render());

        $emailService->send($message);
    }

    protected function getResetUrl ($personId) {
        $token = [
            'exp' => time() + self::TOKEN_VALID_TIME,
            'pid' => $personId
        ];

        $jwt = $this->getJWTService()->encode($token);

        $config = $this->getConfig();
        $baseUrl = $config['application']['url'];

        // Remove a trailing / if present
        if ($baseUrl[strlen($baseUrl) - 1] == '/') {
            $baseUrl = substr($baseUrl, 0, -1);
        }

        $baseUrl .= $this->getRouter()->assemble([
            'controller' => 'forgot',
            'action' => 'reset'
        ], ['name' => 'auth']);

        $baseUrl .= '?' . http_build_query(['t' => $jwt]);

        return $baseUrl;
    }

    /**
     * @param string $jwt
     * @return null|int The Person ID given by the token if valid.
     */
    public function processResetToken ($jwt) {
        $data = $this->getJWTService()->decode($jwt);

        return $data->pid;
    }

    /**
     * @param Person|int $personOrId detached person entity.
     * @return null|Person|object
     */
    public function getPerson ($personOrId) {
        $em = $this->getEntityManager();
        $person = $personOrId;

        if (is_int($personOrId)) {
            $personRepo = $em->getRepository(Person::class);
            $person = $personRepo->findOneBy(['personId' => $personOrId]);
        }
        elseif ($personOrId instanceof Person && ! $em->contains($personOrId)) {
            $personRepo = $em->getRepository(Person::class);
            $existingPerson = $personRepo->findOneBy(['personId' => $personOrId->getPersonId()]);

            if ($existingPerson) {
                $person = $existingPerson;
            }
        }

        return $person;
    }

    /**
     * @return DoctrineManager
     */
    protected function getDoctrineManager () {
        return $this->doctrineManager;
    }

    /**
     * @param DoctrineManager $doctrineManager
     */
    protected function setDoctrineManager ($doctrineManager) {
        $this->doctrineManager = $doctrineManager;
    }

    /**
     * @return \Application\Doctrine\AuditedEntityManager
     */
    protected function getEntityManager () {
        return $this->getDoctrineManager()->getEntityManager();
    }

    /**
     * @return MailService
     */
    protected function getMailService () {
        return $this->mailService;
    }

    /**
     * @param MailService $mailService
     */
    protected function setMailService ($mailService) {
        $this->mailService = $mailService;
    }

    /**
     * @return JWTService
     */
    protected function getJWTService () {
        return $this->JWTService;
    }

    /**
     * @param JWTService $JWTService
     */
    protected function setJWTService ($JWTService) {
        $this->JWTService = $JWTService;
    }

    /**
     * @return mixed
     */
    protected function getConfig () {
        return $this->config;
    }

    /**
     * @param mixed $config
     */
    protected function setConfig ($config) {
        $this->config = $config;
    }

    /**
     * @return RouteStackInterface
     */
    public function getRouter () {
        return $this->router;
    }

    /**
     * @param RouteStackInterface $router
     */
    public function setRouter ($router) {
        $this->router = $router;
    }
}