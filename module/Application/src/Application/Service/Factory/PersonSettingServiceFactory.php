<?php
namespace Application\Service\Factory;

use Application\Service\DoctrineManager;
use Application\Service\PersonSettingService;
use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\AbstractFactoryInterface;

class PersonSettingServiceFactory implements AbstractFactoryInterface {

    /**
     * Can the factory create an instance for the service?
     *
     * @param  ContainerInterface $container
     * @param  string $requestedName
     * @return bool
     */
    public function canCreate (ContainerInterface $container, $requestedName) {
        return $requestedName == PersonSettingService::class;
    }

    /**
     * Create an object
     *
     * @param  ContainerInterface $container
     * @param  string $requestedName
     * @param  null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service.
     * @throws ServiceNotCreatedException if an exception is raised when
     *     creating a service.
     * @throws ContainerException if any other error occurs
     */
    public function __invoke (ContainerInterface $container, $requestedName, array $options = null) {
        $config = $container->get('config');
        $doctrineManager = $container->get(DoctrineManager::class);
        return new PersonSettingService($config, $doctrineManager);
    }
}