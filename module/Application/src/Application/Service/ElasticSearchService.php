<?php
namespace Application\Service;

use Elasticsearch\Client;
use Elasticsearch\ClientBuilder;

class ElasticSearchService {
    private $client;
    private $config;

    public function __construct ($config) {
        $this->setConfig($config);
    }

    /**
     * @return Client
     */
    public function getClient () {
        if (! $this->client) {
            $client = ClientBuilder::fromConfig($this->getConfig());
            $this->setClient($client);
        }

        return $this->client;
    }

    /**
     * @param Client $client
     */
    public function setClient (Client $client) {
        $this->client = $client;
    }

    /**
     * @return mixed
     */
    public function getConfig () {
        return $this->config;
    }

    /**
     * @param mixed $config
     */
    public function setConfig ($config) {
        $this->config = $config;
    }
}