<?php
namespace Application;

use Application\Event\Manager;
use Application\Service\PredisService;
use Application\Session\SaveHandler\Predis;
use Laminas\EventManager\Event;
use Laminas\ModuleManager\ModuleManager;
use Laminas\Mvc\MvcEvent;
use Laminas\Router\RouteMatch;
use Laminas\Session\Config\SessionConfig;
use Laminas\Session\Container;
use Laminas\Session\SessionManager;

class Module {
    public function init (ModuleManager $manager) {}

    public function getConfig () { return include __DIR__ . '/../../config/module.config.php'; }

    public function onBootstrap (MvcEvent $e) {
        $em = $e->getApplication()->getEventManager();

        $em->attach(MvcEvent::EVENT_ROUTE, [$this, 'onRoute'], 1);
        $em->attach(MvcEvent::EVENT_ROUTE, [$this, 'onPreDispatch'], 1000);

        Manager::get()->attach('init_session', [$this, 'onSessionInit']);
    }

    public function onSessionInit (Event $e) {
        $name = $e->getName();
        $params = $e->getParams();
    }

    public function onRoute (MvcEvent $e) {
        $matches = $e->getRouteMatch();

        if (! $matches instanceof RouteMatch) {
            return;
        }

        // Get the name of the route that matched
        $routeName = $matches->getMatchedRouteName();

        // Get the controller name from the route
        $controllerName = $matches->getParam('controller');

        // Store the original to use it later
        $matches->setParam('_controller', $controllerName);

        // Translate controller name to useful class name
        $controllerName = __NAMESPACE__ . '\\Controller\\' . ucfirst($routeName) . '\\' . ucfirst($controllerName) . 'Controller';

        // Save the new controller name
        $matches->setParam('controller', $controllerName);
    }

    public function onPreDispatch (MvcEvent $e) {
        $serviceManager = $e->getApplication()->getServiceManager();

        $config = $serviceManager->get('config');

        $this->initSession($serviceManager, $config['session'], $config['session_custom']);
    }

    /**
     * @param \Laminas\ServiceManager\ServiceLocatorInterface $serviceManager
     * @param array $config
     * @param array $customConfig
     */
    public function initSession($serviceManager, $config, $customConfig) {
        $sessionConfig = new SessionConfig();
        $sessionConfig->setOptions($config);
        $sessionManager = new SessionManager($sessionConfig);

        if ($customConfig['type'] == 'new_predis') {
            $predisSaveHandler = new Predis($customConfig);
            $sessionManager->setSaveHandler($predisSaveHandler);
        }
        elseif ($customConfig['type'] == 'existing_predis') {
            /** @var PredisService $predisService */
            $predisService = $serviceManager->get(PredisService::class);
            $predisSaveHandler = new Predis($customConfig, $predisService->getClient());
            $sessionManager->setSaveHandler($predisSaveHandler);
        }

        $sessionManager->start();

        Container::setDefaultManager($sessionManager);

        Manager::get()->trigger('init_session', $this, ['session' => Container::getDefaultManager()->getId()]);
    }
}
