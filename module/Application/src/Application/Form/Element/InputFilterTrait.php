<?php
namespace Application\Form\Element;

use Laminas\Filter\FilterInterface;
use Laminas\Validator\AbstractValidator;
use Laminas\Validator\ValidatorInterface;

trait InputFilterTrait {
    protected $validators = [];
    protected $filters = [];
    protected $required = false;

    /**
     * Should return an array specification compatible with
     * {@link Laminas\InputFilter\Factory::createInput()}.
     *
     * @return array
     */
    public function getInputSpecification () {
        return [
            'name' => $this->getName(),
            'required' => $this->getRequired(),
            'filters' => $this->getFilters(),
            'validators' => $this->getValidators(),
        ];
    }

    /**
     * @return array
     */
    public function getValidators () {
        return $this->validators;
    }

    /**
     * @param array $validators
     * @return $this
     */
    public function setValidators ($validators) {
        $this->validators = $validators;

        return $this;
    }

    /**
     * @return array
     */
    public function getFilters () {
        return $this->filters;
    }

    /**
     * @param array $filters
     * @return $this
     */
    public function setFilters ($filters) {
        $this->filters = $filters;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getRequired () {
        return $this->required;
    }

    /**
     * @param boolean $required
     * @return $this
     */
    public function setRequired ($required = true) {
        $this->required = $required;

        return $this;
    }

    /**
     * Add a validator to the element
     * @param ValidatorInterface|AbstractValidator $validator
     * @return $this
     */
    public function addValidator (ValidatorInterface $validator, $message = null) {
        if ($message) {
            $validator->setMessage($message);
        }

        $this->validators[] = $validator;

        return $this;
    }

    /**
     * Add a filter to the element
     * @param FilterInterface $filter
     * @return $this
     */
    public function addFilter (FilterInterface $filter) {
        $this->filters[] = ['name' => $filter];

        return $this;
    }

    /**
     * Add an error message to be displayed
     * @param $message
     * @param null $key
     */
    public function addMessage ($message, $key = null) {
        $messages = $this->getMessages();

        if ($key) {
            $messages[$key] = $message;
        }
        else {
            $messages[] = $message;
        }

        $this->setMessages($messages);
    }
}