<?php
namespace Application\Form\Element;

use Laminas\InputFilter\InputProviderInterface;

class Text extends \Laminas\Form\Element\Text implements InputProviderInterface {
    use InputFilterTrait;
}
