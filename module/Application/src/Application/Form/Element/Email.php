<?php
namespace Application\Form\Element;

use Laminas\InputFilter\InputProviderInterface;

class Email extends \Laminas\Form\Element\Email implements InputProviderInterface {
    use InputFilterTrait;
}
