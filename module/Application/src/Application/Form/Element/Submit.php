<?php
namespace Application\Form\Element;

use Laminas\InputFilter\InputProviderInterface;

class Submit extends \Laminas\Form\Element\Submit implements InputProviderInterface {
    use InputFilterTrait;
}
