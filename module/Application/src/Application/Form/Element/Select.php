<?php
namespace Application\Form\Element;

use Laminas\InputFilter\InputProviderInterface;

class Select extends \Laminas\Form\Element\Select implements InputProviderInterface {
    use InputFilterTrait;
}
