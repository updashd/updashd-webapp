<?php
namespace Application\Form\Element;

use Laminas\InputFilter\InputProviderInterface;

class Week extends \Laminas\Form\Element\Week implements InputProviderInterface {
    use InputFilterTrait;
}
