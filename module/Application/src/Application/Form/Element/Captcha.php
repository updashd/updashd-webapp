<?php
namespace Application\Form\Element;

use Laminas\InputFilter\InputProviderInterface;

class Captcha extends \Laminas\Form\Element\Captcha implements InputProviderInterface {
    use InputFilterTrait;
}
