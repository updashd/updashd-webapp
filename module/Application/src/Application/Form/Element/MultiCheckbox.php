<?php
namespace Application\Form\Element;

use Laminas\InputFilter\InputProviderInterface;

class MultiCheckbox extends \Laminas\Form\Element\MultiCheckbox implements InputProviderInterface {
    use InputFilterTrait;
}
