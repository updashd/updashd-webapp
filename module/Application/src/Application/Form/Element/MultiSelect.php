<?php
namespace Application\Form\Element;

use Laminas\InputFilter\InputProviderInterface;

class MultiSelect extends \Laminas\Form\Element\Select implements InputProviderInterface {
    use InputFilterTrait;
}
