<?php
namespace Application\Form\Element;

use Laminas\InputFilter\InputProviderInterface;

class Range extends \Laminas\Form\Element\Range implements InputProviderInterface {
    use InputFilterTrait;
}
