<?php
namespace Application\Form\Element;

use Laminas\InputFilter\InputProviderInterface;

class MonthSelect extends \Laminas\Form\Element\MonthSelect implements InputProviderInterface {
    use InputFilterTrait;
}
