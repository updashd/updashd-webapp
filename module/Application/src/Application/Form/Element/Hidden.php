<?php
namespace Application\Form\Element;

use Laminas\InputFilter\InputProviderInterface;

class Hidden extends \Laminas\Form\Element\Hidden implements InputProviderInterface {
    use InputFilterTrait;
}
