<?php
namespace Application\Form\Element;

use Laminas\InputFilter\InputProviderInterface;

class DateTime extends \Laminas\Form\Element\DateTime implements InputProviderInterface {
    use InputFilterTrait;
}
