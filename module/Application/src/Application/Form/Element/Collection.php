<?php
namespace Application\Form\Element;

use Laminas\InputFilter\InputProviderInterface;

class Collection extends \Laminas\Form\Element\Collection implements InputProviderInterface {
    use InputFilterTrait;
}
