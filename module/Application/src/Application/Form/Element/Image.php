<?php
namespace Application\Form\Element;

use Laminas\InputFilter\InputProviderInterface;

class Image extends \Laminas\Form\Element\Image implements InputProviderInterface {
    use InputFilterTrait;
}
