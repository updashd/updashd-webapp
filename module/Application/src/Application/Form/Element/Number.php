<?php
namespace Application\Form\Element;

use Laminas\InputFilter\InputProviderInterface;

class Number extends \Laminas\Form\Element\Number implements InputProviderInterface {
    use InputFilterTrait;
}
