<?php
namespace Application\Form\Element;

use Laminas\InputFilter\InputProviderInterface;

class Textarea extends \Laminas\Form\Element\Textarea implements InputProviderInterface {
    use InputFilterTrait;
}
