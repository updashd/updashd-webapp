<?php
namespace Application\Form\Element;

use Laminas\InputFilter\InputProviderInterface;

class Checkbox extends \Laminas\Form\Element\Checkbox implements InputProviderInterface {
    use InputFilterTrait;
}
