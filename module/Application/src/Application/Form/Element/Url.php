<?php
namespace Application\Form\Element;

use Laminas\InputFilter\InputProviderInterface;

class Url extends \Laminas\Form\Element\Url implements InputProviderInterface {
    use InputFilterTrait;
}
