<?php
namespace Application\Form\Element;

use Laminas\InputFilter\InputProviderInterface;

class Date extends \Laminas\Form\Element\Date implements InputProviderInterface {
    use InputFilterTrait;
}
