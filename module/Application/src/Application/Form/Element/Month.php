<?php
namespace Application\Form\Element;

use Laminas\InputFilter\InputProviderInterface;

class Month extends \Laminas\Form\Element\Month implements InputProviderInterface {
    use InputFilterTrait;
}
