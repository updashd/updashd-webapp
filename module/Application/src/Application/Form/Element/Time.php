<?php
namespace Application\Form\Element;

use Laminas\InputFilter\InputProviderInterface;

class Time extends \Laminas\Form\Element\Time implements InputProviderInterface {
    use InputFilterTrait;
}
