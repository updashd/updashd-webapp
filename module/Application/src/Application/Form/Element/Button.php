<?php
namespace Application\Form\Element;

use Laminas\InputFilter\InputProviderInterface;

class Button extends \Laminas\Form\Element\Button implements InputProviderInterface {
    use InputFilterTrait;
}
