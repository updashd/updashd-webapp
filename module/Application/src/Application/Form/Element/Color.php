<?php
namespace Application\Form\Element;

use Laminas\InputFilter\InputProviderInterface;

class Color extends \Laminas\Form\Element\Color implements InputProviderInterface {
    use InputFilterTrait;
}
