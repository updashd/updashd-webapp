<?php
namespace Application\Form\Element;

use Laminas\InputFilter\InputProviderInterface;

class Password extends \Laminas\Form\Element\Password implements InputProviderInterface {
    use InputFilterTrait;
}
