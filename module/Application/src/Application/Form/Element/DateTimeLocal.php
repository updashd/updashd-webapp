<?php
namespace Application\Form\Element;

use Laminas\InputFilter\InputProviderInterface;

class DateTimeLocal extends \Laminas\Form\Element\DateTimeLocal implements InputProviderInterface {
    use InputFilterTrait;
}
