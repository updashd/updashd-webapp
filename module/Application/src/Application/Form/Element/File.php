<?php
namespace Application\Form\Element;

use Laminas\InputFilter\InputProviderInterface;

class File extends \Laminas\Form\Element\File implements InputProviderInterface {
    use InputFilterTrait;
}
