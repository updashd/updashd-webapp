<?php
namespace Application\Form\Element;

use Laminas\InputFilter\InputProviderInterface;

class Csrf extends \Laminas\Form\Element\Csrf implements InputProviderInterface {
    use InputFilterTrait;
}
