<?php
namespace Application\Form\Element;

use Laminas\InputFilter\InputProviderInterface;

class Radio extends \Laminas\Form\Element\Radio implements InputProviderInterface {
    use InputFilterTrait;
}
