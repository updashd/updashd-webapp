<?php
namespace Application\Form;

use Application\Form\Element\Select;
use Updashd\Configlib\Config as ConfiglibConfig;
use Updashd\Configlib\Field;
use Updashd\Configlib\Group;

abstract class AbstractConfigForm extends AbstractForm {
    const PREFIX = 'config_';
    const SUFFIX_REFERENCE_FIELD = '_reference_field';
    const SUFFIX_USE_ENCRYPTION = '_use_encryption';
    const SUFFIX_FIELD_TYPE = '_field_type';
    
    const FIELD_TYPE_REFERENCE_FIELD = 'reference_field';
    const FIELD_TYPE_DEFAULT = 'default';
    const FIELD_TYPE_VALUE = 'value';
    const FIELD_TYPE_ENCRYPTED = 'encrypted';

    protected $config;
    
    /**
     * @param ConfiglibConfig $config
     * @param string $name
     * @param array $options
     */
    public function __construct (ConfiglibConfig $config, $name = 'configForm', array $options = array()) {
        $this->setConfig($config);
        
        parent::__construct($name, $options);
    }
    
    /**
     * Initialize the form by adding all the fields specified by the config
     */
    public function init () {
        $workerConfig = $this->getConfig();
        
        foreach ($workerConfig->getFields() as $fieldName => $field) {
            $this->addField($fieldName, $field);
        }
        
        $this->addElementSubmit('submit', 'Submit');
    }

    /**
     * Get an array of the Form Element names for an array of fields.
     * @param Field[] $fields
     * @return array
     */
    public function getFieldNamesFromFields ($fields) {
        $fieldNames = [];

        foreach ($fields as $fieldName => $field) {
            $fieldNames[] = self::PREFIX . $fieldName;
        }

        return $fieldNames;
    }
    
    /**
     * Get an array of the Form Element names.
     * @deprecated Use getConfigGroupsAndFieldNames() instead
     * @return array
     */
    public function getWorkerFieldNames () {
        $workerConfig = $this->getConfig();

        return $this->getFieldNamesFromFields($workerConfig->getFields());
    }

    /**
     * Get an array of groups and their fields.
     * Eg.
     * [
     *     [
     *         'label' => 'Group Name',
     *         'fields' => ['field-elem-name-1', 'field-elem-name-2', ...]
     *     ],
     *     ...
     * ]
     * @return array
     */
    public function getConfigGroupsAndFieldNames () {
        $workerConfig = $this->getConfig();

        $result = [];

        foreach ($workerConfig->getGroups() as $groupKey => $group) {
            $result[] = [
                'label' => $group->getLabel(),
                'fields' => $this->getFieldNamesFromFields($group->getFields())
            ];
        }

        return $result;
    }

    /**
     * @return \Updashd\Configlib\Group[]
     */
    public function getGroups () {
        $workerConfig = $this->getConfig();

        $groups = $workerConfig->getGroups();

        if (! $groups) {
            $group = new Group('General');

            foreach ($workerConfig->getFields() as $fieldKey => $field) {
                $group->addField($fieldKey, $field);
            }

            $groups = [$group];
        }

        return $groups;
    }
    
    /**
     * Get the value field element name
     * @param string $fieldName
     * @return string
     */
    protected function getFieldElementName ($fieldName) {
        return self::PREFIX . $fieldName;
    }
    
    /**
     * Get the name of the source select element.
     * @param string $fieldName
     * @return string
     */
    protected function getFieldSourceElementName ($fieldName) {
        return $fieldName . self::SUFFIX_FIELD_TYPE;
    }
    
    /**
     * Get the name of the reference field select element for a field.
     * @param $fieldName
     * @return string
     */
    protected function getFieldReferenceElementName ($fieldName){
        return $fieldName . self::SUFFIX_REFERENCE_FIELD;
    }

    /**
     * Get the name of the use encryption element for a field.
     * @param $fieldName
     * @return string
     */
    protected function getUseEncryptionElementName ($fieldName){
        return $fieldName . self::SUFFIX_USE_ENCRYPTION;
    }
    
    /**
     * Add a field from the config to the form
     * @param string $fieldName
     * @param Field $field
     * @return \Laminas\Form\ElementInterface
     * @throws \Exception
     */
    protected function addField($fieldName, Field $field) {
        $label = $field->getLabel();
        
        $name = $this->getFieldElementName($fieldName);
        $sourceElementName = $this->getFieldSourceElementName($name);
        $referenceElementName = $this->getFieldReferenceElementName($name);
        $useEncryptionElementName = $this->getUseEncryptionElementName($name);

        // The type of field (text, select, radio, etc.)
        $type = $field->getType();
        
        // Add the appropriate form element
        switch ($type) {
            case $field::FIELD_TYPE_TEXT: $this->addElementText($name, $label); break;
            case $field::FIELD_TYPE_NUMBER: $this->addElementText($name, $label); break;
            case $field::FIELD_TYPE_PASSWORD: $this->addElementText($name, $label); break;
            case $field::FIELD_TYPE_URL: $this->addElementText($name, $label); break;
            case $field::FIELD_TYPE_CHECKBOX: $this->addElementCheckbox($name, $label); break;
            case $field::FIELD_TYPE_MULTI_LINE_TEXT: $this->addElementTextarea($name, $label); break;
            case $field::FIELD_TYPE_RADIO: $this->addElementRadio($name, $label, $field->getOptions()); break;
            case $field::FIELD_TYPE_SELECT: $this->addElementSelect($name, $label, $field->getOptions(), $field->getDefaultValue()); break;
            case $field::FIELD_TYPE_MULTI_CHECKBOX: $this->addElementMultiSelect($name, $label, $field->getOptions(), $field->getDefaultValue()); break;
            default: throw new \Exception('Unknown worker configuration field type: ' . $type); break;
        }
    
        $element = $this->get($name);
        
        // Set the default value as the placeholder
        $defaultValue = $field->getDefaultValue();

        if ($defaultValue !== null) {
            $element->setAttribute('placeholder', $defaultValue);
        }
    
        // Add an element for the source
        $options = $this->getFieldTypeOptions(! $field->getIsRequired() || $defaultValue !== null ? true : false);
        $this->addElementSelect($sourceElementName, 'Field Type', $options);
        
        // Add an element for select a reference field
        $this->addElementSelect($referenceElementName, 'Reference Field', $this->getReferenceFieldOptions());

        $this->addElementCheckbox($useEncryptionElementName, 'Encrypt this value');
        
        return $element;
    }

    /**
     * Populate the form from the service config.
     */
    protected function populateForm () {
        $workerConfig = $this->getConfig();
    
        foreach ($workerConfig->getFields() as $fieldName => $field) {
            $valueElementName = $this->getFieldElementName($fieldName);
            $sourceElementName = $this->getFieldSourceElementName($valueElementName);
            $referenceElementName = $this->getFieldReferenceElementName($valueElementName);
            $useEncryptionElementName = $this->getUseEncryptionElementName($valueElementName);
            
            $fieldType = $this->getFieldSource($field);
            
            // Set element values
            $this->get($valueElementName)->setValue($field->getValue());
            $this->get($sourceElementName)->setValue($fieldType);
            $this->get($referenceElementName)->setValue($field->getReferenceField());
            $this->get($useEncryptionElementName)->setValue($field->getUseEncryption());

            // Handle a field that has an encrypted value
            if ($field->hasEncryptedValue()) {
                /** @var Select $sourceElement */
                $sourceElement = $this->get($sourceElementName);

                // Set new options
                $options = [];
                $options[self::FIELD_TYPE_ENCRYPTED] = 'Encrypted Value';
                $options += $sourceElement->getValueOptions();
                $sourceElement->setValueOptions($options);

                $sourceElement->setValue(self::FIELD_TYPE_ENCRYPTED);
            }
        }
    }
    
    /**
     * Populate the config from form values
     */
    protected function populateConfig () {
        $workerConfig = $this->getConfig();

        foreach ($workerConfig->getFields() as $fieldName => $field) {
            $valueElementName = $this->getFieldElementName($fieldName);
            $sourceElementName = $this->getFieldSourceElementName($valueElementName);
            $referenceElementName = $this->getFieldReferenceElementName($valueElementName);
            $useEncryptionElementName = $this->getUseEncryptionElementName($valueElementName);

            // Get the type from the source select element
            $fieldType = $this->getElementValue($sourceElementName);

            // Clear any pre-existing value
            $field->setValue(null);
            $field->setReferenceField(null);

            // Clear any previously encrypted value if needed
            if ($fieldType != self::FIELD_TYPE_ENCRYPTED) {
                $field->setEncryptedValue(null);
                $field->setUseEncryption(false);
            }

            // If "Value" was chosen, set the value from the value form element
            if ($fieldType == self::FIELD_TYPE_VALUE) {
                $field->setValue($this->getElementValue($valueElementName));

                // Set whether to use encryption or not
                $field->setUseEncryption($this->getElementValue($useEncryptionElementName));
            }
            // If "Reference Field" was chosen, set the reference field from the reference field form element.
            elseif ($fieldType == self::FIELD_TYPE_REFERENCE_FIELD) {
                $field->setReferenceField($this->getElementValue($referenceElementName));
            }
        }
    }

    /**
     * Determine if the form is valid for the given service configuration. This will also call populateConfig.
     * @return bool
     */
    public function isValid () {
        $this->populateConfig();

        $workerConfig = $this->getConfig();

        // Set the proper elements as required when needed
        foreach ($workerConfig->getFields() as $fieldName => $field) {
            $valueElementName = $this->getFieldElementName($fieldName);
            $sourceElementName = $this->getFieldSourceElementName($valueElementName);
            $referenceElementName = $this->getFieldReferenceElementName($valueElementName);

            $valueElement = $this->get($valueElementName);
            $referenceElement = $this->get($referenceElementName);
            $fieldType = $this->getElementValue($sourceElementName);

            // If Reference Field is selected, require a source
            if ($fieldType == self::FIELD_TYPE_REFERENCE_FIELD) {
                $referenceElement->setRequired(true);
            }
            // If the field is required
            elseif ($field->getIsRequired() && $fieldType == self::FIELD_TYPE_VALUE && $field->getDefaultValue() === null) {
                $valueElement->setRequired(true);
            }
        }

        $isValid = parent::isValid();

        foreach ($workerConfig->getFields() as $fieldName => $field) {
            $valueElementName = $this->getFieldElementName($fieldName);
            $valueElement = $this->get($valueElementName);

            // If the field value is not valid
            if (! $field->isValid(true)) {
                // Add all the error messages to the form element
                foreach ($field->getMessages() as $message) {
                    $valueElement->addMessage($message);
                }

                // Mark the form as invalid
                $isValid = false;
            }
        }
        
        return $isValid;
    }
    
    /**
     * Determine what the selected field type is on a certain field.
     * @param Field $field
     * @return string
     */
    protected function getFieldSource ($field) {
        if ($field->getValue() !== null) {
            return self::FIELD_TYPE_VALUE;
        }
        elseif ($field->getReferenceField() !== null) {
            return self::FIELD_TYPE_REFERENCE_FIELD;
        }
        elseif ($field->getDefaultValue() !== null) {
            return self::FIELD_TYPE_DEFAULT;
        }
        else {
            return self::FIELD_TYPE_DEFAULT;
        }
    }
    
    /**
     * Hydrate the config object from a config json string.
     * @param $json
     */
    public function hydrateFromJson ($json) {
        $workerConfig = $this->getConfig();
        $workerConfig->fromJson($json);
        $this->populateForm();
    }
    
    /**
     * Save the config to json string.
     * @return string json encoded string.
     */
    public function saveToJson () {
        $this->populateConfig();
        $workerConfig = $this->getConfig();
        return $workerConfig->toJson();
    }

    /**
     * Get the available sources for a fields value.
     * @param bool $includeDefault
     * @return array
     */
    protected function getFieldTypeOptions ($includeDefault = true) {
        $options = [];
        
        if ($includeDefault) {
            $options[self::FIELD_TYPE_DEFAULT] = 'Default';
        }
        
        $options[self::FIELD_TYPE_REFERENCE_FIELD] = 'Reference Field';
        $options[self::FIELD_TYPE_VALUE] = 'Value';
        
        return $options;
    }
    
    /**
     * Get the list of reference field options
     * @return array
     */
    protected function getReferenceFieldOptions () {
        return [
            '' => '--None--',
        ];
    }
    
    /**
     * @return ConfiglibConfig
     */
    public function getConfig () {
        return $this->config;
    }
    
    /**
     * @param ConfiglibConfig $config
     */
    public function setConfig (ConfiglibConfig $config) {
        $this->config = $config;
    }
}