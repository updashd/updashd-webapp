<?php
namespace Application\Form;

class Notifier extends AbstractForm {
    public function init () {
        $this->addElementHidden('account_notifier_id');

        $this->addElementSelect('notifier_id', 'Type')
            ->setRequired();;

        $this->addElementText('account_notifier_name', 'Name')
            ->setRequired();

        $this->addElementCheckbox('is_default', 'Is Default');

        $this->addElementText('sort_order', 'Sort Order')
            ->setRequired();

        $this->addElementSelect('frequency', 'Notice Frequency', $this->getFrequencyOptions(), '3600')
            ->setRequired();

        $this->addElementSubmit('submit', 'Submit');
    }

    public function getFrequencyOptions () {
        return [
            '0' => 'Always',
            '60' => '1 Minute',
            '300' => '5 Minutes',
            '600' => '10 Minutes',
            '900' => '15 Minutes',
            '1800' => '30 Minutes',
            '3600' => '1 Hour',
            '7200' => '2 Hours',
            '18000' => '5 Hours',
            '21600' => '6 Hours',
            '36000' => '10 Hours',
            '43200' => '12 Hours',
            '86400' => '24 Hours',
        ];
    }
}