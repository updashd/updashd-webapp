<?php
namespace Application\Form;

class Zone extends AbstractForm {
    public function init () {
        $this->addElementText('zone_id', 'Zone ID')
            ->setRequired();
        
        $this->addElementText('zone_name', 'Name')
            ->setRequired();

        $this->addElementText('sort_order', 'Sort Order')
            ->setRequired();
        
        $this->addElementSelect('account_id', 'Account');
        
        $this->addElementSubmit('submit', 'Submit');
    }
}