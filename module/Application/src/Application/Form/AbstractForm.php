<?php
namespace Application\Form;

use Laminas\Filter\Inflector;
use Laminas\Filter\Word\UnderscoreToStudlyCase;

abstract class AbstractForm extends Form {
    public function isDataValid ($data) {
        $this->setData($data);
        
        return $this->isValid();
    }

    public function setDisabled ($disabled = true) {
        /** @var Element $element */
        foreach ($this->getElements() as $element) {
            $element->setAttribute('disabled', $disabled ? 'disabled' : null);
        }
    }

    public function setReadOnly ($readOnly = true) {
        /** @var Element $element */
        foreach ($this->getElements() as $element) {
            $element->setAttribute('readonly', $readOnly ? 'readonly' : null);
        }
    }

    public function setMethod ($method) {
        $this->setAttribute('method', $method);
    }

    public function setAction ($action) {
        $this->setAttribute('action', $action);
    }

    public function hydrateFromEntity ($entity) {
        $reflector = new \ReflectionClass($entity);

        /** @var Element $element */
        foreach ($this->getElements() as $element) {
            $inflector = new Inflector(':fieldName');
            $inflector->setRules([
                ':fieldName' => [UnderscoreToStudlyCase::class]
            ]);

            $getterName = $inflector->filter(['fieldName' =>  'get_' . $element->getName()]);

            // Only try populating if there is a getter for the requested value.
            if ($reflector->hasMethod($getterName)) {
                $value = call_user_func([$entity, $getterName]);
                $element->setValue($value);
            }
        }
    }

    public function saveToEntity ($entity) {
        $reflector = new \ReflectionClass($entity);

        /** @var Element $element */
        foreach ($this->getElements() as $element) {
            $inflector = new Inflector(':fieldName');
            $inflector->setRules([
                ':fieldName' => [UnderscoreToStudlyCase::class]
            ]);

            $setterName = $inflector->filter(['fieldName' =>  'set_' . $element->getName()]);
            $getterName = $inflector->filter(['fieldName' =>  'get_' . $element->getName()]);

            // If we have both a getter and setter for that value
            if ($reflector->hasMethod($setterName) && $reflector->hasMethod($getterName)) {
                $newValue = $element->getValue();

                // Get the current value
                $oldValue = call_user_func([$entity, $getterName]);

                // Only set it if it needs to be updated.
                if ($oldValue != $newValue) {
                    call_user_func([$entity, $setterName], $newValue);
                }
            }
            // If we only have a setter
            elseif ($reflector->hasMethod($setterName)) {
                call_user_func([$entity, $setterName], $element->getValue());
            }
        }
    }
}