<?php

namespace Application\Form\Portal;

use Application\Form\AbstractForm;
use Application\Service\AccountService;
use Application\Validator\Username;

class CreateAccount extends AbstractForm {
    /** @var AccountService */
    protected $accountService;

    public function __construct ($accountService, $name = '', array $options = array()) {
        parent::__construct($name, $options);

        $this->accountService = $accountService;
    }

    public function init () {
        $this->addElementText('name', 'Name')
            ->setRequired();

        $this->addElementText('slug', 'Slug')
            ->setRequired()
            ->addValidator(new Username());

        $this->addElementSubmit('submit', 'Create');
    }

    public function isValid () {
        $isValid = parent::isValid();

        if (! $this->accountService->isAccountSlugAvailable($this->getElementValue('slug'))) {
            $this->get('slug')->addMessage('Slug is already in use');
            $isValid = false;
        }

        return $isValid;
    }
}