<?php
namespace Application\Form;

use Updashd\Configlib\Config as ConfiglibConfig;

class NotifierConfig extends AbstractConfigForm {
    public function __construct (ConfiglibConfig $config, $name = 'notifierConfigForm', array $options = array()) {
        parent::__construct($config, $name, $options);
    }
}