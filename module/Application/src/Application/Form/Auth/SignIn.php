<?php
namespace Application\Form\Auth;

use Application\Form\AbstractForm;
use Laminas\Validator\StringLength;

class SignIn extends AbstractForm {
    public function init () {
        $this->addElementText('username', 'Username or Email')
            ->addValidator(new StringLength(['min' => 3, 'max' => 100]))
            ->setRequired();

        $this->addElementPassword('password', 'Password')
            ->setRequired();

        $this->addElementCheckbox('remember_me', 'Remember Me?');

        $this->addElementSubmit('submit', 'Sign In');
    }
}