<?php
namespace Application\Form\Auth;

use Application\Form\AbstractForm;
use Application\Service\AccountService;
use Application\Validator\PasswordStrength;
use Application\Validator\Username;
use Laminas\Validator\EmailAddress;
use Laminas\Validator\StringLength;

class SignUp extends AbstractForm {
    /** @var AccountService */
    protected $accountService;

    public function __construct ($accountService, $name = '', array $options = array()) {
        $this->accountService = $accountService;

        parent::__construct($name, $options);
    }

    public function init () {
        $this->addElementText('name', 'Full Name')
            ->setRequired();

        $this->addElementText('email', 'Email Address')
            ->addValidator(new EmailAddress())
            ->setRequired();

        $this->addElementText('username', 'Username')
            ->addValidator(new StringLength(['min' => 3, 'max' => 100]))
            ->addValidator(new Username())
            ->setRequired();

        $this->addElementPassword('password', 'Password')
            ->addValidator(new PasswordStrength())
            ->setRequired();

        $this->addElementPassword('password_verify', 'Repeat Password')
            ->setRequired();

        $this->addElementCheckbox('first_born_son', 'I will give you my first born son.')
            ->setRequired();

        $this->addElementSubmit('submit', 'Sign Up');
    }

    public function isValid () {
        $isValid = parent::isValid();
        $username = $this->getElementValue('username');
        $email = $this->getElementValue('email');

        if (! $this->accountService->isUsernameAvailable($username)) {
            $this->get('username')->addMessage('Username already taken.');
            $isValid = false;
        }

        if (! $this->accountService->isEmailAvailable($email)) {
            $this->get('email')->addMessage('Email already in use.');
            $isValid = false;
        }

        if ($this->getElementValue('password') != $this->getElementValue('password_verify')) {
            $this->get('password_verify')->addMessage('Passwords must match');
            $isValid = false;
        }

        return $isValid;
    }
}