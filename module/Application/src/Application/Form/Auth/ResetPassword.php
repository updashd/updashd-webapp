<?php
namespace Application\Form\Auth;

use Application\Form\AbstractForm;
use Application\Validator\PasswordStrength;

class ResetPassword extends AbstractForm {
    public function init () {
        $this->addElementHidden('t', 'Token')
            ->setRequired(true);

        $this->addElementPassword('password', 'Password')
            ->addValidator(new PasswordStrength())
            ->setRequired();

        $this->addElementPassword('password_verify', 'Repeat Password')
            ->setRequired();

        $this->addElementSubmit('submit', 'Save');
    }

    public function isValid () {
        $isValid = parent::isValid();

        if ($this->getElementValue('password') != $this->getElementValue('password_verify')) {
            $this->get('password_verify')->addMessage('Passwords must match');
            $isValid = false;
        }

        return $isValid;
    }

    public function setToken ($token) {
        $this->setElementValue('t', $token);
    }
}