<?php
namespace Application\Form\Auth;

use Application\Form\AbstractForm;
use Laminas\Validator\StringLength;

class ForgotPassword extends AbstractForm {
    public function init () {
        $this->addElementText('email', 'Email')
            ->addValidator(new StringLength(['min' => 3, 'max' => 100]))
            ->setRequired();

        $this->addElementSubmit('submit', 'Reset');
    }
}