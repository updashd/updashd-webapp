<?php
namespace Application\Form;

class TrendFilter extends AbstractForm {
    protected $accountSlug;

    public function __construct ($accountSlug, $name = '', array $options = array()) {
        $this->setAccountSlug($accountSlug);
        parent::__construct($name, $options);
    }

    public function init () {
        $this->setMethod('GET');

        $this->addElementHidden('account', 'Account')
            ->setValue($this->getAccountSlug());
        
        $this->addElementMultiSelect('environment', 'Environment');
        $this->addElementMultiSelect('zone', 'Zone');
        $this->addElementMultiSelect('node', 'Node');
        $this->addElementMultiSelect('service', 'Service');
        $this->addElementMultiSelect('node_service', 'Node Service');
        $this->addElementMultiSelect('field', 'Field');

        $this->addElementSelect('categorize_by', 'Categorize By', [
            'none' => 'None',
            'environment' => 'Environment',
            'zone' => 'Zone',
            'node' => 'Node',
            'service' => 'Service',
            'node ' => 'Node Service',
            'field' => 'Field'
        ]);

        $this->addElementSelect('graph_type', 'Graph Type', [
            'bar' => 'Bar',
            'pie' => 'Pie',
            'time_series' => 'Time Series',
            'heat_map' => 'Head Map'
        ], 'time_series');

        $this->addElementSelect('group_by', 'Group By (X)', [
            '' => 'None',
            'second_of_minute' => 'Second of Minute',
            'minute_of_hour' => 'Minute of Hour',
            'hour_of_day' => 'Hour of Day',
            'day_of_week' => 'Day of Week',
            'week_of_year' => 'Week of Year',
            'second' => 'Second',
            'minute' => 'Minute',
            'hour' => 'Hour',
            'day' => 'Day',
            'week' => 'Week',
            'month' => 'Month',
            'year' => 'Year',
        ]);

        $this->addElementSubmit('generate', 'Generate');
    }

    /**
     * @return string
     */
    public function getAccountSlug () {
        return $this->accountSlug;
    }

    /**
     * @param string $accountSlug
     */
    public function setAccountSlug ($accountSlug) {
        $this->accountSlug = $accountSlug;
    }
}