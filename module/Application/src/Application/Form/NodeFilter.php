<?php
namespace Application\Form;

class NodeFilter extends AbstractForm {
    protected $environmentOptions;
    protected $serviceOptions;

    /**
     * Node constructor.
     * @param array $environmentOptions
     * @param array $serviceOptions
     * @param string $name
     * @param array $options
     */
    public function __construct ($environmentOptions, $serviceOptions, $name = '', array $options = array()) {
        $this->setEnvironmentOptions($environmentOptions);
        $this->setServiceOptions($serviceOptions);

        parent::__construct($name, $options);
    }

    public function init () {
        $this->setMethod('GET');

        $this->addElementMultiSelect('environment', 'Environment', $this->getEnvironmentOptions());
        $this->addElementMultiSelect('service', 'Service', $this->getServiceOptions());
        $this->addElementText('node_name', 'Node Name');
        $this->addElementText('node_hostname', 'Node Host Name');
        $this->addElementText('node_service_name', 'Node Service Name');
        $this->addElementSubmit('filter', 'Filter');
    }

    /**
     * @return array
     */
    public function getEnvironmentOptions () {
        return $this->environmentOptions;
    }

    /**
     * @param array $environmentOptions
     */
    public function setEnvironmentOptions ($environmentOptions) {
        $this->environmentOptions = $environmentOptions;
    }

    /**
     * @return array
     */
    public function getServiceOptions () {
        return $this->serviceOptions;
    }

    /**
     * @param array $serviceOptions
     */
    public function setServiceOptions ($serviceOptions) {
        $this->serviceOptions = $serviceOptions;
    }
}