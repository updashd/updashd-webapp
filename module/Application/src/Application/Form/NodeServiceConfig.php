<?php
namespace Application\Form;

use Updashd\Configlib\Config as ConfiglibConfig;

class NodeServiceConfig extends AbstractConfigForm {
    public function __construct (ConfiglibConfig $config, $name = 'workerConfigForm', array $options = array()) {
        parent::__construct($config, $name, $options);
    }

    /**
     * Get the list of reference field options
     * @return array
     */
    protected function getReferenceFieldOptions () {
        return [
            '' => '--None--',
            'ip' => 'IP',
            'hostname' => 'Host Name',
        ];
    }
}