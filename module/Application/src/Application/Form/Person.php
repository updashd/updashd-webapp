<?php
namespace Application\Form;

use Application\Validator\Username;
use Laminas\Validator\EmailAddress;
use Laminas\Validator\Timezone;

class Person extends AbstractForm {
    public function init () {
        $this->addElementHidden('person_id', 'Person ID');

        $this->addElementText('username', 'Username')
            ->setRequired()
            ->addValidator(new Username());

        $this->addElementText('name', 'Full Name')
            ->setRequired();

        $this->addElementEmail('email', 'Email')
            ->setRequired()
            ->addValidator(new EmailAddress());

        $this->addElementSelect('timezone', 'Time Zone', $this->getTimeZones())
            ->addValidator(new Timezone());

        $this->addElementUrl('profile_picture_url', 'Profile Picture URL');

        $this->addElementSelect('role_type', 'Role', $this->getRoleList(), 'MEMBER')
            ->setRequired();

        $this->addElementText('password_hash', 'Password Hash')
            ->setRequired();

        $this->addElementCheckbox('email_verified', 'Email Verified');

        $this->addElementCheckbox('is_blocked', 'Is Blocked');

        $this->addElementSubmit('submit', 'Submit');
    }

    public function getRoleList () {
        return [
            'ADMIN' => 'Administrator',
            'MEMBER' => 'Member',
        ];
    }

    public function getTimeZones () {
        $timezoneIdentifiers = \DateTimeZone::listIdentifiers();

        $output = [];

        foreach ($timezoneIdentifiers as $timezoneIdentifier) {
            $parts = explode('/', $timezoneIdentifier);

            $continent = array_shift($parts);

            $rest = implode('—', $parts);

            $rest = str_replace('_', ' ', $rest);

            if (! $rest) {
                $rest = $timezoneIdentifier;
            }
            $output[$continent]['label'] = $continent;
            $output[$continent]['options'][$timezoneIdentifier] = $rest;
        }

        return $output;
    }
}