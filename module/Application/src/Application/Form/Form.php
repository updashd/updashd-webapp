<?php
namespace Application\Form;

use Laminas\Form\Form as LaminasForm;
use Laminas\Form\FormInterface as LaminasFormInterface;

class Form extends LaminasForm implements LaminasFormInterface {
    protected $notices = [];

    public function __construct ($name = '', array $options = array()) {
        parent::__construct($name, $options);

        $this->init();
    }

    /**
     * Get the value of a given element
     * @param $elementName
     * @return mixed
     */
    public function getElementValue ($elementName) {
        return $this->get($elementName)->getValue();
    }

    /**
     * @param string $elementName
     * @param mixed $value
     */
    public function setElementValue ($elementName, $value) {
        $this->get($elementName)->setValue($value);
    }

    /**
     * @param string $elementOrFieldset
     * @return \Laminas\Form\ElementInterface|Element\InputFilterTrait
     */
    public function get ($elementOrFieldset) {
        return parent::get($elementOrFieldset);
    }

    public function addNotice ($title, $message, $type = 'warning') {
        $this->notices[] = [
            'title' => $title,
            'message' => $message,
            'type' => $type
        ];
    }

    /**
     * @return array
     */
    public function getNotices () {
        return $this->notices;
    }

    /**
     * @param array $notices
     */
    public function setNotices ($notices) {
        $this->notices = $notices;
    }

    /**
     * @param string $name The name of the field.
     * @param string $value The value for the button.
     * @return Element\Button
     */
    public function addElementButton ($name, $value) {
        $el = new Element\Button($name);
        $el->setValue($value);

        $this->add($el);
        
        return $el;
    }
    
    /**
     * @param string $name The name of the field.
     * @param string $label The label for the field.
     * @return Element\Captcha
     */
    public function addElementCaptcha ($name, $label) {
        $el = new Element\Captcha($name);
        $el->setLabel($label);

        $this->add($el);
        
        return $el;
    }
    
    /**
     * @param string $name The name of the field.
     * @param string $label The label for the field.
     * @param integer|string $value The selected value
     * @return Element\Checkbox
     */
    public function addElementCheckbox ($name, $label, $value = null) {
        $el = new Element\Checkbox($name);
        $el->setLabel($label);

        $el->setCheckedValue('1');
        $el->setUncheckedValue('0');

        if ($value) {
            $el->setValue($value);
        }

        $this->add($el);
        
        return $el;
    }
    
    /**
     * @param string $name The name of the field.
     * @param string $label The label for the field.
     * @return Element\Collection
     */
    public function addElementCollection ($name, $label) {
        $el = new Element\Collection($name);
        $el->setLabel($label);

        $this->add($el);
        
        return $el;
    }
    
    /**
     * @param string $name The name of the field.
     * @param string $label The label for the field.
     * @return Element\Csrf
     */
    public function addElementCsrf ($name, $label) {
        $el = new Element\Csrf($name);
        $el->setLabel($label);

        $this->add($el);
        
        return $el;
    }
    
    /**
     * @param string $name The name of the field.
     * @param string $label The label for the field.
     * @return Element\File
     */
    public function addElementFile ($name, $label) {
        $el = new Element\File($name);
        $el->setLabel($label);

        $this->add($el);
        
        return $el;
    }
    
    /**
     * @param string $name The name of the field.
     * @param string $label The label for the field.
     * @return Element\Hidden
     */
    public function addElementHidden ($name, $label = null) {
        $el = new Element\Hidden($name);

        if ($label) {
            $el->setLabel($label);
        }

        $this->add($el);
        
        return $el;
    }
    
    /**
     * @param string $name The name of the field.
     * @param string $label The label for the field.
     * @return Element\Image
     */
    public function addElementImage ($name, $label) {
        $el = new Element\Image($name);
        $el->setLabel($label);

        $this->add($el);
        
        return $el;
    }
    
    /**
     * @param string $name The name of the field.
     * @param string $label The label for the field.
     * @return Element\MonthSelect
     */
    public function addElementMonthSelect ($name, $label) {
        $el = new Element\MonthSelect($name);
        $el->setLabel($label);

        $this->add($el);
        
        return $el;
    }
    
    /**
     * @param string $name The name of the field.
     * @param string $label The label for the field.
     * @param array $valueOptions Contains all of the options.
     * @param array|string|integer $selectedValueOrValues The option(s) to be selected.
     * @return Element\MultiCheckbox
     */
    public function addElementMultiCheckbox ($name, $label, $valueOptions = [], $selectedValueOrValues = null) {
        $el = new Element\MultiCheckbox($name);
        $el->setLabel($label);
        $el->setValueOptions($valueOptions);

        if ($selectedValueOrValues) {
            $el->setValue($selectedValueOrValues);
        }

        $this->add($el);
        
        return $el;
    }
    
    /**
     * @param string $name The name of the field.
     * @param string $label The label for the field.
     * @return Element\Password
     */
    public function addElementPassword ($name, $label) {
        $el = new Element\Password($name);
        $el->setLabel($label);

        $this->add($el);
        
        return $el;
    }
    
    /**
     * @param string $name The name of the field.
     * @param string $label The label for the field.
     * @param array $valueOptions Contains all of the options for the radio.
     * @return Element\Radio
     */
    public function addElementRadio ($name, $label, $valueOptions = []) {
        $el = new Element\Radio($name);
        $el->setLabel($label);
        $el->setValueOptions($valueOptions);

        $this->add($el);
        
        return $el;
    }
    
    /**
     * @param string $name The name of the field.
     * @param string $label The label for the field.
     * @return Element\Select
     */
    public function addElementSelect ($name, $label, $valueOptions = [], $value = null) {
        $el = new Element\Select($name);
        $el->setLabel($label);

        $el->setValueOptions($valueOptions);

        if ($value) {
            $el->setValue($value);
        }

        $this->add($el);
        
        return $el;
    }
    
    /**
     * @param string $name The name of the field.
     * @param string $label The label for the field.
     * @return Element\Select
     */
    public function addElementMultiSelect ($name, $label, $valueOptions = [], $value = null) {
        $el = new Element\MultiSelect($name);
        $el->setLabel($label);
        $el->setAttribute('multiple', 'multiple');

        $el->setValueOptions($valueOptions);

        if ($value) {
            $el->setValue($value);
        }

        $this->add($el);
        
        return $el;
    }
    
    /**
     * @param string $name The name of the field.
     * @param string $value The value for the field.
     * @return Element\Submit
     */
    public function addElementSubmit ($name, $value) {
        $el = new Element\Submit($name);
        $el->setValue($value);

        $this->add($el);
        
        return $el;
    }
    
    /**
     * @param string $name The name of the field.
     * @param string $label The label for the field.
     * @return Element\Text
     */
    public function addElementText ($name, $label) {
        $el = new Element\Text($name);
        $el->setLabel($label);

        $this->add($el);
        
        return $el;
    }
    
    /**
     * @param string $name The name of the field.
     * @param string $label The label for the field.
     * @return Element\Textarea
     */
    public function addElementTextarea ($name, $label) {
        $el = new Element\Textarea($name);
        $el->setLabel($label);

        $this->add($el);
        
        return $el;
    }
    
    /**
     * @param string $name The name of the field.
     * @param string $label The label for the field.
     * @return Element\Color
     */
    public function addElementColor ($name, $label) {
        $el = new Element\Color($name);
        $el->setLabel($label);

        $this->add($el);
        
        return $el;
    }
    
    /**
     * @param string $name The name of the field.
     * @param string $label The label for the field.
     * @return Element\Date
     */
    public function addElementDate ($name, $label) {
        $el = new Element\Date($name);
        $el->setLabel($label);

        $this->add($el);
        
        return $el;
    }
    
    /**
     * @param string $name The name of the field.
     * @param string $label The label for the field.
     * @return Element\DateTime
     */
    public function addElementDateTime ($name, $label) {
        $el = new Element\DateTime($name);
        $el->setLabel($label);

        $this->add($el);
        
        return $el;
    }
    
    /**
     * @param string $name The name of the field.
     * @param string $label The label for the field.
     * @return Element\DateTimeLocal
     */
    public function addElementDateTimeLocal ($name, $label) {
        $el = new Element\DateTimeLocal($name);
        $el->setLabel($label);

        $this->add($el);
        
        return $el;
    }
    
    /**
     * @param string $name The name of the field.
     * @param string $label The label for the field.
     * @return Element\Email
     */
    public function addElementEmail ($name, $label) {
        $el = new Element\Email($name);
        $el->setLabel($label);

        $this->add($el);
        
        return $el;
    }
    
    /**
     * @param string $name The name of the field.
     * @param string $label The label for the field.
     * @return Element\Month
     */
    public function addElementMonth ($name, $label) {
        $el = new Element\Month($name);
        $el->setLabel($label);

        $this->add($el);
        
        return $el;
    }
    
    /**
     * @param string $name The name of the field.
     * @param string $label The label for the field.
     * @return Element\Number
     */
    public function addElementNumber ($name, $label) {
        $el = new Element\Number($name);
        $el->setLabel($label);

        $this->add($el);
        
        return $el;
    }
    
    /**
     * @param string $name The name of the field.
     * @param string $label The label for the field.
     * @return Element\Range
     */
    public function addElementRange ($name, $label) {
        $el = new Element\Range($name);
        $el->setLabel($label);

        $this->add($el);
        
        return $el;
    }
    
    /**
     * @param string $name The name of the field.
     * @param string $label The label for the field.
     * @return Element\Time
     */
    public function addElementTime ($name, $label) {
        $el = new Element\Time($name);
        $el->setLabel($label);

        $this->add($el);
        
        return $el;
    }
    
    /**
     * @param string $name The name of the field.
     * @param string $label The label for the field.
     * @return Element\Url
     */
    public function addElementUrl ($name, $label) {
        $el = new Element\Url($name);
        $el->setLabel($label);

        $this->add($el);
        
        return $el;
    }
    
    /**
     * @param string $name The name of the field.
     * @param string $label The label for the field.
     * @return Element\Week
     */
    public function addElementWeek ($name, $label) {
        $el = new Element\Week($name);
        $el->setLabel($label);

        $this->add($el);
        
        return $el;
    }
}
