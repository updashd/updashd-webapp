<?php
namespace Application\Form;

class Environment extends AbstractForm {
    public function init () {
        $this->addElementHidden('environment_id');

        $this->addElementText('environment_name', 'Name')
            ->setRequired();

        $this->addElementText('sort_order', 'Sort Order')
            ->setRequired();

        $this->addElementSelect('account_id', 'Account');

        $this->addElementSubmit('submit', 'Submit');
    }
}