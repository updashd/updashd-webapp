<?php
namespace Application\Form;

class IncidentFilter extends AbstractForm {
    protected $environmentOptions;
    protected $zoneOptions;
    protected $serviceOptions;
    protected $severityOptions;

    /**
     * Node constructor.
     * @param array $environmentOptions
     * @param array $serviceOptions
     * @param array $zoneOptions
     * @param array $severityOptions
     * @param string $name
     * @param array $options
     */
    public function __construct ($environmentOptions, $serviceOptions, $zoneOptions, $severityOptions,
                                 $name = '', array $options = array()) {
        $this->setEnvironmentOptions($environmentOptions);
        $this->setServiceOptions($serviceOptions);
        $this->setZoneOptions($zoneOptions);
        $this->setSeverityOptions($severityOptions);

        parent::__construct($name, $options);
    }

    public function init () {
        $this->setMethod('GET');

        $this->addElementMultiSelect('environment', 'Environment', $this->getEnvironmentOptions());
        $this->addElementMultiSelect('service', 'Service', $this->getServiceOptions());
        $this->addElementMultiSelect('zone', 'Zone', $this->getZoneOptions());
        $this->addElementMultiSelect('severity', 'Severity', $this->getSeverityOptions());
        $this->addElementText('node_name', 'Node Name');
        $this->addElementText('node_service_name', 'Node Service Name');
        $this->addElementSelect('read', 'Read', ['' => 'No Filter', '1' => 'Yes', '0' => 'No']);
        $this->addElementSelect('resolved', 'Resolved', ['' => 'No Filter', '1' => 'Yes', '0' => 'No']);
        $this->addElementSubmit('filter', 'Filter');
    }

    /**
     * @return array
     */
    public function getEnvironmentOptions () {
        return $this->environmentOptions;
    }

    /**
     * @param array $environmentOptions
     */
    public function setEnvironmentOptions ($environmentOptions) {
        $this->environmentOptions = $environmentOptions;
    }

    /**
     * @return array
     */
    public function getServiceOptions () {
        return $this->serviceOptions;
    }

    /**
     * @param array $serviceOptions
     */
    public function setServiceOptions ($serviceOptions) {
        $this->serviceOptions = $serviceOptions;
    }

    /**
     * @return array
     */
    public function getZoneOptions () {
        return $this->zoneOptions;
    }

    /**
     * @param array $zoneOptions
     */
    public function setZoneOptions ($zoneOptions) {
        $this->zoneOptions = $zoneOptions;
    }

    /**
     * @return array
     */
    public function getSeverityOptions () {
        return $this->severityOptions;
    }

    /**
     * @param array $severityOptions
     */
    public function setSeverityOptions ($severityOptions) {
        $this->severityOptions = $severityOptions;
    }
}