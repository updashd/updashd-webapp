<?php
namespace Application\Form;

use Updashd\Scheduler\ScheduleType;

class NodeServiceBasic extends AbstractForm {
    protected $serviceId;
    
    public function init () {
        $this->addElementHidden('node_id');
        
        $this->addElementSelect('service_id', 'Service')
            ->setRequired();
        
        $this->addElementText('node_service_name', 'Name');
        
        $this->addElementText('sort_order', 'Sort Order')
            ->setRequired();
        
        $this->addElementSelect('schedule_type', 'Schedule Type', $this->getScheduleTypes())
            ->setRequired();
        
        $this->addElementText('schedule_value', 'Schedule Value');
        
        $this->addElementColor('color', 'Color');
        
        $this->addElementMultiSelect('zone_ids', 'Zones')
            ->setRequired();
        
        $this->addElementSubmit('submit', 'Submit');
    }
    
    public function getScheduleTypes () {
        return [
            ScheduleType::TYPE_ONCE => 'Once',
            ScheduleType::TYPE_SECOND_INTERVAL => 'Interval (in seconds)',
            ScheduleType::TYPE_CRON => 'Cron',
        ];
    }
    
    /**
     * @return mixed
     */
    public function getServiceId () {
        return $this->serviceId;
    }
    
    /**
     * @param mixed $serviceId
     */
    public function setServiceId ($serviceId) {
        $this->serviceId = $serviceId;
    }
}