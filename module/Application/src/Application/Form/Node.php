<?php
namespace Application\Form;

class Node extends AbstractForm {
    public function init () {
        $this->addElementHidden('node_id');

        $this->addElementText('node_name', 'Name')
            ->setRequired();

        $this->addElementSelect('environment_id', 'Environment')
            ->setRequired();

        $this->addElementText('hostname', 'Hostname');

        $this->addElementText('ip', 'IP Address');

        $this->addElementSelect('sort_order', 'Sort Order', [0 => '0']);

        $this->addElementSelect('account_id', 'Account');

        $this->addElementSubmit('submit', 'Submit');
    }

    public function isValid () {
        $isValid = parent::isValid();

        if (! $this->getElementValue('hostname') && ! $this->getElementValue('ip')) {
            $isValid = false;
            $this->get('hostname')->addMessage('Either a hostname or an IP address is required.');
        }

        return $isValid;
    }
}