<?php
namespace Application\Form\User;

use Application\Form\AbstractForm;
use Application\Validator\PasswordStrength;

class Password extends AbstractForm {

    const FIELD_CURRENT_PASSWORD = 'current_password';
    const FIELD_NEW_PASSWORD = 'new_password';
    const FIELD_NEW_PASSWORD_AGAIN = 'new_password_again';
    const BTN_CHANGE_PASSWORD = 'change_password';

    public function init () {
        $this->addElementPassword(self::FIELD_CURRENT_PASSWORD, 'Current Password')
            ->setRequired(true);

        $this->addElementPassword(self::FIELD_NEW_PASSWORD, 'New Password')
            ->addValidator(new PasswordStrength())
            ->setRequired(true);

        $this->addElementPassword(self::FIELD_NEW_PASSWORD_AGAIN, 'New Password (again)')
            ->setRequired(true);

        $this->addElementSubmit(self::BTN_CHANGE_PASSWORD, 'Change');
    }

    public function isValid () {
        $isValid = parent::isValid();

        $newPassword = $this->getElementValue(self::FIELD_NEW_PASSWORD);
        $newPasswordAgain = $this->getElementValue(self::FIELD_NEW_PASSWORD_AGAIN);

        if ($newPassword != $newPasswordAgain) {
            $isValid = false;

            $this->get(self::FIELD_NEW_PASSWORD_AGAIN)
                ->addMessage('Passwords must match.');
        }

        return $isValid;
    }

    public function setPasswordInvalid () {
        $this->get(self::FIELD_CURRENT_PASSWORD)
            ->addMessage('Password invalid. Please try again...');
    }
}