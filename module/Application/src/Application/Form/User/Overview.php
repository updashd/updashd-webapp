<?php
namespace Application\Form\User;

use Application\Form\AbstractForm;
use Laminas\Validator\EmailAddress;

class Overview extends AbstractForm {

    const FIELD_NAME = 'name';
    const FIELD_EMAIL = 'email';
    const FIELD_SUBMIT = 'submit';

    public function init () {
        $this->addElementText(self::FIELD_NAME, 'Full Name')
            ->setRequired();

        $this->addElementEmail(self::FIELD_EMAIL, 'Email')
            ->setRequired()
            ->addValidator(new EmailAddress());

        $this->addElementSubmit(self::FIELD_SUBMIT, 'Submit');
    }
}