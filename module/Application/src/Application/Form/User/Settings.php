<?php
namespace Application\Form\User;

use Application\Form\AbstractForm;

class Settings extends AbstractForm {

    const FIELD_TIMEZONE = 'timezone';
    const FIELD_DATE_FORMAT = 'date_format';
    const FIELD_TIME_FORMAT = 'time_format';
    const FIELD_DATETIME_FORMAT = 'datetime_format';
    const BTN_SAVE = 'save';

    public function init () {
        // timezone
        $this->addElementSelect(self::FIELD_TIMEZONE, 'Time Zone', $this->getTimezoneOptions());

        // date_format
        $this->addElementSelect(self::FIELD_DATE_FORMAT, 'Date Format', $this->getDateFormatOptions());

        // time_format
        $this->addElementSelect(self::FIELD_TIME_FORMAT, 'Time Format', $this->getTimeFormatOptions());

        // datetime_format
        $this->addElementSelect(self::FIELD_DATETIME_FORMAT, 'Date and Time Format', $this->getDateTimeFormatOptions());

        $this->addElementSubmit(self::BTN_SAVE, 'Save');
    }

    protected function getTimezoneOptions () {
        $timezoneIdentifiers = \DateTimeZone::listIdentifiers();

        $output = [];

        foreach ($timezoneIdentifiers as $timezoneIdentifier) {
            $parts = explode('/', $timezoneIdentifier);

            $continent = array_shift($parts);

            $rest = implode('—', $parts);

            $rest = str_replace('_', ' ', $rest);

            if (! $rest) {
                $rest = $timezoneIdentifier;
            }

            $rest .= ' (' . $timezoneIdentifier . ')';

            $output[$continent]['label'] = $continent;
            $output[$continent]['options'][$timezoneIdentifier] = $rest;
        }

        return $output;
    }

    protected function getDateFormatOptions () {
        $date = new \DateTime();
        $output = [];

        $formats = [
            'Y-m-d' => 'YYYY-MM-DD',
            'm/d/Y' => 'MM/DD/YY'
        ];

        foreach ($formats as $format => $desc) {
            $output[$format] = $date->format($format) . ' (' . $desc . ')';
        }

        return $output;
    }

    protected function getTimeFormatOptions () {
        $date = new \DateTime();
        $output = [];

        $formats = [
            'g:i:s A T O' => 'H:MM:SS AM/PM - 12-hour',
            'H:i:s T O' => 'HH:MM:SS - 24-hour'
        ];

        foreach ($formats as $format => $desc) {
            $output[$format] = $date->format($format) . ' (' . $desc . ')';
        }

        return $output;
    }

    protected function getDateTimeFormatOptions () {
        $date = new \DateTime();
        $output = [];

        $formats = [
            'Y-m-d g:i:s A T O' => 'YYYY-MM-DD H:MM:SS AM/PM - 12-hour',
            'Y-m-d H:i:s T O' => 'YYYY-MM-DD HH:MM:SS - 24-hour'
        ];

        foreach ($formats as $format => $desc) {
            $output[$format] = $date->format($format) . ' (' . $desc . ')';
        }

        return $output;
    }
}