<?php
namespace Application\Form;

class Account extends AbstractForm {
    public function init () {
        $this->addElementHidden('account_id', 'Account ID');
        
        $this->addElementText('name', 'Account Name')
            ->setRequired();
        
        $this->addElementText('slug', 'Slug')
            ->setRequired();

        $this->addElementText('logo_url', 'Logo URL');

        $this->addElementSelect('owner_id', 'Owner')
            ->setRequired();
        
        $this->addElementSubmit('submit', 'Submit');
    }
}