<?php
namespace Application\Menu;

use Application\Service\Menu;

class Item {
    const TYPE_ITEM = 1;
    const TYPE_DIVIDER = 2;

    private $id;
    private $name;
    private $url;
    private $route;
    private $controller;
    private $action;
    private $reuseParams = true;
    private $icon = null;
    private $isHidden = false;
    private $collapseWhenNotActive = false;
    private $type = self::TYPE_ITEM;

    private $parent;
    private $children;

    public function __construct ($id, $name, $url = null) {
        $this->setId($id);
        $this->setName($name);
        $this->setUrl($url);
    }
    
    /**
     * Determine if this item has visible children
     * @param Menu $menu
     * @return bool
     */
    public function hasVisibleChildren (Menu $menu) {
        if ($this->getCollapseWhenNotActive() && ! $this->isActive($menu)) {
            return false;
        }
        
        if ($this->hasChildren()) {
            foreach ($this->getChildren() as $child) {
                if ($child->isVisible($menu)) {
                    return true;
                }
            }
        }
        
        return false;
    }
    
    /**
     * Should the menu item be visible
     * @param Menu $menu menu object with active id
     * @return bool true if visible, false if not
     */
    public function isVisible (Menu $menu) {
        // The item is visible if it is not hidden, or if it is active (it or any descendant)
        return ! $this->getIsHidden() || $this->isActive($menu);
    }
    
    /**
     * Determine if this item is active (it is active or it has an active descendant)
     * @param Menu $menu the menu (which knows what the active ID is)
     * @return bool
     */
    public function isActive (Menu $menu) {
        if ($this->getId() ==  $menu->getActiveId()) {
            return true;
        }
        
        if ($this->hasChildren()) {
            foreach ($this->getChildren() as $child) {
                if ($child->isActive($menu)) {
                    return true;
                }
            }
        }
        
        return false;
    }
    
    /**
     * Determine if the item has children
     * @return bool
     */
    public function hasChildren () {
        return $this->children ? true : false;
    }

    /**
     * @return string
     */
    public function getId () {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId ($id) {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getUrl () {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl ($url) {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getName () {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName ($name) {
        $this->name = $name;
    }

    /**
     * @return bool
     */
    public function getIsHidden () {
        return $this->isHidden;
    }

    /**
     * @param bool $isHidden
     */
    public function setIsHidden ($isHidden) {
        $this->isHidden = $isHidden;
    }

    /**
     * @return Item[]|null
     */
    public function getChildren () {
        return $this->children;
    }

    /**
     * @param Item[]|null $children
     */
    public function setChildren ($children) {
        $this->children = $children;
    }

    /**
     * @return mixed
     */
    public function getRoute () {
        return $this->route;
    }

    /**
     * @param mixed $route
     */
    public function setRoute ($route) {
        $this->route = $route;
    }

    /**
     * @return mixed
     */
    public function getController () {
        return $this->controller;
    }

    /**
     * @param mixed $controller
     */
    public function setController ($controller) {
        $this->controller = $controller;
    }

    /**
     * @return mixed
     */
    public function getAction () {
        return $this->action;
    }

    /**
     * @param mixed $action
     */
    public function setAction ($action) {
        $this->action = $action;
    }

    /**
     * @return boolean
     */
    public function getReuseParams () {
        return $this->reuseParams;
    }

    /**
     * @param boolean $reuseParams
     */
    public function setReuseParams ($reuseParams) {
        $this->reuseParams = $reuseParams;
    }

    /**
     * @return mixed
     */
    public function getIcon () {
        return $this->icon;
    }

    /**
     * @param mixed $icon
     */
    public function setIcon ($icon) {
        $this->icon = $icon;
    }

    /**
     * @return Item|null
     */
    public function getParent () {
        return $this->parent;
    }

    /**
     * @param Item|null $parent
     */
    public function setParent ($parent) {
        $this->parent = $parent;
    }

    /**
     * @return int
     */
    public function getType () {
        return $this->type;
    }

    /**
     * @param int $type
     */
    public function setType ($type) {
        $this->type = $type;
    }
    
    /**
     * @return boolean
     */
    public function getCollapseWhenNotActive () {
        return $this->collapseWhenNotActive;
    }
    
    /**
     * @param boolean $collapseWhenNotActive
     */
    public function setCollapseWhenNotActive ($collapseWhenNotActive) {
        $this->collapseWhenNotActive = $collapseWhenNotActive;
    }

    public function isItem () {
        return $this->getType() == self::TYPE_ITEM;
    }

    public function isDivider () {
        return $this->getType() == self::TYPE_DIVIDER;
    }
}