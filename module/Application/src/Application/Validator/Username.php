<?php
namespace Application\Validator;

use Laminas\Validator\AbstractValidator;
use Laminas\Validator\Exception;
use Laminas\Validator\Regex;

class Username extends AbstractValidator {
    const INVALID  = 'invalid';
    const RESERVED  = 'reserved';

    protected $messageTemplates = array(
        self::INVALID => "'%value%' contains invalid characters. Must begin with a letter and use letters, numbers, underscores, and dashes only.",
        self::RESERVED => "Admin, auth, and ajax are reserved for special purposes.",
    );

    /**
     * Returns true if and only if $value meets the validation requirements
     *
     * If $value fails validation, then this method returns false, and
     * getMessages() will return an array of messages that explain why the
     * validation failed.
     *
     * @param  mixed $value
     * @return bool
     * @throws Exception\RuntimeException If validation of $value is impossible
     */
    public function isValid ($value) {
        $regex = new Regex('/^[a-zA-Z][a-zA-Z0-9_\-]+$/');
        
        $isValid = true;
        
        $forbiddenList = [
            'admin',
            'auth',
            'ajax',
        ];
        
        if (in_array($value, $forbiddenList)) {
            $this->error(self::RESERVED);
            $isValid = false;
        }

        if (! $regex->isValid($value)) {
            $this->error(self::INVALID);
            $isValid = false;
        };
        
        return $isValid;
    }
}