<?php
namespace Application\Acl;

class Roles {
    const ADMIN = 'ADMIN';
    const OWNER = 'OWNER';
    const MEMBER = 'MEMBER';
    const VIEWER = 'VIEWER';
}