<?php
namespace Application\Acl;

class Actions {
    const CREATE = 'CREATE';
    const READ = 'READ';
    const UPDATE = 'UPDATE';
    const DELETE = 'DELETE';
    const ACCESS = 'ACCESS';
}