<?php
namespace Application\Acl;

class Resources {
    const ACCOUNT = 'ACCOUNT';
    const ENVIRONMENT = 'ENVIRONMENT';
    const INCIDENT = 'INCIDENT';
    const NODE = 'NODE';
    const NODE_SERVICE = 'NODE_SERVICE';
    const NODE_SERVICE_NOTICE = 'NODE_SERVICE_NOTICE';
    const NODE_SERVICE_ZONE = 'NODE_SERVICE_ZONE';
    const NOTICE = 'NOTICE';
    const USER = 'USER';
    const RESULT = 'RESULT';
    const SERVICE = 'SERVICE';
    const ZONE = 'ZONE';
}