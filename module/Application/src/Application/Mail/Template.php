<?php
namespace Application\Mail;

use Pelago\Emogrifier;
use Laminas\View\Model\ViewModel;
use Laminas\View\Renderer\PhpRenderer;
use Laminas\View\Renderer\RendererInterface;

class Template {
    protected $layout = null;
    protected $layoutParameters = array();
    protected $template = null;
    protected $templateRenderer = null;
    protected $templateParameters = array();
    protected $html = null;

    public function render () {
        $content = $this->getHtml();

        $resolver = new TemplateResolver();

        // Render template content first
        if ($this->getTemplate()) {
            $templateRenderer = $this->getTemplateRenderer();

            if (! $templateRenderer) {
                $templateRenderer = new PhpRenderer();
            }

            $templateRenderer->setResolver($resolver);

            $templateViewModel = new ViewModel();
            $templateViewModel->setTemplate($this->getTemplate());
            $templateViewModel->setVariables($this->getTemplateParameters());
            $templateViewModel->setVariable('content', $content);

            $content = $templateRenderer->render($templateViewModel);
        }

        // Render layout
        if ($this->getLayout()) {
            $layoutRenderer = new PhpRenderer();
            $layoutRenderer->setResolver($resolver);

            $layoutViewModel = new ViewModel();
            $layoutViewModel->setTemplate($this->getLayout());
            $layoutViewModel->setVariables($this->getLayoutParameters());
            $layoutViewModel->setVariable('content', $content);

            $content = $layoutRenderer->render($layoutViewModel);
        }

        // Inline CSS
        $emogrifier = new Emogrifier();
        $emogrifier->setHtml($content);

        return $emogrifier->emogrify();
    }

    /**
     * @param string $layout Name of the layout (<module>/<filename>)
     */
    public function setLayout ($layout) {
        $this->layout = $layout;
    }

    /**
     * @param string $template Name of the template (<module>/<filename>)
     */
    public function setTemplate ($template) {
        $this->template = $template;
    }

    public function getTemplate () {
        return $this->template;
    }

    public function getLayout () {
        return $this->layout;
    }

    /**
     * @return string
     */
    public function getHtml () {
        return $this->html;
    }

    /**
     * @param string $html
     */
    public function setHtml ($html) {
        $this->html = $html;
    }

    /**
     * @return array
     */
    public function getLayoutParameters () {
        return $this->layoutParameters;
    }

    /**
     * @param array $layoutParameters
     */
    public function setLayoutParameters ($layoutParameters) {
        $this->layoutParameters = $layoutParameters;
    }

    /**
     * @return array
     */
    public function getTemplateParameters () {
        return $this->templateParameters;
    }

    /**
     * @param array $templateParameters
     */
    public function setTemplateParameters ($templateParameters) {
        $this->templateParameters = $templateParameters;
    }

    /**
     * @return RendererInterface
     */
    public function getTemplateRenderer () {
        return $this->templateRenderer;
    }

    /**
     * @param RendererInterface $templateRenderer
     */
    public function setTemplateRenderer (RendererInterface $templateRenderer) {
        $this->templateRenderer = $templateRenderer;
    }
}