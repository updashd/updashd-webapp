<?php
namespace Application\Mail;

use Laminas\View\Renderer\RendererInterface;
use Laminas\View\Resolver\ResolverInterface;

class TemplateResolver implements ResolverInterface {

    /**
     * @param string $name
     * @param RendererInterface|null $renderer
     * @return bool|string
     */
    public function resolve ($name, RendererInterface $renderer = null) {
        if (strpos($name, '.phtml') === false) {
            $name .= '.phtml';
        }

        $filename = ROOT_DIR . '/module/Application/view/email/' . $name;

        if (file_exists($filename)) {
            return $filename;
        }
        else {
            return false;
        }
    }
}