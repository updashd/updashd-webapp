<?php
namespace Application\Mail;

use Html2Text\Html2Text;
use Laminas\Mime\Message as MimeMessage;
use Laminas\Mime\Mime;
use Laminas\Mime\Part;
use Laminas\Mime\Part as MimePart;

class Message extends \Laminas\Mail\Message {
    const EOL = "\r\n";

    protected $html;
    protected $plain;

    public function setContentBody ($html, $plain = null) {
        $htmlPart = new MimePart($html);
        $htmlPart->setType("text/html");

        if (! $plain) {
            $html2Text = new Html2Text($html);
            $plain = $html2Text->getText();
        }

        $plainPart = new MimePart($plain);
        $plainPart->setType('text/plain');

        $this->plain = $plain;
        $this->html = $html;

        $multiPartMessage = new MimeMessage();
        $multiPartMessage->addPart($plainPart);
        $multiPartMessage->addPart($htmlPart);

        $multiPartContentMimePart = new Part($multiPartMessage->generateMessage());
        $multiPartContentMimePart->setType(Mime::MULTIPART_ALTERNATIVE . ';' . self::EOL . ' boundary="' . $multiPartMessage->getMime()->boundary() . '"');

        $body = new MimeMessage();

        // Add HTML and plain text part to the body
        $body->addPart($multiPartContentMimePart);

        $this->setEncoding('UTF-8');
        $this->setBody($body);
    }

    /**
     * @return mixed
     */
    public function getHtml () {
        return $this->html;
    }

    /**
     * @return mixed
     */
    public function getPlain () {
        return $this->plain;
    }
}