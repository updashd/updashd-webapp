<?php
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

require_once "vendor/autoload.php";

$doctrineConfig = include __DIR__ . '/config/autoload/doctrine.default.php';
if (file_exists(__DIR__ . '/config/autoload/doctrine.local.php')) {
    $doctrineConfig = array_replace_recursive($doctrineConfig, include __DIR__ . '/config/autoload/doctrine.local.php');
}

$doctrineConfig = $doctrineConfig['doctrine'];

$config = Setup::createAnnotationMetadataConfiguration(
    $doctrineConfig['options']['entity_paths'],
    $doctrineConfig['options']['dev_mode'],
    $doctrineConfig['options']['proxy_dir'],
    null, // Ignore cache for CLI
    $doctrineConfig['options']['simple_annotation']
);

$entityManager = EntityManager::create($doctrineConfig['connection'], $config);

return \Doctrine\ORM\Tools\Console\ConsoleRunner::createHelperSet($entityManager);

/*
 * ./vendor/bin/doctrine orm:convert-mapping -f --from-database --extend "Application\\Model\\Entity\\AbstractAuditedEntity" --namespace "Application\\Model\\Entity\\" -- annotation module/Application/src/
 * ./vendor/bin/doctrine orm:generate-entities --generate-annotations=true module/Application/src
 */