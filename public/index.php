<?php

use Laminas\Mvc\Application;
use Laminas\Stdlib\ArrayUtils;

/**
 * This makes our life easier when dealing with paths. Everything is relative
 * to the application root now.
 */

define('ROOT_DIR', dirname(__DIR__));

chdir(ROOT_DIR);

set_error_handler('errorHandler');

function errorHandler($severity, $message, $filename, $lineno) {
    if (error_reporting() == 0) {
        return;
    }
    
    if (error_reporting() & $severity) {
        throw new ErrorException($message, 0, $severity, $filename, $lineno);
    }
}

// Decline static file requests back to the PHP built-in webserver
if (php_sapi_name() === 'cli-server') {
    $path = realpath(__DIR__ . parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
    if (__FILE__ !== $path && is_file($path)) {
        return false;
    }
    unset($path);
}

// Composer autoloading
include __DIR__ . '/../vendor/autoload.php';

// Retrieve configuration
$appConfig = require __DIR__ . '/../config/application.default.php';
if (file_exists(__DIR__ . '/../config/application.local.php')) {
    $appConfig = ArrayUtils::merge($appConfig, require __DIR__ . '/../config/application.local.php');
}

// Run the application!
Application::init($appConfig)->run();
