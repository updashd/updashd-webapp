function checkAvailabilty (elem, url, field) {
    elem.on('keyup', function () {
        var obj = {};
        obj[field] = elem.val();

        var grp = elem.closest('.form-group');
        grp.removeClass('has-success');
        grp.removeClass('has-error');

        grp.find('span')
            .addClass('fa-spinner fa-spin');

        var timeout = elem.data("timeout");

        if (timeout) {
            clearTimeout(timeout);
        }

        timeout = setTimeout(function () {
            $.getJSON(url, obj, function (data) {
                grp.find('span')
                    .removeClass('fa-spinner fa-spin');

                if (data.available) {
                    grp.addClass("has-success");
                }
                else {
                    grp.addClass("has-error");
                }
            });
        }, 500);

        elem.data("timeout", timeout);
    });
}

$(document).ready(function () {
    checkAvailabilty($('input[name="username"]'), "/auth/signup/checkusername", "username");
});