$(".account-select").select2({
    ajax: {
        url: "/ajax/account/search",
        dataType: 'json',
        delay: 250,
        data: function (params) {
            return {
                q: params.term
            };
        },
        processResults: function (data, params) {
            // params.page = params.page || 1;

            var formattedResult = [];
            for (var i in data) {
                var result = data[i];
                formattedResult.push({
                    "id": result.id,
                    "text": result.name + " (" + result.owner + ")"
                });
            }

            return {
                results: formattedResult
                // pagination: {
                //     more: (params.page * 30) < data.total_count
                // }
            };
        },
        cache: true
    },
    minimumInputLength: 1
});