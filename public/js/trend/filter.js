var sourceMapping = {
    "environment": "environments",
    "zone": "zones",
    "node": "nodes",
    "service": "services",
    "node_service": "node_services",
    "field": "fields"
};

var sourceIsUpdated = {};

function getJqElemById(id) {
    var selector = "." + id;
    return $(selector);
}

function getParams() {
    var params = {};

    params['account'] = getJqElemById('account').val();

    for (var i in sourceMapping) {
        params[i] = getJqElemById(i).val();
    }

    return params;
}

function update(id) {
    var path = sourceMapping[id];
    var elem = getJqElemById(id);

    setIsUpdated(id, false);

    $.get("/ajax/trend/" + path, getParams(), function (data) {
        console.log(id, data.length);

        var oldValue = elem.val();
        // elem.select2("destroy");

        elem.select2("close");
        elem.empty();

        elem.select2({
            data: data
        });

        elem.val(oldValue);
        elem.trigger("change");

        setIsUpdated(id, true);
    }).fail(function () {
        // TODO: handle this
        setIsUpdated(id, true);
    });
}

function setBoxState(id, isDone) {
    var box = $("." + id);

    // If the box is currently "loading"
    var overlay = box.find('.overlay');

    // Remove loading overlay (if it is there)
    if (isDone && overlay.length) {
        overlay.detach();
    }
    // Add loading overlay (if it is not there)
    else if (! overlay.length) {
        box.append('<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>')
    }
}

function getIsUpdated() {
    var isDone = true;

    for (var i in sourceIsUpdated) {
        if (! sourceIsUpdated[i]) {
            isDone = false;
        }
    }

    return isDone;
}

function setIsUpdated(id, isUpdated) {
    sourceIsUpdated[id] = isUpdated;

    setBoxState('filters', getIsUpdated());
}

function updateAll (id) {
    for (var i in sourceMapping) {
        // if (id != i) {
            update(i);
        // }
    }
}

function bindEvents() {
    for (var i in sourceMapping) {
        // Get jQuery Element
        var elem = getJqElemById(i);

        // Init select2 field.
        elem.select2();

        // Function for creating the callback
        var callbackFunctionFactory = function (i) {
            var _i = i;

            return function () {
                // Only update if we aren't waiting for other updates
                if (getIsUpdated()) {
                    updateAll(_i);
                }
            }
        };

        // Call the update function for the given id
        getJqElemById(i).change(callbackFunctionFactory(i));

        // Update the data right now
        update(i);
    }
}

// When everything is ready
$(document).ready(function() {
    bindEvents();
});