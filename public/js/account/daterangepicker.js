function initDateRangePicker (selector, start, end) {
    $(selector).daterangepicker({
        "timePicker": true,
        "showDropdowns": true,
        ranges: {
            'Today': [moment().startOf('day'), moment().endOf('day')],
            'Yesterday': [moment().subtract(1, 'days').startOf('day'), moment().subtract(1, 'days').endOf('day')],
            'Last 7 Days': [moment().subtract(6, 'days').startOf('day'), moment().endOf('day')],
            'Last 30 Days': [moment().subtract(29, 'days').startOf('day'), moment().endOf('day')],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        "locale": {
            "format": "MM/DD/YYYY hh:mm:ss a",
            "separator": " - ",
            "firstDay": 0
        },
        "startDate": start,
        "endDate": end,
        "opens": "left"
    }, function () {
        // $(selector).closest('form').submit();
    });
}
