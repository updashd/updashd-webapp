function initializeFieldType() {
    $('.config-field-type').change(updateFieldType).each(updateFieldType);
}

function updateFieldType () {
    var elem = $(this);

    var useFieldType = elem.val();
    // var useReferenceField = elem.is(':checked');

    var fieldParent = elem.closest('.config-field');
    var valueColumn = fieldParent.find('.config-value');
    var referenceFieldColumn = fieldParent.find('.config-reference-field');

    // Use Reference Field
    if (useFieldType == 'reference_field') {
        valueColumn.hide();
        referenceFieldColumn.show();
    }
    // Use Value
    else if (useFieldType == 'value') {
        referenceFieldColumn.hide();
        valueColumn.show();
    }
    // Default or unknown
    else {
        valueColumn.hide();
        referenceFieldColumn.hide();
    }
}

initializeFieldType();
