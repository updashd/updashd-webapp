var statusIcons = {
    "Rescheduling": "text-aqua  fa-clock-o",
    "Scheduled": "text-aqua  fa-calendar-check-o",
    "Pending": "text-aqua  fa-hourglass-1",
    "Processing": "text-aqua  fa-hourglass-2",
    "Complete": "text-aqua  fa-hourglass-3",
    "Error": "text-red   fa-warning",
    "Disabled": "text-muted fa-power-off",
    "Undefined": "text-muted fa-question"
};

var pusher = (function () {
    var connections = [];

    var addSubscription = function (server, nszid, jwt, elem) {
        addServerSubscription(server, nszid, {
            jwt: jwt,
            elem: elem
        });
    };

    var addServer = function (server) {
        if (! connections.hasOwnProperty(server)) {
            connections[server] = {
                "connection": null,
                "subscriptions": []
            };

            connect(server);
        }
    };

    var addServerSubscription = function (server, nszid, sub) {
        addServer(server);

        connections[server].subscriptions[nszid] = sub;

        if (connections[server].connection) {
            sendSubscription(connections[server].connection, sub)
        }
    };

    var sendSubscription = function (connection, sub) {
        connection.send(JSON.stringify({
            jwt: sub.jwt
        }));
    };

    var connect = function (server) {
        var connection = new WebSocket(server);

        connection.onopen = function (e) {
            console.log("Connection to " + server + " established!");

            connections[server].connection = connection;

            let subs = connections[server].subscriptions;

            for (var i in subs) {
                sendSubscription(connection, subs[i]);
            }
        };

        connection.onmessage = function (e) {
            console.log(e.data);
            var jData = JSON.parse(e.data);
            var elem = connections[server].subscriptions[jData.nszid].elem;

            elem.removeClass(elem.data("current_class"));
            elem.addClass("fa fa-spinner text-muted");

            elem.data("current_class", statusIcons[jData.state]);

            // Add correct icon for status
            elem.addClass(statusIcons[jData.state]);
        };
    };

    return {
        "addSubscription": addSubscription
    };
})();

$(".node-service-zone-status").each(function () {
    var elem = $(this);
    var server = elem.data("server");
    var nodeServiceZoneId = elem.data("node-service-zone-id");
    var jwt = elem.data("jwt");

    pusher.addSubscription(server, nodeServiceZoneId, jwt, elem);

    // var nodeServiceId = elem.data("node-service-id");
    // var zoneId = elem.data("zone-id");
    // var zoneName = elem.data("zone-name");

    var setTooltip = function (elem, status, details) {
        var seperator = "<br />";
        var text = "";

        if (details && details.Zone) {
            text += "Zone: " + details.Zone + seperator;
        }

        text += "Status: " + status;

        if (status != "Undefined" && details && details.Enabled == "1") {
            text += details.LastCompletionTime ? seperator + "Last Run: " + details.LastCompletionTime : "";
            text += details.ScheduledTime ? seperator + "Next Run: " + details.ScheduledTime : "";
            text += details.RunCount ? seperator + "Run Count: " + details.RunCount : "";
        }

        elem.tooltip("destroy").tooltip({
            html: true,
            placement: "auto top",
            container: "body",
            trigger: "hover focus",
            title: text
        });
    };

    var checkStatus = function () {
        // Start ajax request for status
        $.get("/ajax/scheduler/check_state", {"nszid": nodeServiceZoneId}, function (data) {
            var state = data.State;

            // Remove refresh icon
            elem.removeClass("fa-spinner text-muted");

            elem.data("current_class", statusIcons[state]);

            // Add correct icon for status
            elem.addClass(statusIcons[state]);

            setTooltip(elem, state, data);
        }).fail(function () {
            // Remove refresh icon
            elem.removeClass("fa-spinner text-muted");

            var c = "text-red fa-times-circle";

            // Add connection error dot
            elem.addClass(c);

            elem.data("current_class", c);

            setTooltip(elem, "Failed to Retrieve");
        });
    };

    var initStatus = function () {
        // Default to loading icon
        elem.removeClass(elem.data("current_class"));
        elem.addClass("fa fa-spinner text-muted");
        setTooltip(elem, "Loading...");
    };

    var refreshStatus = function () {
        initStatus();
        checkStatus();

        return false;
    };

    elem.on("click", refreshStatus);

    refreshStatus();
});