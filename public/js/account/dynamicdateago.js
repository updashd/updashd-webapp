$(document).ready(function () {
    function updateDateAgo () {
        $(".dynamic-date-ago").each(function () {
            let em = $(this);
            em.html(moment.unix(em.data('timestamp')).fromNow());
        });
    }

    updateDateAgo();
    setInterval(updateDateAgo, 30*1000);
});