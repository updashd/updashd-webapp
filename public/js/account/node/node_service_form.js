function initializeZoneMultiselect() {
    $(".zone-tabs .zone-multi").select2();
}

$('.zone-tabs a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    // e.target // newly activated tab
    // e.relatedTarget // previous active tab
    initializeZoneMultiselect();
    initializeFieldType();
});

initializeZoneMultiselect();
